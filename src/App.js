import React from "react";
import 'animate.css/animate.min.css'
import './styles/seed.min.css'
import { BrowserRouter } from "react-router-dom";
import HomeRoutes from './routes/home.routes'
import DashboardRoutes from './routes/dashboard.routes'
import {Store} from './context/Store' //Application context
import { UserProvider } from "./context/User.context";

const App = () => {
  return (
    <Store>
      <UserProvider>
        <div className="w-100 h-100vh of-x-hidden of-y-hidden h-pr-fl-ma">
          <BrowserRouter>
            <HomeRoutes />
            <DashboardRoutes />
          </BrowserRouter>
        </div>
      </UserProvider>
    </Store>
  );
};

export default App;
