import axios from 'axios'

const baseURL=`http://localhost:8080/api/auth/`
const DEFAULT_METHOD='GET'

export const apiRequest = async (url,data,method=DEFAULT_METHOD) => {
    try{
        let response=await axios({
            method,
            url:baseURL+url,
            data
        }).then();
        return response;
    }catch(err){
        console.log(err)
    }
}