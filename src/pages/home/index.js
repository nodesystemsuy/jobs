import React, { Fragment } from "react";
import { useLocation } from "react-router";
import "./styles.css";
import { ReactComponent as Success} from "../../assets/images/success.svg";
import { ReactComponent as Denied} from "../../assets/images/denied.svg";

import { ReactComponent as NodeJobsLogo } from "../../assets/images/nodejobslogo.svg";
import { Btn, Img, SidebarBtn, Text} from "../../components/atoms/index.js";
import { HomeInfoDate, Modal } from "../../components/molecules/index.js";
import denied from "../../assets/images/denied.svg";
import {
  GuestPublications,
  FormLogin,
  BusinessFormRegister,
  PersonFormRegister,
  PasswordChange,
  PasswordRecovery,
} from "../../components/organisms/index.js";
import { Route } from "react-router";
import { Link } from "react-router-dom";
import { //import routes constants
  HOME_LOGIN,
  HOME_PASSWORDCHANGE,
  HOME_PASSWORDRECOVERY,
  HOME_REGISTER_BUSINESS,
  HOME_REGISTER_PERSON,
} from "../../constants/routes.constant";

const HomePage = ({ HomeForm }) => {
    const location = useLocation()
    const view = location.pathname

    return (
      <Fragment>
        <div className="w-90vw ns-bg-2 h-90vh of-h small h-pr-fl-ma of-hidden centered c-br-4">
        {/* <Modal><div className="w-100 h-auto h-pr-fl-ma">
      <img src={denied} className="w-and-h-150px h-pr-fl-ma r-h-c"></img>
      <Text align="t-a-c" size="medium" className="f-w-bo" text="ERROR"></Text>
      <Text  align="t-a-c" text="Soluciona los siguientes conflictos para continuar"></Text>
      <div className="w-auto h-auto h-pr-fl-ma r-h-c"><Text  align="t-a-l" text="Soluciona los siguientes conflictos para continuar"></Text>
      <Text  className="w-80" align="t-a-l" text="Soluciona los siguientes conflictos para continuar"></Text>
      </div>
      </div>
      </Modal> */}
          <div className="w-100 h-10 h-pr-fl-ma ns-bg-1">
            <div className="w-70 m-w-30 h-100 h-pr-fl-ma c-p-1">
              <div className="w-150px m-w-100px h-100 h-pr-fl-ma">
                <Img className="h-auto centered c-p-1 c-br-2 ns-bg-2 ">
                  <NodeJobsLogo className="w-80px v-a-m h-pr-f-ma c-p-1"></NodeJobsLogo>
                </Img>
              </div>
            </div>
            <div className="w-30 m-w-70 h-100 ns-bg-1 h-pr-fr-ma r-v-c c-p-1">
              <div className="w-80 h-auto d-i-f ns-bg-2 c-br-2 c-p-1 centered h-pr-fr-ma">
                <div className="w-50 h-auto h-pr-fr-ma ">

                  <Btn
                    text="REGISTRO"
                    iconLeft="fas fa-user-circle"
                    theme={view === HOME_REGISTER_BUSINESS || view === HOME_REGISTER_PERSON ? "success" : "default"}
                    size="small"
                    className="w-100 h-pr-fr-ma c-f-1"
                    path={HOME_REGISTER_PERSON}
                  ></Btn>
                </div>
                <div className="w-50 h-auto h-pr-fr-ma ">
                  <Btn
                    text="INGRESAR"
                    iconLeft="fas fa-sign-in-alt"
                    theme={view === HOME_LOGIN ? "info" : "default"}
                    size="small"
                    className="w-100 h-pr-fr-ma m-r-5px c-f-1"
                    path={HOME_LOGIN}
                  ></Btn>
                </div>
              </div>
            </div>
          </div>
          <div className="w-70 m-d-n h-100 h-pr-fl-ma">
            {/* SECTOR IZQUIERDO - Hora actual - zonas horarias - soporte - personas online - web nsuy */}
            <div className="w-100 h-80 h-pr-fl-ma ">
              <div className="w-20 h-100 h-pr-fl-ma c-p-1 ns-bg-2">
                <div className="w-100 h-70 h-pr-fl-ma small">
                  <HomeInfoDate
                    Time="21:05"
                    theme="ns-bg-1 ns-b-s"
                    TimeSize="big"
                    className="c-f-4 small"
                  ></HomeInfoDate>
                  <div className="m-b-3px w-100 h-auto of-hidden small f-w-bo c-p-1 c-br-1 c-f-1 h-pr-fl-ma ns-bg-1 ns-b-s">
                    <div className="w-100 h-auto c-br-1 ns-bg-2 c-p-1 h-pr-fl-ma">
                    <SidebarBtn iconLeft="fas fa-clock" text="ARGENTINA"  textSize="normal" counter="19:00" counterSize="small"  counterColor="default small"></SidebarBtn>
                    <SidebarBtn iconLeft="fas fa-clock" text="COLOMBIA"  textSize="normal" counter="19:00" counterSize="small"  counterColor="default small"></SidebarBtn>
                    <SidebarBtn iconLeft="fas fa-clock" text="PERÚ"  textSize="normal" counter="19:00" counterSize="small"  counterColor="default small"></SidebarBtn>
                    <SidebarBtn iconLeft="fas fa-clock" text="URUGUAY"  textSize="normal" counter="19:00" counterSize="small"  counterColor="default small"></SidebarBtn>
                    <SidebarBtn iconLeft="fas fa-clock" text="ZIMBAWE"  textSize="normal" counter="19:00" counterSize="small"  counterColor="default small"></SidebarBtn>
                    </div>
                  </div>
                </div>
                <div className="w-100 h-30 h-pr-fl-ma ">
                  <div className="w-100 h-auto r-v-b small">
                  <SidebarBtn iconLeft="fas fa-user-circle c-green-node" text="CONECTADOS" theme="ns-bg-1 ns-b-s" textSize="nano f-w-bo c-green-node" ></SidebarBtn>
                  <SidebarBtn iconLeft="fas fa-cog" text="SOPORTE TÉCNICO" theme="ns-bg-1 ns-b-s" textSize="nano f-w-bo" ></SidebarBtn>
                  <SidebarBtn iconLeft="fas fa-heart c-red-node" text="NODESYSTEMS.UY" theme="ns-bg-1 ns-b-s" textSize="nano f-w-bo" ></SidebarBtn>
                  </div>
                </div>
              </div>
              <div className="w-80 h-100 h-pr-fl-ma">
                <div className="w-100 ns-bg-2 m-d-n h-100 h-pr-fl-ma">
                  <GuestPublications></GuestPublications>
                </div>
              </div>
            </div>
            {/* PIE DE PAGINA */}
            <div className="w-100 h-10 h-pr-fl-ma ns-bg-1">
              <div className="w-100 h-10vh h-pr-fl-ma ns-bg-1">
                <div className="w-100 h-auto centered h-pr-fl-ma">
                  <div className="w-100 t-a-c h-pr-fl-ma c-f-1 nano c-c-3 f-w-bo">
                    JOBS - Un proyecto de Node Systems
                    <span className="m-d-n c-c-7">
                      Developed by Team Node for NODE WORK - 2020 LATAM
                    </span>
                  </div>
                  <div className="w-100 h-auto h-pr-fl-ma nano c-f-1  c-c-7 f-w-bo">
                    <div className="w-auto flexbox h-auto h-pr-fl-ma r-h-c line-h-0-8em">
                      <span className="w-auto h-auto t-a-c flex-auto h-pr-fl-ma p-5px c-p h-e">
                        Términos y condiciones
                        <span className="m-d-n">de uso</span>
                      </span>
                      <span className="w-auto h-auto t-a-c flex-auto h-pr-fl-ma p-5px c-p h-e">
                        Políticas de privacidad
                      </span>
                      <span className="w-auto h-auto t-a-c flex-auto h-pr-fl-ma p-5px c-p h-e">
                        Políticas de cookies
                      </span>
                      <span className="w-auto h-auto t-a-c flex-auto h-pr-fl-ma p-5px c-p h-e">
                        Soporte de errores
                      </span>
                      <span className="w-auto h-auto t-a-c flex-auto h-pr-fl-ma p-5px c-p h-e">
                        Contacto
                      </span>
                      <span className="w-auto h-auto t-a-c flex-auto h-pr-fl-ma p-5px c-p h-e">
                        Empleo
                      </span>
                      <span className="w-auto h-auto t-a-c flex-auto h-pr-fl-ma p-5px c-p h-e">
                        Glosario
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="w-30 m-w-100 h-80 h-pr-fl-ma ">
            <div className="w-100 h-100 h-pr-fl-ma ns-bg-2">

              <HomeForm />

            </div>
            </div>
            <div  className="w-30 m-w-100 h-10 ns-bg-1 h-pr-fl-ma">

              <div  className="w-auto h-auto centered ns-bg-2 c-br-1 c-p-1 c-br-1 h-pr-fl-ma">

                  <Btn
                    size="small"
                    theme={view === HOME_PASSWORDRECOVERY ? "alert" : "default"}
                    text="Recuperar Contraseña"
                    className="w-auto normal"
                    iconLeft="fas fa-asterisk"
                    path={HOME_PASSWORDRECOVERY}
                  ></Btn>
                </div>
              </div>

          </div>

      </Fragment>
    );
}

export default HomePage
