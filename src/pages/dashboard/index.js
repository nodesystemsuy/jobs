import React from "react";
import "./styles.css";
import 'animate.css';
import denied from "../../assets/images/denied.svg";
import { ReactComponent as NodeJobsLogo } from "../../assets/images/nodejobslogo.svg";
import { Btn, Img, Text, SidebarBtn, ProgBar } from "../../components/atoms/index.js";
import {
  SearchBar,
  MenuBtn,
  SearchFilter,
  PublicationBusiness,
  Notify,
  Modal,
  CardOffer,
  MenuBtnMobile,
  DataBox,
} from "../../components/molecules/index.js";
import {
  ErrorView,
  BusinessProfile,
  BusinessSidebar,
  BusinessEditProfile,
  BusinessPublications,
  BusinessMenu,
  BusinessCreateInterview,
  CreatePost,
  GuestPublications,
  PersonSidebar,
  PersonMyPublications,
  PersonMenu,
  PersonPreviewPost,
  PersonProfile,
  PersonEditProfile,
  PersonPublications,
  TemplatesInterviews,
} from "../../components/organisms/index.js";
import {
  DASHBOARD_TYPE_PERSON,
  DASHBOARD_TYPE_BUSINESS,
} from '../../constants/routes.constant'
import { useState } from "react";

const Business = (props) => {

  const { MainContent, viewType, children } = props

  const switchContentByViewType = (personContent, businessContent) => {
    switch (viewType) {
      case DASHBOARD_TYPE_PERSON:
        return personContent
      case DASHBOARD_TYPE_BUSINESS:
        return businessContent
      default:
        <></>
    }
  }
  // state para ver u ocultar menu
  const [menuView,setMenuView]=useState(false)

  return (
    <>
      {/*
TODO ██████   █████  ███████ ██   ██ ██████   ██████   █████  ██████  ██████ 
TODO ██   ██ ██   ██ ██      ██   ██ ██   ██ ██    ██ ██   ██ ██   ██ ██   ██ 
TODO ██   ██ ███████ ███████ ███████ ██████  ██    ██ ███████ ██████  ██   ██ 
TODO ██   ██ ██   ██      ██ ██   ██ ██   ██ ██    ██ ██   ██ ██   ██ ██   ██ 
TODO ██████  ██   ██ ███████ ██   ██ ██████   ██████  ██   ██ ██   ██ ██████  
 */}

      <div className="w-100 h-100vh h-pr-fl-ma of-hidden">

        <Modal title="Seleccione el plan que más se ajuste a usted:" content="Puede cancelar su suscripción sin costo antes de los primeros de 30 días de su subscripción.">
          <div className=" w-auto h-60vh r-h-c h-pr-fl-ma of-y-auto c-p-1">
        <CardOffer title="FREE" theme="info" before="99.00" subtitle="USD/MES" description="Apoyo para emprendedores" icon="fas fa-user-circle" price="0" cent="00" btnText="Contratar"></CardOffer>
        <CardOffer title="LITE" theme="caution" before="99.00" subtitle="USD/MES" description="Especial para equipos pequeños"  icon="fas fa-user-circle" price="5" cent="99" btnText="Contratar"></CardOffer>
        <CardOffer title="AMATEUR" theme="success" before="99.00" subtitle="USD/MES" description="Preparado para micro pymes" icon="fas fa-user-circle" price="8" cent="99" btnText="Contratar"></CardOffer>
        <CardOffer title="PRO" theme="alert" before="99.00" subtitle="USD/MES" description="Ideal para empresas" icon="fas fa-user-circle" price="17" cent="99" btnText="Contratar"></CardOffer>
        <CardOffer title="RECRUITER" theme="fuchsia" before="99.00" subtitle="USD/MES" description="Inigualable para reclutadores" icon="fas fa-user-circle" price="39" cent="99" btnText="Contratar"></CardOffer>
        </div>
        <div className=" w-auto r-h-c h-auto h-pr-fl-ma  small f-w-bo c-p-1 "><div className="w-auto h-auto c-br-1 r-h-c small c-bg-9 c-p-1 t-a-c h-pr-fl-ma">Puede cancelar su suscripción sin costo antes de los primeros de 30 días de su subscripción.</div></div>

        </Modal>

        {/* Antes de ser presionado el icono es fa-bars pero solo oculta los SidebarBtn (les añade la clase d-n) */}
        {/* <MenuBtnMobile icon="fas fa-times normal " className="default">
      <SidebarBtn iconLeft="fas fa-th-list" size="normal" theme="default" className="w-100 m-b-5px m-l-80px" text="VER PUBLICACIONES"></SidebarBtn>
          <SidebarBtn iconLeft="fas fa-caret-square-right" size="normal" theme="default" className="w-100 m-b-5px m-l-50px" text="ABRIR SIDEBAR"></SidebarBtn>
          <SidebarBtn iconLeft="fas fa-bars" size="normal" theme="default" className="w-100 m-b-5px m-l-20px" text="ABRIR MENU"></SidebarBtn>
          <SidebarBtn iconLeft="fas fa-user-edit" size="normal" theme="default" className="w-100 m-b-5px m-l-10px" text="EDITAR PERFIL"></SidebarBtn>
          <SidebarBtn iconLeft="fas fa-cog" size="normal" theme="default" className="w-100 m-b-5px m-l-10px" text="CONFIGURACIÓN"></SidebarBtn>
          <SidebarBtn iconLeft="fas fa-power-off" size="normal" theme="alert" className="w-100 m-b-5px m-l-20px" text="CERRAR SESIÓN"></SidebarBtn>
        </MenuBtnMobile> */}
        {/* <Modal title="Elija su metodo de pago">
            <div className="w-60 r-h-c h-pr-fl-ma c-p-2">
              <SidebarBtn
                className="w-100 h-pr-fl-ma m-b-2px"
                theme="default"
                size="small f-w-bo"
                icon="fab fa-paypal"
                text="PAYPAL"
              ></SidebarBtn>
              <SidebarBtn
                className="w-100 h-pr-fl-ma m-b-2px"
                theme="default"
                size="small f-w-bo"
                icon="fas fa-handshake"
                text="MERCADO PAGO"
              ></SidebarBtn>
              <SidebarBtn
                className="w-100 h-pr-fl-ma m-b-2px"
                theme="default"
                size="small f-w-bo"
                icon="fab fa-stripe"
                text="STRIPE"
              ></SidebarBtn>
              <SidebarBtn
                className="w-100 h-pr-fl-ma m-b-2px"
                theme="default"
                size="small f-w-bo"
                icon="fab fa-cc-visa"
                text="VISA"
              ></SidebarBtn>
              <SidebarBtn
                className="w-100 h-pr-fl-ma m-b-2px"
                theme="default"
                size="small f-w-bo"
                icon="fab fa-cc-mastercard"
                text="MASTERCARD"
              ></SidebarBtn>
              <SidebarBtn
                className="w-100 h-pr-fl-ma m-b-2px"
                theme="default"
                size="small f-w-bo"
                icon="fab fa-bitcoin"
                text="BITCOIN"
              ></SidebarBtn>
            </div>
            <div className="w-100 h-pr-fl-ma c-bg-9 c-p-2 c-br-2">
              <Btn
                className="w-100 h-35px"
                iconLeft="fas fa-check"
                theme="success"
                text="COMENZAR"
              ></Btn>
            </div>
          </Modal> */}
        <div className="NotifyBox w-40 m-w-80 min-w-20 h-auto z-i-99 c-p-2 bottom right">
           <Notify
         icon="fas fa-exclamation"
            theme="alert"
            title="ESTA ES UNA NOTIFICACIÓN 3.0"
            text="si te anda mal, proba reiniciando la licuadora"
            textSize="nano"
        content="A lo mejor lo terminamos en 2047"
        showNoty={false}
        contentSize="small t-a-c">
        <div className="w-100 h-auto m-b-5px c-br-1 h-pr-fl-ma c-p">
                      <ProgBar className="w-100 h-15px" ></ProgBar>
                    </div>
        </Notify>

          {/*<Notify
            theme="info"
            title="Titulo"
            content="CONTENIDO 2 2 2CONTENIDO 1 1 1CONTENIDO 1 1 1CONTENIDO 1 1 1CONTENIDO 1 1 1CONTENIDO 1 1 1CONTENIDO 1 1 1C 1 1 1CONTENIDO 1 1 1CONTENIDO 1 1 1CONTENIDO 1 1 1CONTENIDO 1 1 1CONTENIDO 1 1 1CONONTENIDO 1 1 1"></Notify>
        <Notify title="Titulo"></Notify>
        <Notify theme="caution" title="" title="Titulo"></Notify>*/}
        </div>
        {/* * ------------------------ | ESTE ES EL COMPONENTE MODAL (Descomentelo para usarlo) | <Modal title="¿ PREGUNTA ?" content="AASODASODIASDAOSID" buttons="true" confirm="true" delete="true" caution="true" accept="true" cancel="true"></Modal>  */}

        {/* * ------------------------ | ESTE ES EL COMPONENTE LOADER (Descomentelo para usarlo) | <Loader></Loader> */}

        {/* HEADER */}
        <div className="w-100 h-10vh h-pr-fl-ma ns-bg-1">
          <div className="w-20 h-100 h-pr-fl-ma">
            <div className="w-35 m-w-80 flexbox centered">
              <div className="w-150px m-w-100px h-100 h-pr-fl-ma">
                <Img className="h-auto r-h-c c-p-1 testing-bg-1 c-br-2">
                  <NodeJobsLogo className="w-100 m-w-100 v-a-m h-pr-f-ma c-p-1"></NodeJobsLogo>
                </Img>
              </div>
            </div>
          </div>

          <div className="w-60 h-100 h-pr-fl-ma ">
            <div className="w-90 h-pr-fl-ma flexbox r-h-c h-100">
          <DataBox childrenStyle="ns-bg-2 " className="small" >
            <SearchBar></SearchBar>
            </DataBox></div>
          </div>
          <div className="w-20 h-100 h-pr-fl-ma c-p-1">
            <div className="w-100 h-auto h-pr-fl-ma ns-bg-2 c-br-2 c-p-1">
            <MenuBtn className="w-100 ns-bg-1 r-h-c" setMenuView={setMenuView} menuView={menuView} menuImg="url"></MenuBtn></div>
            {menuView &&
              <div className="w-95 a-h-c h-auto p-a m-auto m-t-80px z-i-99 c-br-1 c-p-1">
                {switchContentByViewType(
                   (<BusinessMenu className="z-i-99"/>),
                   (<PersonMenu/>)
                )}
              </div>
            }
          </div>
        </div>
        {/* BODY */}
        <div className="w-100 h-80vh h-pr-fl-ma ns-bg-2">
          <div className="w-20 m-d-n h-100 h-pr-fl-ma c-p-1">
            {switchContentByViewType(
              (<PersonSidebar />),
              (<BusinessSidebar />)
            )}
          </div>

          <div className="w-60 m-w-100 h-100 h-pr-fl-ma c-p-1">
            <div className="w-95 d-n m-w-95 b-s-2 a-h-c h-auto p-a m-auto z-i-99 c-br-1 ns-bg-1 c-p-1">
              <SearchFilter className="z-i-99 bg-red"></SearchFilter>
            </div>
            <div className="w-100 h-100 h-pr-fl-ma c-br-1 of-hidden ns-bg-1">
              {MainContent()}
            </div>
          </div>
          <div className="w-20 m-d-n h-100 h-pr-fl-ma c-p-1">
            <BusinessSidebar></BusinessSidebar>
          </div>
        </div>
        {/* FOOTER */}
        <div className="w-100 h-10vh h-pr-fl-ma ns-bg-1">
          <div className="w-20 m-d-n h-100 h-pr-fl-ma">
            <div className="w-auto flexbox h-auto h-pr-fl-ma ns-bg-2 c-p-1 c-br-1 c-f-1 l-h-1em centered">
              <div className="w-auto flexbox h-auto h-pr-fl-ma ns-bg-1 c-p-1 c-br-1">
              <div className="w-100 h-auto h-pr-fl-ma l-h-1em small ns-c-1 p-r-5px">
                Estás en:
              </div>
              {switchContentByViewType(
                (<div className="w-100 h-auto h-pr-fl-ma l-h-1em normal c-skyblue-node f-w-bo">PERSON</div>),
                (<div className="w-100 h-auto h-pr-fl-ma l-h-1em normal c-fuchsia-node f-w-bo">BUSINESS</div>)
              )}
            </div>
            </div>
          </div>
          <div className="w-60 m-w-100 h-100 h-pr-fl-ma ">
            <div className="w-100 h-10vh h-pr-fl-ma ">
              <div className="w-100 h-auto centered h-pr-fl-ma">
                <div className="w-100 t-a-c h-pr-fl-ma c-f-1 nano ns-c-e f-w-bo ">
                  JOBS - Un proyecto de Node Systems
                  <span className="m-d-n ns-c-1 c-p-1">
                    Developed by Team Node for NODE WORK - 2020 LATAM
                  </span>
                </div>
                <div className="w-100  h-auto h-pr-fl-ma nano c-f-1 ns-c-1 f-w-bo">
                  <div className="w-auto flexbox h-auto h-pr-fl-ma r-h-c line-h-0-8em">
                    <span className="w-auto h-auto t-a-c flex-auto h-pr-fl-ma p-5px c-p h-e">
                      Términos y condiciones
                      <span className="m-d-n">de uso</span>
                    </span>
                    <span className="w-auto h-auto t-a-c flex-auto h-pr-fl-ma p-5px c-p h-e">
                      Políticas de privacidad
                    </span>
                    <span className="w-auto h-auto t-a-c flex-auto h-pr-fl-ma p-5px c-p h-e">
                      Políticas de cookies
                    </span>
                    <span className="w-auto h-auto t-a-c flex-auto h-pr-fl-ma p-5px c-p h-e">
                      Soporte de errores
                    </span>
                    <span className="w-auto h-auto t-a-c flex-auto h-pr-fl-ma p-5px c-p h-e">
                      Contacto
                    </span>
                    <span className="w-auto h-auto t-a-c flex-auto h-pr-fl-ma p-5px c-p h-e">
                      Empleo
                    </span>
                    <span className="w-auto h-auto t-a-c flex-auto h-pr-fl-ma p-5px c-p h-e">
                      Glosario
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="w-20 m-d-n h-100 small  h-pr-fl-ma">
            <div className="w-auto flexbox h-auto h-pr-fl-ma ns-bg-2 c-p-1 c-br-1 l-h-1em c-f-1 small centered">
              <Btn
                theme="ns-bg-1 ns-c-1"
                text="SOPORTE TÉCNICO"
                size="small"
                iconLeft="fas fa-bug"
              ></Btn>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Business
