import { Redirect, Route , Switch } from 'react-router-dom'
import { DashboardPage } from '../pages'

import {
  PersonEditProfile,
  PersonProfile,
  PersonPublications,
  PersonMyPostulations,
  ErrorView,
  Alerts,
  BusinessCreateInterview,
  BusinessEditProfile,
  BusinessProfile,
  BusinessPublications,
  CreatePost,
  Candidates,
  TemplatesInterviews,
} from '../components/organisms'

import {
  DASHBOARD_BASE,
  DASHBOARD_TYPE_BUSINESS,
  DASHBOARD_TYPE_PERSON,
  DASHBOARD_PERSON_EDITPROFILE,
  DASHBOARD_PERSON_MYPOSTULATIONS,
  DASHBOARD_PERSON_PROFILE,
  DASHBOARD_PERSON_PUBLICATIONS,
  DASHBOARD_PERSON_BASE,
  DASHBOARD_PERSON_ALERTS,
  DASHBOARD_BUSINESS_BASE,
  DASHBOARD_BUSINESS_EDITPROFILE,
  DASHBOARD_BUSINESS_PROFILE,
  DASHBOARD_BUSINESS_CREATEPOST,
  DASHBOARD_BUSINESS_CREATEINTERVIEW,
  DASHBOARD_BUSINESS_INTERVIEWS,
  DASHBOARD_BUSINESS_PUBLICATIONS,
  DASHBOARD_BUSINESS_CANDIDATES,
} from '../constants/routes.constant'

const DashboardRoutes = () => {
  return (
    <Switch>
      {/* Person */}
      <Route exact path={DASHBOARD_PERSON_EDITPROFILE} render={() => <DashboardPage MainContent={() => <PersonEditProfile />} viewType={DASHBOARD_TYPE_PERSON} />} />
      <Route exact path={DASHBOARD_PERSON_PROFILE} render={() => <DashboardPage MainContent={() => <PersonProfile />} viewType={DASHBOARD_TYPE_PERSON} />} />
      <Route exact path={DASHBOARD_PERSON_PUBLICATIONS} render={() => <DashboardPage MainContent={() => <PersonPublications />} viewType={DASHBOARD_TYPE_PERSON} />} />
      <Route exact path={DASHBOARD_PERSON_MYPOSTULATIONS} render={() => <DashboardPage MainContent={() => <PersonMyPostulations />} viewType={DASHBOARD_TYPE_PERSON} />} />
      <Route exact path={DASHBOARD_PERSON_ALERTS} render={() => <DashboardPage MainContent={() => <Alerts />} viewType={DASHBOARD_TYPE_PERSON} />} />
      <Route exact path={`${DASHBOARD_PERSON_BASE}/404`} render={() => <DashboardPage MainContent={() => <ErrorView redirect={DASHBOARD_PERSON_PUBLICATIONS} className="bg-red w-60 h-pr-fl-ma h-auto centered"></ErrorView>} viewType={DASHBOARD_TYPE_PERSON} />} />
      {/* Business */}
      <Route exact path={DASHBOARD_BUSINESS_EDITPROFILE} render={() => <DashboardPage MainContent={() => <BusinessEditProfile />} viewType={DASHBOARD_TYPE_BUSINESS} />} />
      <Route exact path={DASHBOARD_BUSINESS_PROFILE} render={() => <DashboardPage MainContent={() => <BusinessProfile />} viewType={DASHBOARD_TYPE_BUSINESS} />} />
      <Route exact path={DASHBOARD_BUSINESS_CREATEPOST} render={() => <DashboardPage MainContent={() => <CreatePost />} viewType={DASHBOARD_TYPE_BUSINESS} />} />
      <Route exact path={DASHBOARD_BUSINESS_CREATEINTERVIEW} render={() => <DashboardPage MainContent={() => <BusinessCreateInterview />} viewType={DASHBOARD_TYPE_BUSINESS} />} />
      <Route exact path={DASHBOARD_BUSINESS_INTERVIEWS} render={() => <DashboardPage MainContent={() => <TemplatesInterviews />} viewType={DASHBOARD_TYPE_BUSINESS} />} />
      <Route exact path={DASHBOARD_BUSINESS_PUBLICATIONS} render={() => <DashboardPage MainContent={() => <BusinessPublications />} viewType={DASHBOARD_TYPE_BUSINESS} />} />
      <Route exact path={DASHBOARD_BUSINESS_CANDIDATES} render={() => <DashboardPage MainContent={() => <Candidates />} viewType={DASHBOARD_TYPE_BUSINESS} />} />
      <Route exact path={`${DASHBOARD_BUSINESS_BASE}/404`} render={() => <DashboardPage MainContent={() => <ErrorView redirect={DASHBOARD_BUSINESS_PUBLICATIONS} className="bg-red w-60 h-pr-fl-ma h-auto centered"></ErrorView>} viewType={DASHBOARD_TYPE_BUSINESS} />} />
      {/* Redirects */}
      <Redirect exact path={DASHBOARD_PERSON_BASE} to={DASHBOARD_PERSON_PUBLICATIONS} />
      <Redirect path={`${DASHBOARD_PERSON_BASE}/**`} to={`${DASHBOARD_PERSON_BASE}/404`} />
      <Redirect exact path={DASHBOARD_BUSINESS_BASE} to={DASHBOARD_BUSINESS_PUBLICATIONS} />
      <Redirect path={`${DASHBOARD_BUSINESS_BASE}/**`} to={`${DASHBOARD_BUSINESS_BASE}/404`} />
      <Redirect exact path={DASHBOARD_BASE} to={DASHBOARD_PERSON_PUBLICATIONS} />
      <Redirect path={`${DASHBOARD_BASE}/**`} to={`${DASHBOARD_PERSON_BASE}/404`} />
    </Switch>
  )
}

export default DashboardRoutes
