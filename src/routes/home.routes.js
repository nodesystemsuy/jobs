import { Route , Switch , Redirect } from 'react-router-dom'
import { HomePage } from '../pages'

import {
  FormLogin,
  BusinessFormRegister,
  PersonFormRegister,
  PasswordChange,
  PasswordRecovery,
} from '../components/organisms'

import { //import routes constants
  HOME_BASE,
  HOME_LOGIN,
  HOME_PASSWORDCHANGE,
  HOME_PASSWORDRECOVERY,
  HOME_REGISTER_BUSINESS,
  HOME_REGISTER_PERSON,
} from '../constants/routes.constant'

const HomeRoutes = () => {
  return (
    <Switch>
      <Route exact path={HOME_REGISTER_BUSINESS} render={() => <HomePage HomeForm={BusinessFormRegister} />} />
      <Route exact path={HOME_REGISTER_PERSON} render={() => <HomePage HomeForm={PersonFormRegister} />} />
      <Route exact path={HOME_LOGIN} render={() => <HomePage HomeForm={FormLogin} />} />
      <Route exact path={HOME_PASSWORDRECOVERY} render={() => <HomePage HomeForm={PasswordRecovery} />} />
      <Route exact path={HOME_PASSWORDCHANGE} render={() => <HomePage HomeForm={PasswordChange}/>} />
      <Redirect exact path={HOME_BASE} to={HOME_REGISTER_PERSON} />
      <Redirect path="/home/**" to={HOME_REGISTER_PERSON} />
    </Switch>
  )
}
export default HomeRoutes
