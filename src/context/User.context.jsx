import React,{ createContext } from "react";
import { useMemo } from "react";
import { useState } from "react";

export const UserContext=createContext(null);

export const UserProvider =({children})=> {
    const [user,setUser]=useState("Kelvin");
    const userValue=useMemo(() => ({user,setUser},[user,setUser]))
    return (
        <UserContext.Provider value={{userValue}}>
            {children}
        </UserContext.Provider>
    );
}
