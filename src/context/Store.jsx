import React,{ createContext } from "react";

export const DataContext=createContext();

export const Store =({children})=> {
    return (
        <DataContext.Provider>
            {children}
        </DataContext.Provider>
    );
}
