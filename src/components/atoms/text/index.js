import React, { Fragment, Component } from "react";
import "./styles.css";
export default function Text(props) {
  const { theme, className, onClick, children, size, text, color, align } = props;

  return (
      <Fragment>
        <div className={`Text ${align} w-100 c-p-1  ${theme}  c-f-1 of-hidden c-br-1 h-pr-fl-ma ${className}`} onClick={onClick}>
        <span className={` ${size}  ${color}`}>{text}{children}</span>
        </div>
      </Fragment>
   );
  }
