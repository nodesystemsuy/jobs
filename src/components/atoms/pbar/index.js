import React, { Fragment } from "react";

import "./styles.css";

export default function ProgBar(props) {
  const {
    theme,
    size,
    className,
    value,
  } = props;

  return (
    <Fragment>
      <div

        className={` ProgBar h-e h-pr-fl-ma ns-bg-2  ${size} c-br-1 c-p-1 ${className} `}
      >
        <div className={` ProgLoad ${value} ${theme} h-100 c-br-2 h-pr-fl-ma `}></div>

      </div>

    </Fragment>
  );
}
