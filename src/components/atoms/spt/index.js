import React, { Fragment, Component } from "react";
import "./styles.css";
import {Btn} from "../index.js";

export default function Spt(props) {
  const { theme, className, onClick, text, align, center, left, right, font, lineColor, lineSize } = props;

  return (
      <Fragment>
        <div className={` ${align} w-100 c-p-1 ${theme} c-f-1 of-hidden c-br-1 h-pr-fl-ma ${className}`} onClick={onClick}>


        {left && <div className="  w-100 h-pr-fl-ma d-i-f flexbox h-auto">
      <div className="w-10 d-f h-pr-fl-ma h-auto"><div className={`w-100 h-pr-fl-ma ${lineColor} ${lineSize}`}>{""}</div></div>
      <div className={`w-auto d-f h-pr-fl-ma h-auto max-l-1 t-of-c of-hidden c-p-1 t-a-c ${font} `}>{text}</div>
      <div className="w-80 d-f h-pr-fl-ma h-auto"><div className={`w-100 h-pr-fl-ma ${lineColor} ${lineSize}`}>{""}</div></div>
      </div> }

        {center && <div className="  w-100 h-pr-fl-ma d-i-f flexbox h-auto">
      <div className="w-40 d-f h-pr-fl-ma h-auto"><div className={`w-100 h-pr-fl-ma ${lineColor} ${lineSize} `}>{""}</div></div>
      <div className={`w-80 d-f h-pr-fl-ma h-auto max-l-1 t-of-c of-hidden c-p-1 t-a-c ${font} `}>{text}</div>
      <div className="w-40 d-f h-pr-fl-ma h-auto"><div className={`w-100 h-pr-fl-ma ${lineColor} ${lineSize}`}>{""}</div></div>
      </div> }

      {right && <div className="  w-100 h-pr-fl-ma d-i-f flexbox h-auto">
      <div className=" w-80 d-f h-pr-fl-ma h-auto"><div className={`w-100 h-pr-fl-ma ${lineColor} ${lineSize} `}>{""}</div></div>
      <div className={`w-auto d-f h-pr-fl-ma h-auto max-l-1 t-of-c of-hidden c-p-1 t-a-c ${font} `}>{text}</div>
      <div className="w-10 d-f h-pr-fl-ma h-auto"><div className={`w-100 h-pr-fl-ma ${lineColor} ${lineSize}`}>{""}</div></div>
      </div> }

        </div>
      </Fragment>
   );
  }
