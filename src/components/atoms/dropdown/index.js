import React from "react";
import { Link } from 'react-router-dom'
import "./styles.css";
import { ProfilePicture } from "../../molecules";

export default function Dropdown(props) {
  const { icon, iconColor, iconRight, iconLeft, text, textSize, iconDropRight,iconDropLeft,size, counter, counterColor, counterSymbol, path, className, theme, counterSize, children } = props;

  return (
    <Link to={path}>
      <div className={` sidebar-btn ${size} ${className} ${theme} m-b-2px w-100 flexbox d-i-f h-auto b-s-b-b c-f-1 h-pr-fl-ma c-p h-e c-br-1 `}>
        {iconLeft && <div className="w-15 h-auto h-pr-fl-ma " >
          <div className="w-auto r-h-c c-p-1 h-auto small h-pr-fl-ma ">
            <i className={` ${iconLeft} ${iconColor}  small h-pr-fl-ma `}></i>
          </div>
        </div>}

        <div className="w-55 h-auto h-pr-fl-ma">
          <div className="w-auto h-auto h-pr-fl-ma of-hidden small">
            <div className={`sidebar-text ${textSize}  of-hidden l-h-1em t-of-e w-auto h-auto h-pr-fl-ma `}>
              {text}
            </div>
          </div>
        </div>

        <div className="w-20 h-auto h-pr-fl-ma">
          {counter && (
            <div className="w-auto h-auto r-h-c c-p-1 h-pr-fl-ma ">
              <div
                className={` ${counterColor} ${counterSize} bg-inh w-auto h-auto h-pr-fl-ma nano c-br-1 f-w-bo c-white p-l-3px p-r-3px  `}
              >
                {counterSymbol && { counterSymbol }}
                {counter}
              </div>
            </div>
          )}
        </div>

        {iconRight && <div className=" w-15 h-auto h-pr-fl-ma ">

          <div className="w-auto r-h-c c-p-1 h-auto small h-pr-fl-ma ">
            <i className={` ${iconRight} ${iconColor}  small h-pr-fl-ma `}></i>
          </div></div>
        }

        {iconDropRight && <div className=" w-15 h-auto h-pr-fl-ma ">
          <div className="w-auto r-h-c h-auto small h-pr-fl-ma">
            <span className={`w-auto h-auto small h-pr-fl-ma DropdownChevron`}></span>
          </div></div>
        }

      </div>
      <div className="w-100 h-auto small h-pr-fl-ma">
        {children}</div>
    </Link>
  );
}
