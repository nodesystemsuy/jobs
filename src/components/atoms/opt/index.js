import React, { Fragment } from "react";
import "./styles.css";

export default function Opt(props) {
  const { children, theme, text } = props;

  return (
    <Fragment>
      <option className={` Opt h-100 h-pr-fl-ma c-p c-f-1 ns-bg-1 ns-c-1 c-p-1 outline-none h-e ${theme} `} {...props}>{text}{children}</option>
    </Fragment>
  );
}
