import React, { Fragment } from "react";
import "./styles.css";


export default function Img(props) {
  const { className, children, srcImg, userIcon} = props;

  return (
    <Fragment>
        <div className={`${"image"} ${"hover-effect"} ${"h-pr-fl-ma"} ${className}`}>
          {userIcon && <div className="userIcon w-100 h-100 h-pr-fl-ma bg-yellow"><i className="fas fa-user-circle big"></i></div>}
          {srcImg && <div className={`${"w-100"} ${"h-100"} ${"h-pr-fl-ma"} `}><img src={`${srcImg}`} className="w-100 h-100 h-pr-fl-ma" alt="alt"/></div>}
          {children}
        </div>
      </Fragment>
    );
  }
