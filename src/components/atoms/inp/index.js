import React, { Fragment } from "react";
import "./styles.css";

import { Btn } from '../index.js';

export default function Inp(props) {
  const {
    error,
    helperText,
    info,
    orientation,
    theme,
    type,
    size,
    Name,
    className,
    iconLeft,
    iconRight,
    placeholder,
    alignText,
    width,
    children,
    SearchBtn,
    denied,
    onChange,
    onBlur

  } = props;

  return (
    <Fragment>

      <label

        className={`Inp InputContainer h-pr-fl-ma c-br-2 h-e ${orientation} ${theme} ${type} ${size} ${className} ${width} `} >

        {iconLeft && <div

          className={`InputIconLeft flex-auto b-s-b-b f-s-inh h-100 br-none b-s-n h-pr-fl-ma ns-c-1 ${size} ${theme} `}>
          <div className="w-100"><i className={` h-pr-fl-ma t-a-c r-h-c ${iconLeft}`}></i></div>
        </div>}

        <div className="Input flex-auto h-pr-fl-ma br-none b-s-n">
          <input name={`${Name}`} placeholder={`${placeholder}`} type={`${type}`}
            className={`  h-100 h-pr-fl-ma f-s-inh   br-none b-s-n c-p-1 ns-c-1 ${alignText} ${theme} ${className} ${width} `} />{children}
          {placeholder &&(
            <>
              {/* Si se detecta un icon left, el placeholder se movera 50px a la derecha(left) en la parte superior
                Si "no" se detecta un icon left el placeholder se movera solo a la parte superior(top) */}
              <span className={`${iconLeft ? 'Input__placeholder-left' : 'Input__placeholder-top' } p-a p-l-5px t-02s o-60`}>
                {placeholder}
              </span>
            </>
          )}
        </div>
        {iconRight &&
          <div

            className={` InputIconRight br-none b-s-n  flex-auto b-s-b-b f-s-inh h-100 h-pr-fl-ma ns-c-1 ${size} ${theme} `}>
            <i className={` w-100 h-100 h-pr-fl-ma t-a-c r-h-c ${iconRight} `}></i>
          </div>}
        {SearchBtn &&
          <Btn

            className={` InputIconRight ${size} ${theme}  flex-auto b-s-b-b f-s-inh h-100 h-pr-fl-ma ns-c-1 `}>
            <i className={` w-100 h-100 h-pr-fl-ma t-a-c r-h-c ${iconRight} `}></i>
          </Btn>}


      </label>
      {error && <div className="w-100 h-auto h-pr-fl-ma small c-p-1">
        <div className="w-100 alert h-auto h-pr-fl-ma c-br-1 c-f-1 c-p-1 small"><icon className="fas fa-exclamation-circle p-r-5px"></icon>{helperText}</div></div>}
      {info && <div className="w-100 h-auto h-pr-fl-ma small c-p-1">
        <div className="w-100 info h-auto h-pr-fl-ma c-br-1 c-f-1 c-p-1 small"><icon className="fas fa-info-circle p-r-5px"></icon>{helperText}</div></div>}

    </Fragment>
  );
}
