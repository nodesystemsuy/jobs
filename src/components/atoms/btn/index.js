import React, { Fragment } from "react";
import { Link } from 'react-router-dom'
import "./styles.css";

export default function Btn(props) {
  const {
    type,
    theme,
    size,
    className,
    iconLeft,
    icon,
    path,
    text,
    children,
    iconRight,
    onClick
  } = props;

  return (
    <>
    <Link to={path} >
      <div
        type={`${type} `}
        onClick={onClick}
        className={`Btn w-inh h-e h-pr-fl-ma ${theme} ${size} ${className}`}
      >
        {iconLeft && <i className={` BtnIconLeft ${iconLeft}`}></i>}
        <div className="BtnText ">
          {icon && <i className={` BtnIcon ${icon} `}></i>}
          {text && <span className={`c-inh ${size} `}>{text}</span>}
          {children && <span className="h-pr-fl-ma">{children}</span>}
        </div>
        {iconRight && <i className={` BtnIconRight ${iconRight} `}></i>}

      </div>
    </Link>
    </>
  );
}
