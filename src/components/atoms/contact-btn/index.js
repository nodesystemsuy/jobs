import React from "react";
import "./styles.css";
import { Link } from 'react-router-dom'

import { ProfilePicture } from "../../molecules";

export default function ContactBtn(props) {
  const {  text, textSize, size, counter, counterColor, counterSymbol, path, className, theme, counterSize, } = props;

  return (
    <Link to={path}>
      <div className={` contact-btn ${size} ${className} ${theme} m-b-2px w-100 flexbox d-i-f h-auto b-s-b-b c-f-1 h-pr-fl-ma c-p h-e c-br-1 `}>

        <div className="w-15 h-auto h-pr-fl-ma c-p-1 " >
          <ProfilePicture className="w-and-h-30px r-h-c h-pr-fl-ma"></ProfilePicture>
        </div>

        <div className="w-55 h-auto h-pr-fl-ma c-p-1">
          <div className="w-auto h-auto h-pr-fl-ma of-hidden small">
            <div className={`contact-text ${textSize}  of-hidden l-h-1em t-of-e w-auto h-auto h-pr-fl-ma `}>
              {text}
            </div>
          </div>
        </div>

        <div className="w-20 h-auto h-pr-fl-ma">
          {counter && (

           <div className="w-auto h-auto r-h-c c-p-1 h-pr-fl-ma ">
              <div
                className={` ${counterColor} ${counterSize} bg-inh w-auto h-auto h-pr-fl-ma nano c-br-1 f-w-bo c-white p-l-3px p-r-3px  `}
              >
                {counterSymbol && { counterSymbol }}
                {counter}
              </div>
            </div>
          )}
        </div>

      </div>
    </Link>
  );
}
