export {default as Btn} from '../atoms/btn/index.js';
export {default as CardHorizontal} from '../atoms/card-horizontal/index.js';
export {default as Checkbox} from '../atoms/checkbox/index.js';
export {default as Img} from '../atoms/img/index.js';
export {default as SidebarBtn} from '../atoms/sidebar-btn/index.js';
export {default as Dropdown} from '../atoms/dropdown/index.js';

export {default as ContactBtn} from '../atoms/contact-btn/index.js';

export {default as Spt} from './spt/index.js';


export {default as DatList} from './datlist/index.js';
export {default as Sct} from './sct/index.js';
export {default as Opt} from '../atoms/opt/index.js';

export {default as ProgBar} from '../atoms/pbar/index.js';
export {default as Text} from '../atoms/text/index.js';
export {default as Tag} from '../atoms/tag/index.js';
export {default as Pag} from '../atoms/pag/index.js';
export {default as Inp} from '../atoms/inp/index.js';

