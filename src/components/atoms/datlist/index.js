import React, { Fragment } from "react";
import "./styles.css";

export default function DatList(props) {
  const { orientation, theme, type, size, name, text, className, iconLeft, iconRight, placeholder, alignText, width, children, SearchBtn,} = props;

  return (
    <Fragment>

<label

    className={` Sct Inp InputContainer h-pr-fl-ma c-br-2 of-hidden ${orientation} ${theme} ${type} ${size} ${className} ${width} `} >

   {iconLeft && <div

        className={`InputIconLeft flex-auto b-s-b-b f-s-inh h-100 h-pr-fl-ma ns-c-1 ${size} ${theme} `}>
       <span className="w-100"><i className={`h-pr-fl-ma t-a-c centered ${iconLeft}`}></i></span>
    </div>}
    <div className="Input select flex-auto h-pr-fl-ma">
    <input list="datalist"  name={`${name}`} placeholder={`${placeholder}`} type={`${type}`} className={`w-100 bg-inh h-100 h-pr-fl-ma f-s-inh h-e ${alignText} ${theme} ${className} ${width} `}/>
        <datalist id="datalist" className={`w-100 h-100 h-pr-fl-ma f-s-inh h-e ${alignText} ${theme} ${className} ${width} `}
          >{children}</datalist><div className="select_arrow z-i-99"></div>
    </div>
    {iconRight &&
    <div

        className={` InputIconRight  flex-auto b-s-b-b f-s-inh h-100 h-pr-fl-ma ns-c-1 ${size} ${theme} `}>
        <i className={` w-100 h-100 h-pr-fl-ma t-a-c r-h-c ${iconRight} `}></i>
    </div>}


</label>
</Fragment>
  );
}
