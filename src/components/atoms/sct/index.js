import React, { Fragment } from "react";
import "./styles.css";

export default function Sct(props) {
  const { orientation, theme, type, size, Name, className, iconLeft, iconRight, placeholder, alignText, width, children, SearchBtn,} = props;

  return (
    <Fragment>

<label

    className={` Sct Inp InputContainer  h-pr-fl-ma c-br-2 of-hidden ${orientation} ${theme} ${type} ${size} ${className} ${width} `} >

   {iconLeft && <div

        className={`InputIconLeft flex-auto ns-c-1 b-s-b-b f-s-inh h-100 h-pr-fl-ma ${size} ${theme} `}>
       <span className="w-100"><i className={`h-pr-fl-ma t-a-c centered ${iconLeft}`}></i></span>
    </div>}

    <div className="Input select flex-auto ns-c-1 h-pr-fl-ma">
        <select name={`${Name}`} placeholder={`${placeholder}`} type={`${type}`}
            className={`h-100 h-pr-fl-ma f-s-inh ns-c-1  h-e ${alignText} ${theme} ${className} ${width} `}>{children}</select><div className="select_arrow z-i-99"></div>
    </div>
    {iconRight &&
    <div

        className={` InputIconRight  ns-c-1 flex-auto b-s-b-b f-s-inh h-100 h-pr-fl-ma  ${size} ${theme} `}>
        <i className={` w-100 h-100 h-pr-fl-ma t-a-c r-h-c ${iconRight} `}></i>
    </div>}


</label>
</Fragment>
  );
}
