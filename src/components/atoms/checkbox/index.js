import React, { Fragment } from "react";

import "./styles.css";

export default function Checkbox(props) {
  const { theme, className, size, text,onChange, onBlur,Name } = props;

  return (
    <Fragment>
      <div className={`c-br-1 h-auto h-pr-fl-ma ${theme} ${className}`}>
        <label className={`checkbox checkbox-checkbox ${size}`}>
          <input type="checkbox" className="r-h-c" onChange={onChange} onBlur={onBlur} name={Name}/>
          <div className={`checkbox_indicator ${size}  ${theme}`}></div>
          {text && (
            <span
              className={` checkbox-text ${size}  ns-c-1 h-pr-fl-ma c-f-1 p-l-5px m-r-5px `}
            >{text}
            </span>
          )}
        </label>
      </div>
    </Fragment>
  );
}
