import React, { Fragment } from "react";
import { Text, ContactBtn } from "../../atoms/index.js";
import { Alert } from '../../molecules/index.js';
import "./styles.css";

const Alerts = (props) => {
  const { } = props;

  return (
    <Fragment>

      <div className="w-100 h-100 h-pr-fl-ma  ns-bg-1 c-br-2 c-p-1 of-x-hidden of-y-auto">
        <div className="w-100 h-10 h-pr-fl-ma">
          <Text className="w-100 h-auto  f-w-bo" text="ALERTAS" theme="alert" size="normal" align="t-a-c"></Text>
        </div>
        <div className="w-30 h-100 h-pr-fl-ma c-p-1">
          <div className="w-100 h-auto m-b-5px h-pr-fl-ma ns-bg-2 c-br-1 c-p-1">
            <Text className="w-100 h-auto f-w-bo ns-c-1" text="USUARIO" theme="ns-bg-1" size="normal" align="t-a-c"></Text>
          </div>

          <div className="w-100 h-auto h-pr-fl-ma ns-bg-2 c-p-1 c-br-1" align="center">

            <ContactBtn className="f-w-bo ns-bg-1" size="small" counterColor="alert" counter="+123" text="NOMBRE EMPRESA"></ContactBtn>
            <ContactBtn className="f-w-bo ns-bg-1" size="small" counterColor="alert" counter="+123" text="NOMBRE EMPRESA"></ContactBtn>
            <ContactBtn className="f-w-bo ns-bg-1" size="small" counterColor="alert" counter="+123" text="NOMBRE EMPRESA"></ContactBtn>
            <ContactBtn className="f-w-bo ns-bg-1" size="small" counterColor="alert" counter="+123" text="NOMBRE EMPRESA"></ContactBtn>
            <ContactBtn className="f-w-bo ns-bg-1" size="small" counterColor="alert" counter="+123" text="NOMBRE EMPRESA"></ContactBtn>
            <ContactBtn className="f-w-bo ns-bg-1" size="small" counterColor="alert" counter="+123" text="NOMBRE EMPRESA"></ContactBtn>

          </div>

        </div>
        <div className="w-70 h-100 h-pr-fl-ma c-p-1">
          <div className="w-100 h-auto m-b-5px h-pr-fl-ma ns-bg-2 c-br-1 c-p-1">
            <Text className="w-100 h-auto f-w-bo ns-c-1" text="NOTIFICACION" theme="ns-bg-1" size="normal" align="t-a-c"></Text>
          </div>
          <div className="w-100 h-90 small h-pr-fl-ma ns-bg-2 c-p-1 c-br-1">
            <Alert title="REUNIÓN MAÑANA" description="HA ACEPTADO TU ENTREVISTA"></Alert>
            <Alert title="REUNION EL LUNES" description="HA ACEPTADO TU ENTREVISTA"></Alert>
            <Alert
              title="NOMBRE APELLIDO"
              description="Sunt ex cupidatat Lorem fugiat pariatur minim ad non. Aliqua et nulla duis occaecat Lorem nisi. Magna adipisicing incididunt laboris Lorem do occaecat occaecat nostrud anim culpa excepteur adipisicing proident. Velit magna cupidatat laboris officia excepteur Lorem. Ex ut consequat labore incididunt sunt.

Id aliquip anim laboris velit dolore duis do laboris tempor qui officia id cupidatat. Lorem nisi laborum est tempor laborum consectetur consectetur. Aliquip minim in pariatur laborum tempor non est officia culpa quis enim ut et labore."></Alert>
            <Alert title="NUEVO CONTRATO" description="HA ACEPTADO TU ENTREVISTA"></Alert>
            <Alert
              title="NOMBRE APELLIDO"
              description="Sunt ex cupidatat Lorem fugiat pariatur minim ad non. Aliqua et nulla duis occaecat Lorem nisi. Magna adipisicing incididunt laboris Lorem do occaecat occaecat nostrud anim culpa excepteur adipisicing proident. Velit magna cupidatat laboris officia excepteur Lorem. Ex ut consequat labore incididunt sunt.

Id aliquip anim laboris velit dolore duis do laboris tempor qui officia id cupidatat. Lorem nisi laborum est tempor laborum consectetur consectetur. Aliquip minim in pariatur laborum tempor non est officia culpa quis enim ut et labore."></Alert>
            <Alert title="NOMBRE APELLIDO" description="HA ACEPTADO TU ENTREVISTA"></Alert>
            <Alert title="NOMBRE APELLIDO" description="HA ACEPTADO TU ENTREVISTA"></Alert>
          </div></div>
      </div>
    </Fragment>
  );
}

export default Alerts;
