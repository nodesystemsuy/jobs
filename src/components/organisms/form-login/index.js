import React, { Fragment } from "react";
import "./styles.css";

import { Btn, Inp, Text, } from "../../atoms/index.js";
import { DataBox } from "../../molecules/index.js";


const FormLogin = () => {
    return (
      <Fragment>
        {/*

? ██       ██████   ██████  ██ ███    ██     ██    ██ ███████ ███████ ██████
? ██      ██    ██ ██       ██ ████   ██     ██    ██ ██      ██      ██   ██
? ██      ██    ██ ██   ███ ██ ██ ██  ██     ██    ██ ███████ █████   ██████
? ██      ██    ██ ██    ██ ██ ██  ██ ██     ██    ██      ██ ██      ██   ██
? ███████  ██████   ██████  ██ ██   ████      ██████  ███████ ███████ ██   ██

 */}

        <form className="w-100 h-100 c-p-1 of-hidden b-s-b-b h-pr-fl-ma">

          <div className="w-80 m-w-70 h-auto h-pr-fl-ma centered">
            <div className="w-100 h-auto of-hidden c-p-1 ns-bg-1 ns-b-s c-br-1 h-pr-fl-ma">
              <div className="w-100 h-auto h-pr-fl-ma ns-bg-2 c-br-1 m-b-5px c-p-1">
            <DataBox text="INGRESAR" textStyle="ns-bg-1 small "></DataBox></div>
              {/* EMAIL or NICKNAME LOGIN */}
              <div className="w-100 h-auto h-pr-fl-ma c-br-1 ns-bg-2 c-p-1">
              <DataBox childrenStyle="m-b-5px ns-bg-1" className="small">
                <Inp
                    className="w-100"
                    iconLeft="fas fa-envelope"
                    theme="ns-bg-1 ns-c-1"
                    type="text"
                    size="normal"
                    placeholder="Email ó Username"
                  />
                </DataBox>
                <DataBox childrenStyle="ns-bg-1" className="small" >
                <Inp
                    className="w-100"
                    iconLeft="fas fa-asterisk"
                    theme="ns-bg-1 ns-c-1"
                    type="text"
                    size="normal"
                    placeholder="Password"
                  />
                </DataBox>
              {/* PASSWORD LOGIN */}

             </div>
              <div className="w-100 h-auto h-pr-fl-ma c-p-1">
           <div className="w-100 h-100 h-pr-fl-ma">
                <Btn
                  theme="info"
                  iconLeft="fas fa-sign-in-alt"
                  className="w-100 c-p-1"
                  size="normal"
                  text="Ingresar"
                ></Btn>
               </div>
              </div>

            </div>


            <div className="w-100 h-auto of-hidden flex-auto c-p-1 small h-pr-fl-ma">
              <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">

                <Btn
                  size="small"
                  theme="white"
                  text="Ingresar con Google"
                  className="w-100"
                  iconLeft="fab fa-google"
                ></Btn>
              </div>
              <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                <Btn
                  size="small"
                  theme="white"
                  text="Ingresar con Facebook"
                  className="w-100"
                  iconLeft="fab fa-facebook"
                ></Btn>

              </div>
            </div>

          </div>
        </form>
      </Fragment>
    );
}

export default FormLogin
