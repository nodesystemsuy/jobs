import React, { Fragment } from "react";
import { Link } from 'react-router-dom'
import "./styles.css";

import { ReactComponent as ErrorViewSvg } from "../../../assets/images/errorview.svg";
import { Btn, Text } from "../../atoms/index.js";

const ErrorView = (props) => {
  const { redirect } = props

  return (
    <Link to={redirect}>
      <div className="w-100 centered h-auto h-pr-fl-ma ">
        <div className="w-100 h-auto h-pr-fl-ma ">
          <ErrorViewSvg className="w-200px h-auto r-h-c"></ErrorViewSvg></div>
        <div className="w-100 h-auto h-pr-fl-ma c-p-1 m-b-5px">
          <Text text="OOPS!! ALGO MALIÓ SAL" size="title" align="t-a-c" ></Text>
          <Text text="Volviendo al sitio anterior en: 3..2..1" size="small" align="t-a-c" ></Text>

        </div>
        <div className="w-40 m-w-70 r-h-c h-auto h-pr-fl-ma">
          <Btn iconLeft="fas fa-caret-left" theme="alert" className="w-100 r-h-c" size="small" text="VOLVER"></Btn>
        </div> </div>
    </Link>
  );
}

export default ErrorView
