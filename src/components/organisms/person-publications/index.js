import React, { Fragment } from "react";

import { SidebarBtn } from "../../atoms/index.js";

import {
  Publication,
  PublicationFilter,
  Pagination,
} from "../../molecules/index.js";
import "./styles.css";

const PersonPublications = () => {
    return (
      <Fragment>
        <div className="w-100 h-100 h-pr-fl-ma ns-bg-1 c-p-1">
        <div className="w-100 h-10 h-pr-fl-ma b-s-b-b c-p-1 ">
        <div className="w-100 h-auto h-pr-fl-ma d-i-f r-v-c ns-bg-2 c-br-1 m-b-5px">
          <div className="w-25 h-auto h-pr-fl-ma c-p-1"><SidebarBtn text="URGENTE" textSize="small f-w-bo p-l-5px" theme="alert" iconRight="fas fa-exclamation-circle"></SidebarBtn></div>
          <div className="w-25 h-auto h-pr-fl-ma c-p-1"><SidebarBtn text="TODOS" textSize="small f-w-bo p-l-5px" theme="default" iconRight="fas fa-globe-americas"></SidebarBtn></div>
          <div className="w-25 h-auto h-pr-fl-ma c-p-1"><SidebarBtn text="ANTIGUAS" textSize="small f-w-bo p-l-5px" theme="default" iconRight="fas fa-history"></SidebarBtn></div>
          <div className="w-25 h-auto h-pr-fl-ma c-p-1"><SidebarBtn text="PRIORIDAD" textSize="small f-w-bo p-l-5px" theme="bg-blue-node c-white" iconRight="fas fa-wheelchair"></SidebarBtn></div>
        </div>
        </div>

          <div className="w-100 h-80 h-pr-fl-ma small of-y-auto of-x-none ns-bg-2 c-br-1 c-p-1">
            <Publication></Publication>
            <Publication></Publication>
            <Publication></Publication>
            <Publication></Publication>
          </div>
          <div className="w-100 h-10 h-pr-fl-ma c-p-1 b-s-b-b">

<Pagination className="w-100 h-pr-fl-ma r-v-c"></Pagination>

</div>
        </div>
      </Fragment>
    );
}

export default PersonPublications
