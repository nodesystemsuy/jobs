import React, { Fragment } from "react";
import "./styles.css";

import { Btn, Inp, Sct, Opt, Spt } from "../../atoms/index.js";
import { ProfilePicture, ProfileCoverPage, Notify, DataBox } from "../../molecules/index.js";

const BusinessEditProfile = () => {
  return (
    <Fragment>
      <div className="w-100 h-100 h-pr-fl-ma c-br-2 small c-p-2 of-x-hidden of-y-auto">
        <div className="w-100 h-auto flexbox h-pr-fl-ma d-i-f">
          <ProfileCoverPage className="h-100 c-br-1 of-hidden bg-op-10"></ProfileCoverPage>
          <div className="w-20 h-auto flex-auto h-pr-fl-ma c-p-1 b-s-b-b ">
              <div className="w-auto m-w-100 ns-bg-2 c-br-3 ns-c-1 h-auto h-pr-fl-ma r-h-c">
              <ProfilePicture className="w-and-h-125px m-w-80px m-h-80px "></ProfilePicture>
            </div>
          </div>
          <div className="w-50 h-auto flex-auto d-i-f h-pr-fl-ma c-p-2 c-br-2">
            <div className="w-100 h-auto h-pr-fl-ma ns-c-1">
              <div className="w-100 h-auto h-pr-fl-ma c-f-1 c-p-1 f-w-bo m-b-3px">
              <Inp placeholder="NOMBRE FANTASÍA" size="medium" theme="ns-bg-2"></Inp>
              </div>
              <div className="w-100 h-auto h-pr-fl-ma c-f-1 c-p-1 normal">
                <Inp placeholder="SLOGAN DE SU EMPRESA" theme="ns-bg-2"></Inp>

              </div>
            </div>
          </div>
          <div className="w-30 h-auto flex-auto h-pr-fl-ma c-p-1 b-s-b-b ">
          <Notify theme="ns-bg-1 ns-c-1 ns-b-s" iconSize="medium" icon="fas fa-check" iconColor="success" title="Tus cambios se han guardado correctamente" titleSize="nano"></Notify>
          </div>
        </div>

        <div className="w-100 h-auto small h-pr-fl-ma c-p-1 ns-bg-2 c-p-1 c-br-1">
        <DataBox text="DATOS BASICOS" textStyle="m-b-5px t-a-c ns-c-e-c ns-bg-e"></DataBox>

          {/*
TODO ██ ██████  ███████ ███    ██ ████████ ██ ██████   █████  ██████ 
TODO ██ ██   ██ ██      ████   ██    ██    ██ ██   ██ ██   ██ ██   ██ 
TODO ██ ██   ██ █████   ██ ██  ██    ██    ██ ██   ██ ███████ ██   ██ 
TODO ██ ██   ██ ██      ██  ██ ██    ██    ██ ██   ██ ██   ██ ██   ██ 
TODO ██ ██████  ███████ ██   ████    ██    ██ ██████  ██   ██ ██████ 
        */}
          <div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w ns-bg-1 c-br-1 m-b-5px c-p-1 b-s-b-b ns-b-s ">
            <div className="w-10 m-w-100 m-d-b h-100 flex-auto h-pr-fl-ma c-p-1">
              <div className="w-40px h-40px h-pr-fl-ma r-h-c circle c-p-1 b-s-b-b caution">
                {/* ? COLOR DEL INDICADOR   */}
                <i className="fas fa-exclamation c-white centered "></i>
                {/* ? ICONO DEL INDICADOR  */}
              </div>
            </div>
            <div className="w-90 m-w-100 m-d-b h-auto flex-auto h-pr-fl-ma ns-bg-2 b-s-b-b c-p-1 c-br-1">
            <DataBox text="IDENTIDAD" textStyle="m-b-5px t-a-c ns-c-e"></DataBox>

              {/* TODO |                                  | NOMBRE EMPRESA + RAZÓN SOCIAL |  */}
              <div className=" w-100 h-auto h-pr-fl-ma c-p-1">
                <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="NOMBRE FANTASÍA:" titleStyle="m-b-5px">
                    <Inp
                      className="w-100"
                      iconLeft="fas fa-signature"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                      placeholder="NOMBRE DE SU EMPRESA"
                    />
                  </DataBox>
                </div>

                <div className="w-50  h-auto h-pr-fl-ma c-p-1">
                <DataBox title="RAZÓN SOCIAL:" titleStyle="m-b-5px">
                    <Inp
                      className="w-100"
                      iconLeft="fas fa-registered"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                      placeholder="RAZON SOCIAL"
                    />
                  </DataBox>
                </div>
              </div>

              {/* TODO |                                  | RUT DE EMPRESA + FECHA DE FUNDACIÓN |  */}
              <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="RUT/RUC DE SU EMPRESA:" titleStyle="m-b-5px">
                    <Inp
                      className="w-100"
                      iconLeft="fas fa-file-contract"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                      placeholder="IDENTIFICADOR RUT"
                    />
                  </DataBox>
                </div>
                <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="FECHA DE FUNDACIÓN:" titleStyle="m-b-5px">
                    <Inp
                      className="w-100"
                      iconLeft="fas fa-calendar-day"
                      theme="ns-bg-1"
                      orientation=""
                      type="date"
                      size="small"
                      placeholder=""
                    />
                  </DataBox>
                </div>
              </div>
            </div>
          </div>

          {/*

TODO ██   ██  ██████  ██████   █████  ██████  ██  ██████  ███████ 
TODO ██   ██ ██    ██ ██   ██ ██   ██ ██   ██ ██ ██    ██ ██      
TODO ███████ ██    ██ ██████  ███████ ██████  ██ ██    ██ ███████ 
TODO ██   ██ ██    ██ ██   ██ ██   ██ ██   ██ ██ ██    ██      ██ 
TODO ██   ██  ██████  ██   ██ ██   ██ ██   ██ ██  ██████  ███████

            */}
         <div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w ns-bg-1 c-br-1 m-b-5px c-p-1 b-s-b-b ns-b-s ">
            <div className="w-10 m-w-100 m-d-b h-100 flex-auto h-pr-fl-ma c-p-1">
              <div className="w-40px h-40px h-pr-fl-ma r-h-c circle c-p-1 b-s-b-b caution">
                {/* ? COLOR DEL INDICADOR   */}
                <i className="fas fa-exclamation c-white centered"></i>
                {/* ? ICONO DEL INDICADOR   */}
              </div>
            </div>
            <div className="w-90 m-w-100 m-d-b h-auto flex-auto h-pr-fl-ma ns-bg-2 b-s-b-b c-p-1 c-br-1">
            <DataBox text="HORARIOS" textStyle="m-b-5px t-a-c ns-c-e"></DataBox>
              {/* TODO |                                  | HORA DE APERTURA + DÍA DE APERTURA |  */}
              <div className="w-100 h-auto h-pr-fl-ma c-p-1">

              <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="DÍA DESDE:" titleStyle="m-b-5px">
                    <Sct
                      className="w-100"
                      iconLeft="fas fa-calendar"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                    >
                      <Opt text="DESDE" selected hidden />
                      <Opt text="SOLO LOS" />
                      <Opt text="LUNES" />
                      <Opt text="MARTES" />
                      <Opt text="MIERCOLES" />
                      <Opt text="JUEVES" />
                      <Opt text="VIERNES" />
                      <Opt text="SABADO" />
                      <Opt text="DOMINGO" />
                    </Sct>
                    </DataBox>
                </div>
                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="DÍA HASTA:" titleStyle="m-b-5px">
                    <Sct
                      className="w-100"
                      iconLeft="fas fa-calendar"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                    >
                      <Opt text="HASTA" selected hidden />
                      <Opt text="LUNES" />
                      <Opt text="MARTES" />
                      <Opt text="MIERCOLES" />
                      <Opt text="JUEVES" />
                      <Opt text="VIERNES" />
                      <Opt text="SABADO" />
                      <Opt text="DOMINGO" />
                    </Sct>
                    </DataBox>
                </div>
                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="HORA APERTURA:" titleStyle="m-b-5px">
                    <Inp
                      className="w-100"
                      iconLeft="fas fa-door-open"
                      theme="ns-bg-1"
                      orientation=""
                      type="time"
                      size="small"
                      placeholder=""
                    />
                 </DataBox>
                </div>
                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="HORA CIERRE:" titleStyle="m-b-5px">
                    <Inp
                      className="w-100"
                      iconLeft="fas fa-door-closed"
                      theme="ns-bg-1"
                      orientation=""
                      type="time"
                      size="small"
                      placeholder=""
                    />
                   </DataBox>
                </div>




                <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                  <Spt
                    text="¿Tienes más Horarios?"
                    center
                    lineSize="h-2px"
                    font="c-f-1 f-w-bo"
                    lineColor="bg-skyblue-node"
                  ></Spt>
                  <div className="w-auto r-h-c h-pr-fl-ma">
                  <Btn
                    className="w-auto "
                    theme="ns-bg-1"
                    iconLeft="fas fa-plus"
                    size="small"
                    text="AÑADIR HORARIOS"
                  ></Btn>
                   </div>
                </div>
              </div>
            </div>
          </div>
          {/*

TODO  ██    ██ ██████  ██  ██████  █████   ██████ ██  ██████  ███    ██ 
TODO  ██    ██ ██   ██ ██ ██      ██   ██ ██      ██ ██    ██ ████   ██ 
TODO  ██    ██ ██████  ██ ██      ███████ ██      ██ ██    ██ ██ ██  ██ 
TODO  ██    ██ ██   ██ ██ ██      ██   ██ ██      ██ ██    ██ ██  ██ ██ 
TODO   ██████  ██████  ██  ██████ ██   ██  ██████ ██  ██████  ██   ████ 

            */}
          {/* TODO |                                  | PAÍS DE ORIGEN + CIUDAD DE ORIGEN |  */}
          <div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w ns-bg-1 c-br-1 m-b-5px c-p-1 b-s-b-b ns-b-s ">
            <div className="w-10 m-w-100 m-d-b h-100 flex-auto h-pr-fl-ma c-p-1">
              <div className="w-40px h-40px h-pr-fl-ma r-h-c circle c-p-1 b-s-b-b caution">
                <i className="fas fa-exclamation c-white centered"></i>
              </div>
            </div>
            <div className="w-90 m-w-100 m-d-b h-auto flex-auto h-pr-fl-ma ns-bg-2 b-s-b-b c-p-1 c-br-1">
            <DataBox text="UBICACIÓN" textStyle="m-b-5px t-a-c ns-c-e"></DataBox>
              <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="PAÍS:" titleStyle="m-b-5px">
                    <Inp
                      className="w-100"
                      iconLeft="fas fa-flag"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                      placeholder="PAIS"
                    />
                  </DataBox>
                </div>
                <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="CIUDAD:" titleStyle="m-b-5px">
                    <Inp
                      className="w-100"
                      iconLeft="fas fa-map-marked-alt"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                      placeholder="CIUDAD"
                    />
                 </DataBox>
                </div>
              </div>
              {/* TODO |                                  | DIRECCIÓN DE EMPRESA |  */}
              <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="BARRIO:" titleStyle="m-b-5px">
                    <Inp
                      className="w-100"
                      iconLeft="fas fa-map"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                      placeholder="BARRIO"
                    />
                 </DataBox>
                </div>
                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="CALLE:" titleStyle="m-b-5px">
                    <Inp
                      className="w-100"
                      iconLeft="fas fa-road"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                      placeholder="CALLE"
                    />
                  </DataBox>
                </div>
                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="INTERSECCIÓN:" titleStyle="m-b-5px">
                    <Inp
                      className="w-100"
                      iconLeft="fas fa-directions"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                      placeholder="INTERSECCIÓN"
                    />
                  </DataBox>
                </div>
                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="SUCURSAL:" titleStyle="m-b-5px">
                    <Inp
                      className="w-100"
                      iconLeft="fas fa-store-alt"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                      placeholder="SUCURSAL"
                    />
                  </DataBox>
                </div>
              </div>
              {/* TODO |                                  | DIRECCIÓN DE EMPRESA |  */}
              <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="PISO:" titleStyle="m-b-5px">
                    <Inp
                      className="w-100"
                      iconLeft="fas fa-building"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                      placeholder="EDIFICIO"
                    />
                  </DataBox>
                </div>
                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="NUMERO DE PUERTA:" titleStyle="m-b-5px">
                    <Inp
                      className="w-100"
                      iconLeft="fas fa-door-closed"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                      placeholder="N° PUERTA"
                    />
                  </DataBox>
                </div>
                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="CÓDIGO POSTAL:" titleStyle="m-b-5px">
                    <Inp
                      className="w-100"
                      iconLeft="fas fa-mail-bulk"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                      placeholder="CÓDIGO POSTAL"
                    />
                   </DataBox>
                </div>
                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="UBICACIÓN ACTUAL:" titleStyle="m-b-5px">
                    <Btn
                      className="w-100"
                      iconLeft="fas fa-map-marker-alt"
                      theme="ns-bg-e ns-c-e-c f-w-bo"
                      orientation=""
                      size="small"
                      text="ACTUALIZAR"
                    />
                   </DataBox>
                </div>
              </div>
            </div>
          </div>
          {/*

TODO  ██████  ██████  ███    ██ ████████  █████   ██████ ████████  ██████ 
TODO ██      ██    ██ ████   ██    ██    ██   ██ ██         ██    ██    ██ 
TODO ██      ██    ██ ██ ██  ██    ██    ███████ ██         ██    ██    ██ 
TODO ██      ██    ██ ██  ██ ██    ██    ██   ██ ██         ██    ██    ██ 
TODO  ██████  ██████  ██   ████    ██    ██   ██  ██████    ██     ██████ 


            */}
          <div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w ns-bg-1 c-br-1 m-b-5px c-p-1 b-s-b-b ns-b-s ">
            <div className="w-10 m-w-100 m-d-b h-100 flex-auto h-pr-fl-ma c-p-1">
              <div className="w-40px h-40px h-pr-fl-ma r-h-c circle c-p-1 b-s-b-b caution">
                <i className="fas fa-exclamation c-white centered"></i>
              </div>
            </div>
            <div className="w-90 m-w-100 m-d-b h-auto flex-auto h-pr-fl-ma ns-bg-2 b-s-b-b c-p-1 c-br-1">
            <DataBox text="CONTACTO" textStyle="m-b-5px t-a-c ns-c-e"></DataBox>
              {/* TODO |                                  | REDES SOCIALES |  */}
              <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="LINKEDIN:" titleStyle="m-b-5px">
                    <Inp
                      className="w-100"
                      iconLeft="fab fa-linkedin"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                      placeholder="USERNAME"
                    />
                  </DataBox>
                </div>
                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="TWITTER:" titleStyle="m-b-5px">
                    <Inp
                      className="w-100"
                      iconLeft="fab fa-twitter"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                      placeholder="USERNAME"
                    />
                  </DataBox>
                </div>
                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="INSTAGRAM:" titleStyle="m-b-5px">
                    <Inp
                      className="w-100"
                      iconLeft="fab fa-instagram"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                      placeholder="USERNAME"
                    />
                  </DataBox>
                </div>


                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="FACEBOOK:" titleStyle="m-b-5px">
                    <Inp
                      className="w-100"
                      iconLeft="fab fa-facebook"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                      placeholder="USERNAME"
                    />
                  </DataBox>
                </div> </div>

              <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-70 m-w-50 h-auto h-pr-fl-ma">
                  <DataBox title="TELÉFONO:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100"
                        iconLeft="fas fa-phone"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="+000-000-000-000"
                      />
                    </DataBox>
                  </div>
                  <div className="w-30 m-w-50 h-auto h-pr-fl-ma">
                  <DataBox title="INTERNO:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100"
                        iconLeft="fas fa-phone"
                        theme="ns-bg-1"
                        orientation=""
                        type="number"
                        size="small"
                        placeholder="0000"
                      />
                     </DataBox>
                  </div>
                </div>

                <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-70 m-w-50 h-auto h-pr-fl-ma">
                  <DataBox title="TELÉFONO 2:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100"
                        iconLeft="fas fa-phone"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="+000-000-000-000"
                      />
                    </DataBox>
                  </div>

                  <div className="w-30 m-w-50 h-auto h-pr-fl-ma">
                  <DataBox title="INTERNO:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100"
                        iconLeft="fas fa-phone"
                        theme="ns-bg-1"
                        orientation=""
                        type="number"
                        size="small"
                        placeholder="0000"
                      />
                    </DataBox>
                  </div>

                </div>
              </div>
              <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                <div className="w-100 m-w-100 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="INGRESE SU SITIO WEB:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100"
                        iconLeft="fas fa-globe"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="https://nodesystems.uy"
                      />
                    </DataBox>
                  </div>
                </div>

            </div>
          </div>

          {/*
TODO ██████  ███████ ███████  ██████ ██████  ██ ██████   ██████ ██  ██████  ███    ██ 
TODO ██   ██ ██      ██      ██      ██   ██ ██ ██   ██ ██      ██ ██    ██ ████   ██ 
TODO ██   ██ █████   ███████ ██      ██████  ██ ██████  ██      ██ ██    ██ ██ ██  ██ 
TODO ██   ██ ██           ██ ██      ██   ██ ██ ██      ██      ██ ██    ██ ██  ██ ██ 
TODO ██████  ███████ ███████  ██████ ██   ██ ██ ██       ██████ ██  ██████  ██   ████ 
 */}

<div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w ns-bg-1 c-br-1 m-b-5px c-p-1 b-s-b-b ns-b-s ">
            <div className="w-10 m-w-100 m-d-b h-100 flex-auto h-pr-fl-ma c-p-1">
              <div className="w-40px h-40px h-pr-fl-ma r-h-c circle c-p-1 b-s-b-b caution">
                <i className="fas fa-exclamation c-white centered"></i>
              </div>
            </div>
            <div className="w-90 m-w-100 m-d-b h-auto flex-auto h-pr-fl-ma ns-bg-2 b-s-b-b c-p-1 c-br-1">
            <DataBox text="DESCRIPCIÓN" textStyle="m-b-5px t-a-c ns-c-e"></DataBox>
              {/* TODO |                                  | + TOPOLOGIA DE EMPRESA |  */}
              <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="SECTOR DE MERCADO:" titleStyle="m-b-5px">
                    <Sct
                      className="w-100"
                      iconLeft="fas fa-bullseye"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                    >
                      <Opt
                        text="Sector ofrecido..."
                        selected
                        hidden
                      />
                      <Opt text="Telecomunicaciones" value="1" />
                      <Opt text="Informática" value="2" />
                      <Opt text="Bellas Artes" value="3" />
                      <Opt text="Pequeños Almacenes" value="4" />
                      <Opt text="Medianos Almacenes" value="5" />
                      <Opt text="Grandes Almacenes" value="6" />
                      <Opt text="Supermercados" value="7" />
                      <Opt text="Puestos callejeros" value="8" />
                      <Opt text="Bibliotecas" value="9" />
                      <Opt text="Restaurantes" value="10/" />
                    </Sct>
                    </DataBox>
                </div>
                <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="CANTIDAD DE PERSONAL:" titleStyle="m-b-5px">
                    <Sct
                      className="w-100"
                      iconLeft="fas fa-user"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                    >
                      <Opt
                        text="Mi empresa tiene..."
                        selected
                        hidden
                      />
                      <Opt text="1 persona" value="0" />
                      <Opt
                        text="De 1 a 10 personas"
                        value="1"
                      />
                      <Opt
                        text="De 11 a 20 personas"
                        value="2"
                      />
                      <Opt
                        text="De 21 a 30 personas"
                        value="3"
                      />
                      <Opt
                        text="De 31 a 40 personas"
                        value="4"
                      />
                      <Opt
                        text="e 41 a 50 personas"
                        value="5"
                      />
                      <Opt
                        text="51 a 100 personas"
                        value="6"
                      />
                      <Opt
                        text="De 100 a 200 personas"
                        value="7"
                      />
                      <Opt
                        text="De 200 a 500 personas"
                        value="8"
                      />
                      <Opt
                        text="Más de 500 personas"
                        value="9"
                      />
                    </Sct>
                    </DataBox>
                </div>
              </div>
              {/* TODO |                                  | + DESCRIPCIÓN EMPRESA |  */}
              <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                <div className="w-100 m-w-100 h-auto h-pr-fl-ma c-p-1 ns-c-1">
                <DataBox title="DESCRIPCIÓN:" titleStyle="m-b-5px">
                    <textarea
                      maxlength="350"
                      className="w-100 h-10vh c-br-1 txt-a-r-n h-pr-fl-ma br-none ns-bg-1 ns-c-1 c-p-1"
                      placeholder="Ingrese una descripción breve de su empresa. (Max 350 caracteres.)"
                    ></textarea>
                  </DataBox>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* TODO |                                  | BOTON GUARDAR CAMBIOS |  */}
        <div className="w-100 h-auto flex-auto h-pr-fl-ma c-p-2 ">
          <div className="w-40 m-w-100  h-auto flex-auto h-pr-fl-ma r-h-c">
            <Btn
              className="w-100 normal "
              className="w-100 c-p-1"
              theme="success"
              text="GUARDAR CAMBIOS"
              iconLeft="fas fa-save"
            ></Btn>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default BusinessEditProfile
