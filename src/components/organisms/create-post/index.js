import React, { Fragment } from "react";
import "./styles.css";

import { Btn, Inp, Sct, Opt, Spt, DatList } from "../../atoms/index.js";
import { DataBox } from "../../molecules/index.js";

const CreatePost = () => {
  return (
    <Fragment>


      <div className="w-100 h-100 h-pr-fl-ma c-f-1  c-f-1 c-br-2 c-p-2 of-x-hidden of-y-auto">
        <DataBox text="CREAR POST" textStyle="success m-b-5px"></DataBox>

        <div className="w-100 h-auto h-pr-fl-ma c-br-1 t-a-c c-p-1 c-f-1 f-w-bo m-b-5px ns-bg-2 small">
          <div className="w-100 h-auto h-pr-fl-ma ">
            <div className="w-auto h-auto flexbox h-pr-fl-ma r-h-c ns-bg-1 d-i c-p-1 c-br-1 m-t-10px m-b-10px">
              <i className="w-auto h-auto fas fa-question-circle ns-c-e h-pr-fl-ma p-l-5px p-r-5px"></i>
              <div className="w-auto h-auto h-pr-fl-ma ns-c-1">¿Cómo funciona?</div>
            </div>
          </div>

          {/*   !      INICIO ------------------------------------------------------------ | (!) OCULTAR ESTO PARA OCULTAR INSTRUCCIONES | */}

          <div className="w-100 h-auto h-pr-fl-ma t-a-l ns-bg-1 ns-b-s c-br-1 c-p-1">
            <div className="w-33 m-w-100 h-auto h-pr-fl-ma c-p-1">
              <div className="flexbox w-auto h-pr-fl-ma m-w-100 d-i">
                <div className="w-auto d-i h-pr-fl-ma h-auto flex-auto c-p-2 ">
                  <div className="w-and-h-25px ns-bg-2 circle b-s-3px b-s-solid b-c-skyblue">
                    <div className="w-auto h-auto h-pr-fl-ma centered ">
                      <span className="ns-c-1 ">1</span>
                    </div>
                  </div>
                </div>
                <div className="w-auto h-auto h-pr-fl-ma flex-auto">
                  <span className="ns-c-1 h-pr-fl-ma w-100">
                    La empresa solicita
                  </span>
                  <span className="ns-c-1 h-pr-fl-ma l-h-1-2em nano">
                    Especifica cuál es la persona ideal que necesitas para el
                    puesto.
                  </span>
                </div>
              </div>
            </div>
            <div className="w-33 m-w-100 h-auto h-pr-fl-ma c-p-1">
              <div className="flexbox w-auto h-pr-fl-ma m-w-100 d-i">
                <div className="w-auto d-i h-pr-fl-ma h-auto flex-auto c-p-2 ">
                  <div className="w-and-h-25px ns-bg-2 circle b-s-3px b-s-solid b-c-green">
                    <div className="w-auto h-auto h-pr-fl-ma centered ">
                      <span className="ns-c-1">2</span>
                    </div>
                  </div>
                </div>
                <div className="w-auto h-auto h-pr-fl-ma flex-auto">
                  <span className="ns-c-1 h-pr-fl-ma w-100">
                    La empresa ofrece
                  </span>
                  <span className="ns-c-1 h-pr-fl-ma l-h-1-2em nano">
                    Ofrece el cargo que ocupará tu candidato y los recursos
                    acordes a tu solicitud.
                  </span>
                </div>
              </div>
            </div>
            <div className="w-33 m-w-100 h-auto h-pr-fl-ma c-p-1">
              <div className="flexbox w-auto h-pr-fl-ma m-w-100 d-i">
                <div className="w-auto d-i h-pr-fl-ma h-auto flex-auto c-p-2 ">
                  <div className="w-and-h-25px ns-bg-2 circle b-s-3px b-s-solid b-c-red">
                    <div className="w-auto h-auto h-pr-fl-ma centered ">
                      <span className="ns-c-1">3</span>
                    </div>
                  </div>
                </div>
                <div className="w-auto h-auto h-pr-fl-ma flex-auto">
                  <span className="ns-c-1 h-pr-fl-ma w-100">
                    Lo necesitas urgente?
                  </span>
                  <span className="ns-c-1 h-pr-fl-ma l-h-1-2em nano">
                    Selecciona la opción urgente para aparecer primero en las
                    búsquedas!
                  </span>
                </div>
              </div>
            </div>
          </div>
          {/*   !      FIN ------------------------------------------------------------ | (!) OCULTAR ESTO PARA OCULTAR INSTRUCCIONES | */}
        </div>

        {/*

? ███████ ███████    ███████  ██████  ██      ██  ██████ ██ ████████  █████
? ██      ██      ██ ██      ██    ██ ██      ██ ██      ██    ██    ██   ██
? ███████ █████      ███████ ██    ██ ██      ██ ██      ██    ██    ███████
?      ██ ██      ██      ██ ██    ██ ██      ██ ██      ██    ██    ██   ██
? ███████ ███████    ███████  ██████  ███████ ██  ██████ ██    ██    ██   ██

 */}

        <div className=" w-100 h-auto h-pr-fl-ma c-bg-10 info f-w-bo c-p-1 c-br-1 t-a-c normal m-b-5px">
          LA EMPRESA SOLICITA
        </div>

        {/*
TODO ██████  ███████ ██████  ███████  ██████  ███    ██  █████  ██
TODO ██   ██ ██      ██   ██ ██      ██    ██ ████   ██ ██   ██ ██
TODO ██████  █████   ██████  ███████ ██    ██ ██ ██  ██ ███████ ██
TODO ██      ██      ██   ██      ██ ██    ██ ██  ██ ██ ██   ██ ██
TODO ██      ███████ ██   ██ ███████  ██████  ██   ████ ██   ██ ███████
 */}
        <div className="w-100 h-auto h-pr-fl-ma ns-bg-2 c-p-1 c-br-1 m-b-5px">
          <div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w c-br-1 m-b-5px small ns-bg-1 ns-b-s c-p-1 b-s-b-b">
            <div className="w-10 m-w-100 m-d-b h-100 flex-auto h-pr-fl-ma c-p-1">
              <div className="w-40px h-40px h-pr-fl-ma r-h-c circle c-p-1 b-s-b-b alert">
                {/* ? COLOR DEL INDICADOR  */}
                <i className="fas fa-times c-white centered"></i>
                {/* ? ICONO DEL INDICADOR */}
              </div>
            </div>
            <div className="w-90 m-w-100 m-d-b h-auto flex-auto h-pr-fl-ma small ns-bg-2 c-p-1 c-br-1 b-s-b-b">
              <DataBox text="PERSONAL REQUERIDO" textStyle="m-b-5px t-a-c ns-c-e"></DataBox>

              {/* TODO |                                  | DATOS DE ENTREVISTADOR/A | */}
              <div className="w-100 h-auto h-pr-fl-ma c-p-1">

                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="EDAD" titleStyle="m-b-5px">
                    <Sct
                      className="w-100"
                      iconLeft="fas fa-calendar-day"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                    >
                      <Opt text="SELECCIONAR" selected hidden />
                      <Opt value="0" text="INDIFERENTE" />
                      <Opt
                        theme="ns-bg-1"
                        value="1"
                        text="PRIMERA EXPERIENCIA LABORAL"
                      />
                      <Opt value="2" text="+ 18" />
                      <Opt value="3" text="ENTRE 20 Y 25 AÑOS" />
                      <Opt value="4" text="ENTRE 25 Y 30" />
                      <Opt value="5" text="ENTRE 30 Y 40" />
                      <Opt value="6" text="ENTRE 40 Y 50" />
                      <Opt value="7" text="ENTRE 50 Y 60" />
                      <Opt value="8" text="MÁS DE 60" />
                      <Opt value="9" text="JUBILADOS" />
                    </Sct>

                  </DataBox>




                </div>
                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="SEXO" titleStyle="m-b-5px">
                    <Sct
                      className="w-100"
                      iconLeft="fas fa-restroom"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                    >
                      <Opt text="SELECCIONAR" selected hidden />
                      <Opt value="0" text="INDIFERENTE" />
                      <Opt value="1" text="FEMENINO" />
                      <Opt value="2" text="MASCULINO" />
                    </Sct>

                  </DataBox>

                </div>
                <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="PRIORIDAD A PERSONAS CON DISCAPACIDAD:" titleStyle="m-b-5px">
                    <Sct
                      className="w-100"
                      iconLeft="fas fa-wheelchair"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                    >
                      <Opt text="SELECCIONAR" selected hidden />
                      <Opt value="0" text="SIN PRIORIDAD" />
                      <Opt value="1" text="MOTORA" />
                      <Opt value="2" text="SENSORIAL" />
                      <Opt value="3" text="INTELECTUAL" />
                      <Opt value="4" text="PSÍQUICA" />
                      <Opt value="5" text="VISCERAL" />
                      <Opt value="6" text="MULTIPLE" />
                    </Sct>
                  </DataBox>

                </div>
              </div>
            </div>
          </div>

          {/*
TODO ██   ██  █████  ██████  ██ ██      ██ ████████  █████   ██████ ██  ██████  ███    ██ ███████ ███████
TODO ██   ██ ██   ██ ██   ██ ██ ██      ██    ██    ██   ██ ██      ██ ██    ██ ████   ██ ██      ██
TODO ███████ ███████ ██████  ██ ██      ██    ██    ███████ ██      ██ ██    ██ ██ ██  ██ █████   ███████
TODO ██   ██ ██   ██ ██   ██ ██ ██      ██    ██    ██   ██ ██      ██ ██    ██ ██  ██ ██ ██           ██
TODO ██   ██ ██   ██ ██████  ██ ███████ ██    ██    ██   ██  ██████ ██  ██████  ██   ████ ███████ ███████
 */}

          <div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w ns-bg-1 ns-b-s c-br-1 m-b-5px c-p-1 b-s-b-b small">
            <div className="w-10 m-w-100 m-d-b h-100 flex-auto h-pr-fl-ma c-p-1">
              <div className="w-40px h-40px h-pr-fl-ma r-h-c circle c-p-1 b-s-b-b alert">
                {/* ? COLOR DEL INDICADOR  */}
                <i className="fas fa-times c-white centered"></i>
                {/* ? ICONO DEL INDICADOR */}
              </div>
            </div>
            <div className="w-90 m-w-100 m-d-b h-auto flex-auto h-pr-fl-ma small ns-bg-2 c-p-1 c-br-1 b-s-b-b">

              <DataBox text="HABILITACIONES" textStyle="m-b-5px t-a-c ns-c-e"></DataBox>
              {/* TODO |                                  | DATOS DE ENTREVISTADOR/A | */}
              <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                <div className="w-50 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="LIBRETA DE CONDUCIR:" titleStyle="m-b-5px">
                    <Sct
                      className="w-100"
                      iconLeft="fas fa-address-card"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                    >
                      <Opt text="SELECCIONAR" selected hidden />
                      <Opt value="0" text="NO REQUERIDO" />
                      <Opt value="1" text="REQUERIDO" />
                    </Sct>
                  </DataBox>


                </div>
                <div className="w-50 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="VEHICULO PROPIO:" titleStyle="m-b-5px">
                    <Sct
                      className="w-100"
                      iconLeft="fas fa-car"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                    >
                      <Opt text="SELECCIONAR" selected hidden />
                      <Opt value="0" text="NO REQUERIDO" />
                      <Opt value="1" text="CICLOMOTOR" />
                      <Opt value="2" text="MOTOCICLETA" />
                      <Opt value="3" text="AUTO" />
                      <Opt value="4" text="CAMIÓN PEQUEÑO" />
                      <Opt value="5" text="CAMIÓN MEDIANO" />
                      <Opt value="6" text="CAMIÓN GRANDE" />
                      <Opt value="7" text="CAMIÓN UTILITARIO" />
                      <Opt value="8" text="CAMIÓN INDUSTRIAL" />
                      <Opt value="9" text="MICRO BUS" />
                      <Opt value="10" text="OMNIBUS" />
                      <Opt value="11" text="MAQUINARIA VIAL" />
                      <Opt value="12" text="MAQUINARIA AGRICOLA" />
                      <Opt value="13" text="AERONAVE UNITARIA"  />
                      <Opt value="14" text="AERONAVE COMERCIAL" />
                      <Opt value="15" text="EMBARCACIÓN PEQUEÑA" />
                      <Opt value="16"text="EMBARCACIÓN MEDIANA" />
                      <Opt value="17" text="EMBARCACIÓN GRANDE" />
                      <Opt value="18" text="EMBARCACIÓN UTILITARIA" />
                      <Opt value="19" text="EMBARCACIÓN INDUSTRIAL" />
                    </Sct>
                  </DataBox>


                </div>
              </div>
              <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                <div className="w-50 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="CARNÉ DE SALUD:" titleStyle="m-b-5px">
                  <Sct
                      className="w-100"
                      iconLeft="fas fa-notes-medical"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                    >
                      <Opt text="SELECCIONAR" selected hidden />
                      <Opt value="0" text="NO REQUERIDO" />
                      <Opt value="1" text="REQUERIDO" />
                    </Sct>
                  </DataBox>

                </div>
                <div className="w-50 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="CARNÉ DE MANIPULACIÓN ALIMENTOS:" titleStyle="m-b-5px">
                  <Sct
                      className="w-100"
                      iconLeft="fas fa-apple-alt"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                    >
                      <Opt text="SELECCIONAR" selected hidden />
                      <Opt value="0" text="NO REQUERIDO" />
                      <Opt value="1" text="REQUERIDO" />
                    </Sct>
                  </DataBox>

                </div>
              </div>
            </div>
          </div>

          {/*
TODO ███████ ███████ ████████ ██    ██ ██████  ██  ██████  ███████
TODO ██      ██         ██    ██    ██ ██   ██ ██ ██    ██ ██
TODO █████   ███████    ██    ██    ██ ██   ██ ██ ██    ██ ███████
TODO ██           ██    ██    ██    ██ ██   ██ ██ ██    ██      ██
TODO ███████ ███████    ██     ██████  ██████  ██  ██████  ███████

 */}

          <div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w ns-bg-1 ns-b-s c-br-1 m-b-5px c-p-1 b-s-b-b small">
            <div className="w-10 m-w-100 m-d-b h-100 flex-auto h-pr-fl-ma c-p-1">
              <div className="w-40px h-40px h-pr-fl-ma r-h-c circle c-p-1 b-s-b-b alert">
                {/* ? COLOR DEL INDICADOR  */}
                <i className="fas fa-times c-white centered"></i>
                {/* ? ICONO DEL INDICADOR */}
              </div>
            </div>
            <div className="w-90 m-w-100 m-d-b h-auto flex-auto h-pr-fl-ma small ns-bg-2 c-p-1 c-br-1 b-s-b-b">
              <DataBox text="ESTUDIOS" textStyle="m-b-5px t-a-c ns-c-e"></DataBox>

              {/* TODO |                                  | DATOS DE ENTREVISTADOR/A | */}
              <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="NIVEL ACADÉMICO:" titleStyle="m-b-5px">
                    <Sct
                      className="w-100"
                      iconLeft="fas fa-graduation-cap"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                    >
                      <Opt text="SELECCIONAR" selected hidden />
                      <Opt value="0" text="NO REQUERIDO" />
                      <Opt value="1"
                        text="PRIMARIA INCOMPLETA"
                      ></Opt>
                      <Opt  value="2"
                        text="PRIMARIA CURSANDO"
                      ></Opt>
                      <Opt value="3"
                        text="PRIMARIA COMPLETA"
                      ></Opt>
                      <Opt value="4"
                        text="SECUNDARIA INCOMPLETA"
                      ></Opt>
                      <Opt value="5"
                        text="SECUNDARIA CURSANDO"
                      ></Opt>
                      <Opt value="6"
                        text="SECUNDARIA COMPLETA"
                      ></Opt>
                      <Opt value="7"
                        text="UNIVERSIDAD INCOMPLETA"
                      ></Opt>
                      <Opt value="8"
                        text="UNIVERSIDAD CURSANDO"
                      ></Opt>
                      <Opt value="9"
                        text="UNIVERSIDAD COMPLETA"
                      ></Opt>
                      <Opt value="10"
                        text="POST GRADO INCOMPLETO"
                      ></Opt>
                      <Opt value="11"
                        text="POST GRADO CURSANDO"
                      ></Opt>
                      <Opt value="12"
                        text="POST GRADO COMPLETO"
                      ></Opt>
                    </Sct>
                  </DataBox>

                </div>
                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="IDIOMAS:" titleStyle="m-b-5px">
                  <Sct
                      className="w-100"
                      iconLeft="fas fa-language"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                    >
                      <Opt text="SELECCIONAR" selected hidden />
                      <Opt value="0" text="NO REQUERIDO"></Opt>
                      <Opt value="1" text="ESPAÑOL"></Opt>
                      <Opt value="2" text="INGLES"></Opt>
                      <Opt value="3" text="PORTUGUES"></Opt>
                      <Opt value="4" text="ALEMAN"></Opt>
                      <Opt value="5" text="JAPONES"></Opt>
                      <Opt value="6" text="CHINO"></Opt>
                      <Opt value="7" text="FRANCÉS"></Opt>
                      <Opt value="8" text="ITALIANO"></Opt>
                      <Opt value="9" text="RUSO"></Opt>
                      <Opt value="10" text="ARABE"></Opt>
                      <Opt value="11" text="INDÚ"></Opt>
                      <Opt value="12" text="TAILANDÉS"></Opt>
                    </Sct>
                  </DataBox>

                </div>
                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="NIVEL IDIOMAS:" titleStyle="m-b-5px">
                  <Sct
                      className="w-100"
                      iconLeft="fas fa-arrow-circle-up"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                    >
                      <Opt text="SELECCIONAR" selected hidden />
                      <Opt value="0" text="NO REQUERIDO"></Opt>
                      <Opt value="1" text="BAJO"></Opt>
                      <Opt value="2" text="MEDIO"></Opt>
                      <Opt value="3" text="ALTO"></Opt>
                      <Opt value="4" text="NATIVO"></Opt>
                    </Sct>
                  </DataBox>
                </div>
              </div>
            </div>
          </div>
          </div>
          <div className=" w-100 h-auto h-pr-fl-ma c-bg-10 success f-w-bo c-p-1 c-br-1 t-a-c normal m-b-5px">
            LA EMPRESA OFRECE
          </div>

          {/*
           * ███████ ███████     ██████  ███████ ██████  ███████  ██████ ███████
           * ██      ██      ██ ██    ██ ██      ██   ██ ██      ██      ██
           * ███████ █████      ██    ██ █████   ██████  █████   ██      █████
           *      ██ ██      ██ ██    ██ ██      ██   ██ ██      ██      ██
           * ███████ ███████     ██████  ██      ██   ██ ███████  ██████ ███████
           */}
        <div className="w-100 h-auto h-pr-fl-ma ns-bg-2 c-p-1 ns-bg-1  c-br-1 m-b-5px">
          <div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w ns-bg-1 ns-b-s c-br-1 m-b-5px c-p-1 b-s-b-b  small">
            <div className="w-10 m-w-100 m-d-b h-100 flex-auto h-pr-fl-ma c-p-1">
              <div className="w-40px h-40px h-pr-fl-ma r-h-c circle c-p-1 b-s-b-b alert">
                {/* ? COLOR DEL INDICADOR  */}
                <i className="fas fa-times c-white centered"></i>
                {/* ? ICONO DEL INDICADOR */}
              </div>
            </div>
            <div className="w-90 m-w-100 m-d-b h-auto flex-auto h-pr-fl-ma small ns-bg-2 c-p-1 c-br-1 b-s-b-b">

              <DataBox text="PUESTO OFRECIDO" textStyle="m-b-5px t-a-c ns-c-e"></DataBox>

              {/* TODO |                                  | CARGO OFRECIDO | */}
              <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="CARGO OFRECIDO:" titleStyle="m-b-5px">
                  <DatList
                      type="datalist"
                      iconLeft="fas fa-bullseye"
                      theme="ns-bg-1"
                      className="w-100"
                      size="small"
                      placeholder="BUSCAR PUESTO"
                    >
                      <Opt value="SELECCIONAR" selected hidden />
                      <Opt value="PROGRAMADOR"></Opt>
                      <Opt value="MAQUETADOR FRONTEND"></Opt>
                      <Opt value="DISEÑADOR/A GRÁFICO"></Opt>
                      <Opt value="ANALISTA EN SISTEMAS"></Opt>
                      <Opt value="VENDEDOR/A"></Opt>
                      <Opt value="CARPINTERO/A"></Opt>
                      <Opt value="MÉDICO NUTRICIONISTA"></Opt>
                      <Opt value="MÉDICO CIRUJANO"></Opt>
                      <Opt value="MÉDICO NEFROLOGÍA"></Opt>
                      <Opt value="MALABARISTA"></Opt>
                      <Opt value="BARMAN"></Opt>
                      <Opt value="MESERO/A"></Opt>
                      <Opt value="CONTADOR/A"></Opt>
                      <Opt value="ABOGADO/A"></Opt>
                      <Opt value="FUTBOLISTA"></Opt>
                      <Opt value="JARDINERO/A"></Opt>
                      <Opt value="MÉDICO ANESTESISTA"></Opt>
                    </DatList>
                  </DataBox>

                </div>
                {/* TODO |                                  | SALARIO OFRECIDO | */}
                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="SUELDO:" titleStyle="m-b-5px">
                  <Inp
                      className="w-100"
                      iconLeft="fas fa-money-bill-wave"
                      theme="ns-bg-1"
                      orientation=""
                      type="number"
                      size="small"
                      placeholder=""
                    />
                  </DataBox>

                </div>
                {/* TODO |                                  | FECHA DE COBRO OFRECIDO | */}
                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="FECHA DE PAGO:" titleStyle="m-b-5px">
                  <Sct
                      className="w-100"
                      iconLeft="fas fa-calendar-day"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                    >
                      <Opt text="SELECCIONAR" selected hidden />
                      <Opt value="0" text="PAGO ÚNICO"></Opt>
                      <Opt value="1" text="SEMANAL (CADA 1 SEMANA)"></Opt>
                      <Opt value="2" text="QUINCENAL (CADA 15 DÍAS)"></Opt>
                      <Opt value="3" text="BIMENSUAL (2 VECES AL MES)"></Opt>
                      <Opt value="4" text="TRIMESTRAL (3 VECES AL MES)"></Opt>
                      <Opt value="5" text="MENSUAL (CADA 1 MES)"></Opt>
                      <Opt value="3" text="BIMENSUAL (CADA 2 MESES)"></Opt>
                      <Opt value="3" text="TRIMESTRAL (CADA 3 MESES)"></Opt>
                      <Opt value="6" text="ANUAL (CADA AÑO)"></Opt>
                    </Sct>
                  </DataBox>

                </div>
              </div>
            </div>
          </div>
          {/* TODO |                                  | DESCRIPCION | */}
          <div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w ns-bg-1 ns-b-s c-br-1 m-b-5px c-p-1 b-s-b-b small">
            <div className="w-10 m-w-100 m-d-b h-100 flex-auto h-pr-fl-ma c-p-1">
              <div className="w-40px h-40px h-pr-fl-ma r-h-c circle c-p-1 b-s-b-b alert">
                {/* ? COLOR DEL INDICADOR  */}
                <i className="fas fa-times c-white centered"></i>
                {/* ? ICONO DEL INDICADOR */}
              </div>
            </div>
            <div className="w-90 m-w-100 m-d-b h-auto flex-auto h-pr-fl-ma ns-bg-2 c-p-1 c-br-1 small b-s-b-b">
              <DataBox text="DESCRIPCIÓN" textStyle="m-b-5px t-a-c ns-c-e"></DataBox>
              {/* TODO |                                  | SUGERENCIAS PARA DESCRIPCIÓN | */}
              <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                <div className="w-100  h-auto h-pr-fl-ma c-p-1">
                  <div className="w-100 h-auto h-pr-fl-ma c-f-1 d-i-f ns-bg-1 p-l-5px b-s-b-b small c-p-1 c-br-1">
                    <i className="fas fa-exclamation-circle c-p-1 c-green-node p-l-5px p-r-5px"></i>
                    <div className="w-auto h-auto c-green-node f-w-bo h-pr-fl-ma">
                      Ayudale a tus candidatos a entender lo que buscas.
                    </div>
                  </div>
                  <div className="w-100 h-auto h-pr-fl-ma d-i-f c-f-1 p-l-5px b-s-b-b small c-p-1 c-br-1">
                    <i className="fas fa-arrow-alt-circle-down c-p-1 ns-c-1 p-l-5px p-r-5px"></i>
                    <div className="w-auto h-auto f-w-bo ns-c-1 h-pr-fl-ma">
                      Responde a alguna de las siguientes preguntas en tu
                      descripción.
                    </div>
                  </div>
                </div>

                <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-100 h-auto h-pr-fl-ma c-f-1 ns-c-1 f-w-bo ns-bg-1 ns-c-1 p-l-5px b-s-b-b small c-p-1 c-br-1">
                    <i className="fas fa-brain ns-c-e p-l-5px p-r-5px"></i>
                    ¿Qué cualidades busca tu empresa?
                  </div>
                </div>

                <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-100 h-auto h-pr-fl-ma c-f-1 ns-c-1 f-w-bo ns-bg-1 ns-c-1 p-l-5px b-s-b-b small c-p-1 c-br-1">
                    <i className="far fa-clock ns-c-e p-l-5px p-r-5px"></i>
                    ¿Ofrecen disponibilidad horaria?
                  </div>
                </div>

                <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-100 h-auto h-pr-fl-ma c-f-1 ns-c-1 f-w-bo ns-bg-1 ns-c-1 p-l-5px b-s-b-b small c-p-1 c-br-1">
                    <i className="fas fa-chart-bar ns-c-e p-l-5px p-r-5px"></i>
                    ¿Porqué deberían trabajar en su empresa?
                  </div>
                </div>
                <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-100 h-auto h-pr-fl-ma c-f-1 ns-bg-1 ns-c-1 f-w-bo p-l-5px b-s-b-b small c-p-1 c-br-1">
                    <i className="fas fa-hand-holding-usd ns-c-e p-l-5px p-r-5px"></i>
                    ¿Qué incentivos ofrecen a sus empleados?
                  </div>
                </div>
              </div>

              {/* TODO |                                  | DESCRIPCIÓN ENTREVISTA | */}
              <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                <div className="w-100 m-w-100 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="DESCRIPCIÓN:" titleStyle="m-b-5px">
                    <textarea
                      maxlength="350"
                      className="w-100 h-10vh c-br-1 txt-a-r-n h-pr-fl-ma br-none ns-bg-1 ns-c-1 c-p-1 ns-c-1"
                      placeholder="Ingrese una descripción breve de su publicación. (Max 350 caracteres.)"
                    ></textarea>
                  </DataBox>
                </div>

              </div>
            </div>
          </div>
          {/* TODO |                                  | PUBLICACION URGENTE | */}
          <div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w ns-bg-1 ns-b-s c-br-1 m-b-5px c-p-1 b-s-b-b h-e small">
            <div className="w-10 h-auto flex-auto h-pr-fl-ma">
              <span className="w-and-h-40px bg-red-node c-br-1 h-pr-fl-ma">
                <i className="fas fa-exclamation centered c-white"></i>
              </span>
            </div>
            <div className="w-60 h-auto flex-auto h-pr-fl-ma c-p-1 b-s-b-b">
              <span className="w-100 h-auto h-pr-fl-ma ns-c-1">
                Haz esta publicación urgente!
              </span>
              <span className="w-100 h-auto h-pr-fl-ma nano ns-c-1">
                Las publicaciones urgentes se ven primero en todas las
                búsquedas!
              </span>
            </div>
            <div className="w-30 h-auto flex-auto h-pr-fl-ma">
              <Btn
                className="w-100 c-p-1 small"
                theme="alert"
                text="URGENTE"
                iconLeft="fas fa-exclamation"
              ></Btn>
            </div>
          </div>
        </div>
        {/* TODO |                                  | BOTON CREAR POST | */}
        <div className="w-100 h-auto flex-auto h-pr-fl-ma c-p-2 ">
          <div className="w-40 m-w-100  h-auto flex-auto h-pr-fl-ma r-h-c">
            <Btn
              className="w-100 c-p-1 normal"
              theme="success"
              text="CREAR POST"
              iconLeft="fas fa-plus"
            ></Btn>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default CreatePost
