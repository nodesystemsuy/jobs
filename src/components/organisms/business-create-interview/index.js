import React, { Fragment } from "react";
import "./styles.css";

import { Btn, Inp, Sct, Spt, Opt, Text } from "../../atoms/index.js";
import { DataBox } from "../../molecules/index.js";
const BusinessCreateInterview = () => {
    return (
      <Fragment>
<div className="w-100 h-100 h-pr-fl-ma c-f-1  c-f-1 c-br-2 c-p-2 of-x-hidden of-y-auto">
<DataBox text="CREAR ENTREVISTA" textStyle="info m-b-5px"></DataBox>

    {/*
TODO ██ ██████  ███████ ███    ██ ████████ ██ ██████   █████  ██████ 
TODO ██ ██   ██ ██      ████   ██    ██    ██ ██   ██ ██   ██ ██   ██ 
TODO ██ ██   ██ █████   ██ ██  ██    ██    ██ ██   ██ ███████ ██   ██ 
TODO ██ ██   ██ ██      ██  ██ ██    ██    ██ ██   ██ ██   ██ ██   ██ 
TODO ██ ██████  ███████ ██   ████    ██    ██ ██████  ██   ██ ██████ 
 */}

<div className="w-100 h-auto h-pr-fl-ma ns-bg-2 c-p-1 c-br-1 m-b-5px">
    <div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w ns-bg-1 ns-b-s c-br-1 m-b-5px c-p-1 b-s-b-b small">
        <div className="w-10 m-w-100 m-d-b h-100 flex-auto h-pr-fl-ma c-p-1">
            <div className="w-40px h-40px h-pr-fl-ma r-h-c circle c-p-1 b-s-b-b alert">
                 {/* ? COLOR DEL INDICADOR   */}
                <i className="fas fa-times c-white centered"></i>
                 {/* ? ICONO DEL INDICADOR  */}
            </div>
        </div>
        <div className="w-90 m-w-100 m-d-b h-auto flex-auto h-pr-fl-ma ns-bg-2 c-p-1 c-br-1 small b-s-b-b">
        <DataBox text=" DATOS DE ENTREVISTADOR/A" textStyle="m-b-5px t-a-c ns-c-e"></DataBox>

             {/* TODO |                                  | DATOS DE ENTREVISTADOR/A |  */}
            <div className="w-100 h-auto h-pr-fl-ma c-p-1">
            <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
            <DataBox title="NOMBRE:" titleStyle="m-b-5px">
            <Inp
                        className="w-100"
                        iconLeft="fas fa-signature"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="APELLIDO"
                      />
            </DataBox>

                  </div>
                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="APELLIDO:" titleStyle="m-b-5px">
                <Inp
                        className="w-100"
                        iconLeft="fas fa-signature"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="APELLIDO"
                      />
                </DataBox>
                  </div>

                <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="CARGO DE ENTREVISTADOR/A:" titleStyle="m-b-5px">
                <Sct
                      className="w-100"
                      iconLeft="fas fa-people-arrows"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                    >
                      <Opt text="SELECCIONAR" selected hidden />
                      <Opt value="0"  text="PERSONAL RRHH"></Opt>
                      <Opt value="1" text="ENCARGADO/A RRHH"></Opt>
                      <Opt value="2" text="PERSONAL TERCIARIZADO/A"></Opt>
                      <Opt value="3" text="EMPRESA TERCIARIZADA"></Opt>
                      <Opt value="4" text="GERENTE GENERAL"></Opt>
                      <Opt value="5" text="EMPLEADO/A"></Opt>
                      <Opt value="6" text="DIRECTOR/A MARKETING"></Opt>
                      <Opt value="7" text="DIRECTOR/A JURIDICA"></Opt>
                      <Opt value="8" text="DIRECTOR/A ADMINISTRACIÓN"></Opt>
                      <Opt value="9" text="DIRECTOR/A TESORERÍA"></Opt>
                      <Opt value="10" text="DIRECTOR/A CAPACITACIÓN"></Opt>
                    </Sct>
                </DataBox>
                </div>
            </div>

        </div>
    </div>

     {/*
TODO  ██    ██ ██████  ██  ██████  █████   ██████ ██  ██████  ███    ██ 
TODO  ██    ██ ██   ██ ██ ██      ██   ██ ██      ██ ██    ██ ████   ██ 
TODO  ██    ██ ██████  ██ ██      ███████ ██      ██ ██    ██ ██ ██  ██ 
TODO  ██    ██ ██   ██ ██ ██      ██   ██ ██      ██ ██    ██ ██  ██ ██ 
TODO   ██████  ██████  ██  ██████ ██   ██  ██████ ██  ██████  ██   ████ 
            */}
      <div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w ns-bg-1 ns-b-s c-br-1 m-b-5px c-p-1 b-s-b-b small">
        <div className="w-10 m-w-100 m-d-b h-100 flex-auto h-pr-fl-ma c-p-1">
            <div className="w-40px h-40px h-pr-fl-ma r-h-c circle c-p-1 b-s-b-b caution">
                 {/* ? COLOR DEL INDICADOR   */}
                <i className="fas fa-exclamation c-white centered"></i>
                 {/* ? ICONO DEL INDICADOR  */}
            </div>
        </div>
        <div className="w-90 m-w-100 m-d-b h-auto flex-auto h-pr-fl-ma ns-bg-2 c-p-1 c-br-1 small b-s-b-b">
        <DataBox text="UBICACIÓN DE LA ENTREVISTA" textStyle="m-b-5px t-a-c ns-c-e"></DataBox>

             {/* TODO |                                  | DIRECCIÓN DE ENTREVISTA |  */}
            <div className="w-100 h-auto h-pr-fl-ma c-p-1">
            <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
            <DataBox title="CALLE:" titleStyle="m-b-5px">
            <Inp
                        className="w-100"
                        iconLeft="fas fa-road"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="CALLE"
                      />
            </DataBox>
                  </div>
                  <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="INTERSECCIÓN:" titleStyle="m-b-5px">
                  <Inp
                        className="w-100"
                        iconLeft="fas fa-directions"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="INTERSECCIÓN"
                      /></DataBox>


                  </div>
                <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="PISO:" titleStyle="m-b-5px">
                <Inp
                        className="w-100"
                        iconLeft="fas fa-building"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="EDIFICIO"
                      />
                </DataBox>
                  </div>
                  <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="NUMERO DE PUERTA:" titleStyle="m-b-5px">
                  <Inp
                        className="w-100"
                        iconLeft="fas fa-door-closed"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="N° PUERTA"
                      />
                  </DataBox>


                  </div>
            </div>
        </div>
    </div>

     {/*

TODO ██   ██  ██████  ██████   █████  ██████  ██  ██████  ███████ 
TODO ██   ██ ██    ██ ██   ██ ██   ██ ██   ██ ██ ██    ██ ██      
TODO ███████ ██    ██ ██████  ███████ ██████  ██ ██    ██ ███████ 
TODO ██   ██ ██    ██ ██   ██ ██   ██ ██   ██ ██ ██    ██      ██ 
TODO ██   ██  ██████  ██   ██ ██   ██ ██   ██ ██  ██████  ███████

            */}

<div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w ns-bg-1 ns-b-s c-br-1 m-b-5px c-p-1 b-s-b-b small">
        <div className="w-10 m-w-100 m-d-b h-100 flex-auto h-pr-fl-ma c-p-1">
            <div className="w-40px h-40px h-pr-fl-ma r-h-c circle c-p-1 b-s-b-b success">
                 {/* ? COLOR DEL INDICADOR   */}
                <i className="fas fa-check c-white centered"></i>
                 {/* ? ICONO DEL INDICADOR  */}
            </div>
        </div>
        <div className="w-90 m-w-100 m-d-b h-auto flex-auto h-pr-fl-ma ns-bg-2 c-p-1 c-br-1 small b-s-b-b">
        <DataBox text="HORARIO DE LA ENTREVISTA" textStyle="m-b-5px t-a-c ns-c-e"></DataBox>

            <div className="w-100 h-auto h-pr-fl-ma c-p-1">
            <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
            <DataBox title="HORA APERTURA:" titleStyle="m-b-5px">
            <Inp
                        className="w-100"
                        iconLeft="fas fa-door-open"
                        theme="ns-bg-1"
                        orientation=""
                        type="time"
                        size="small"
                        placeholder=""
                      /> </DataBox>
                  </div>
                  <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="HORA CIERRE:" titleStyle="m-b-5px">
                  <Inp
                        className="w-100"
                        iconLeft="fas fa-door-closed"
                        theme="ns-bg-1"
                        orientation=""
                        type="time"
                        size="small"
                        placeholder=""
                      />
                  </DataBox>

                  </div>
                  <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="DE:" titleStyle="m-b-5px">
                  <Sct
                        className="w-100"
                        iconLeft="fas fa-calendar"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                      >
                        <Opt text="DESDE" selected hidden />
                        <Opt text="SOLO LOS" />
                        <Opt text="LUNES" />
                        <Opt text="MARTES" />
                        <Opt text="MIERCOLES" />
                        <Opt text="JUEVES" />
                        <Opt text="VIERNES" />
                        <Opt text="SABADO" />
                        <Opt text="DOMINGO" />
                      </Sct>
                  </DataBox>
                  </div>

                  <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="HASTA:" titleStyle="m-b-5px">
                    <Sct
                        className="w-100"
                        iconLeft="fas fa-calendar"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                      >
                        <Opt text="HASTA" selected hidden />
                        <Opt text="SOLO LOS" />
                        <Opt text="LUNES" />
                        <Opt text="MARTES" />
                        <Opt text="MIERCOLES" />
                        <Opt text="JUEVES" />
                        <Opt text="VIERNES" />
                        <Opt text="SABADO" />
                        <Opt text="DOMINGO" />
                      </Sct> </DataBox>





                  </div>
            </div>
        </div>
    </div>

     {/*
TODO  ██████  ██████  ███    ██ ████████  █████   ██████ ████████  ██████ 
TODO ██      ██    ██ ████   ██    ██    ██   ██ ██         ██    ██    ██ 
TODO ██      ██    ██ ██ ██  ██    ██    ███████ ██         ██    ██    ██ 
TODO ██      ██    ██ ██  ██ ██    ██    ██   ██ ██         ██    ██    ██ 
TODO  ██████  ██████  ██   ████    ██    ██   ██  ██████    ██     ██████ 
            */}

     {/* TODO |                                  | EMAIL CONTACTO + WEB / ENLACE ENTREVISTA |  */}
     <div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w ns-bg-1 ns-b-s c-br-1 m-b-5px c-p-1 b-s-b-b small">
        <div className="w-10 m-w-100 m-d-b h-100 flex-auto h-pr-fl-ma c-p-1">
            <div className="w-40px h-40px h-pr-fl-ma r-h-c circle c-p-1 b-s-b-b caution">
                 {/* ? COLOR DEL INDICADOR   */}
                <i className="fas fa-exclamation c-white centered"></i>
                 {/* ? ICONO DEL INDICADOR  */}
            </div>
        </div>
        <div className="w-90 m-w-100 m-d-b h-auto flex-auto h-pr-fl-ma ns-bg-2 c-p-1 c-br-1 small b-s-b-b">
        <DataBox text="CONTACTO" textStyle="m-b-5px t-a-c ns-c-e"></DataBox>

            <div className="w-100 h-auto h-pr-fl-ma ">
            <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                <div className="w-100 m-w-100 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="EMAIL DE CONTACTO:" titleStyle="m-b-5px">
                <Inp
                        className="w-100"
                        iconLeft="fas fa-envelope"
                        theme="ns-bg-1"
                        orientation=""
                        type="email"
                        size="small"
                        placeholder="rrhh@empresa.com"
                      />
                </DataBox>
                </div>
              </div>

              <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                <div className="w-100 m-w-100 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="INGRESE SU SITIO WEB:" titleStyle="m-b-5px">
                <Inp
                        className="w-100"
                        iconLeft="fas fa-globe"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="https://nodesystems.uy"
                      />
                </DataBox>
                </div>
              </div>

            </div>
             {/* TODO |                                  | TELÉFONO CONTACTO (1) Y (2) |  */}

            <div className="w-100 h-auto h-pr-fl-ma c-p-1">
            <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-70 m-w-50 h-auto h-pr-fl-ma">
                  <DataBox title="TELÉFONO:" titleStyle="m-b-5px">
                  <Inp
                        className="w-100"
                        iconLeft="fas fa-phone"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="+000-000-000-000"
                      />
                  </DataBox>
                  </div>
                  <div className="w-30 m-w-50 h-auto h-pr-fl-ma ">
                  <DataBox title="INTERNO:" titleStyle="m-b-5px">
                  <Inp
                        className="w-100"
                        iconLeft="fas fa-phone"
                        theme="ns-bg-1"
                        orientation=""
                        type="number"
                        size="small"
                        placeholder="0000"
                      />
                  </DataBox>
                  </div>
                </div>

                <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-70 m-w-50 h-auto h-pr-fl-ma">
                  <DataBox title="TELÉFONO 2:" titleStyle="m-b-5px">
                  <Inp
                        className="w-100"
                        iconLeft="fas fa-phone"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="+000-000-000-000"
                      />
                  </DataBox></div>
                  <div className="w-30 m-w-50 h-auto h-pr-fl-ma ">
                  <DataBox title="INTERNO:" titleStyle="m-b-5px">
                  <Inp
                        className="w-100"
                        iconLeft="fas fa-phone"
                        theme="ns-bg-1"
                        orientation=""
                        type="number"
                        size="small"
                        placeholder="0000"
                      />
                  </DataBox>
                  </div>
                </div>
            </div>
        </div>
    </div>

     {/*
TODO ██████  ███████ ███████  ██████ ██████  ██ ██████   ██████ ██  ██████  ███    ██ 
TODO ██   ██ ██      ██      ██      ██   ██ ██ ██   ██ ██      ██ ██    ██ ████   ██ 
TODO ██   ██ █████   ███████ ██      ██████  ██ ██████  ██      ██ ██    ██ ██ ██  ██ 
TODO ██   ██ ██           ██ ██      ██   ██ ██ ██      ██      ██ ██    ██ ██  ██ ██ 
TODO ██████  ███████ ███████  ██████ ██   ██ ██ ██       ██████ ██  ██████  ██   ████ 
 */}

<div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w ns-bg-1 ns-b-s c-br-1 m-b-5px c-p-1 b-s-b-b small">
        <div className="w-10 m-w-100 m-d-b h-100 flex-auto h-pr-fl-ma c-p-1">
            <div className="w-40px h-40px h-pr-fl-ma r-h-c circle c-p-1 b-s-b-b caution">
                 {/* ? COLOR DEL INDICADOR   */}
                <i className="fas fa-exclamation c-white centered"></i>
                 {/* ? ICONO DEL INDICADOR  */}
            </div>
        </div>
        <div className="w-90 m-w-100 m-d-b h-auto flex-auto h-pr-fl-ma ns-bg-2 c-p-1 c-br-1 small b-s-b-b">
        <DataBox text="DESCRIPCION" textStyle="m-b-5px t-a-c ns-c-e"></DataBox>
             {/* TODO |                                  | SUGERENCIAS PARA DESCRIPCIÓN |  */}
            <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                <div className="w-100  h-auto h-pr-fl-ma c-p-1">
                    <div
                        className="w-100 h-auto h-pr-fl-ma c-f-1 d-i-f ns-bg-1 p-l-5px b-s-b-b small c-p-1 c-br-1">
                        <i className="fas fa-exclamation-circle c-p-1 c-green-node p-l-5px p-r-5px"></i>
                        <div className="w-auto h-auto c-green-node h-pr-fl-ma f-w-bo">Ayudale a tus candidatos a prepararse para una entrevista</div>
                    </div>
                    <div
                        className="w-100 h-auto h-pr-fl-ma d-i-f c-f-1  p-l-5px b-s-b-b small c-p-1 c-br-1">
                        <i className="fas fa-arrow-alt-circle-down c-p-1 ns-c-1 p-l-5px p-r-5px"></i>
                        <div className="w-auto h-auto h-pr-fl-ma ns-c-1 f-w-bo">Responde a alguna de las siguientes preguntas en tu descripción.</div>
                    </div>

                </div>

                <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                    <div
                        className="w-100 h-auto h-pr-fl-ma c-f-1 f-w-bo ns-bg-1 ns-c-1  p-l-5px b-s-b-b small c-p-1 c-br-1">
                        <i className="fas fa-user-tie ns-c-e p-l-5px p-r-5px"></i>
                        ¿La entrevista será formal o casual?</div>
                </div>

                <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                    <div
                        className="w-100 h-auto h-pr-fl-ma c-f-1 f-w-bo ns-bg-1 ns-c-1 p-l-5px b-s-b-b small c-p-1 c-br-1">
                        <i className="fas fa-users ns-c-e p-l-5px p-r-5px"></i>
                        ¿Será una entrevista grupal o individual?</div>
                </div>

                <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                    <div
                        className="w-100 h-auto h-pr-fl-ma c-f-1 f-w-bo ns-bg-1 ns-c-1 p-l-5px b-s-b-b small c-p-1 c-br-1">
                        <i className="fas fa-brain ns-c-e p-l-5px p-r-5px"></i>
                        ¿Habrá pruebas lógicas o psicológicas?</div>
                </div>
                <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                    <div
                        className="w-100 h-auto h-pr-fl-ma c-f-1 f-w-bo ns-bg-1 ns-c-1 p-l-5px b-s-b-b small c-p-1 c-br-1">
                        <i className="fas fa-hand-peace ns-c-e p-l-5px p-r-5px"></i>
                        ¿De no poder asistir, habrá otra oportunidad?</div>
                </div>
            </div>

             {/* TODO |                                  | DESCRIPCIÓN ENTREVISTA |  */}
            <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                <div className="w-100 m-w-100 h-auto h-pr-fl-ma c-p-1 ns-c-1">
                <DataBox title="DESCRIPCIÓN:" titleStyle="m-b-5px">
                    <textarea
                      maxlength="350"
                      className="w-100 h-10vh c-br-1 txt-a-r-n h-pr-fl-ma br-none ns-bg-1 ns-c-1 c-p-1 "
                      placeholder="Ingrese una descripción breve de su entrevista (Max 350 caracteres.)"
                    ></textarea>
                  </DataBox>
                </div>
                </div>
            </div>

        </div>
    </div>

    <div className="w-100 h-auto flex-auto h-pr-fl-ma c-p-2 ">
      <div className="w-40 m-w-100  h-auto flex-auto h-pr-fl-ma r-h-c">
        <Btn
            className="w-100 normal c-p-1"
            theme="info"
            text="CREAR ENTREVISTA"
            iconLeft="fas fa-plus"
    ></Btn>
    </div>
    </div>


</div>
</Fragment>
  );
}
export default BusinessCreateInterview
