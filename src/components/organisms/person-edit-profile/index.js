import React, { Fragment } from "react";
import "./styles.css";

import { Btn, Inp, Sct, Opt, Spt, DatList } from "../../atoms/index.js";
import { ProfileCoverPage, ProfilePicture, Notify, DataBox } from "../../molecules/index.js";

const PersonEditProfile = () => {
    return (
      <Fragment>
        <div className="w-100 h-100 h-pr-fl-ma c-br-2 small c-p-2 of-x-hidden of-y-auto">

          <div className="w-100 h-auto flexbox h-pr-fl-ma d-i-f m-b-5px">
          <ProfileCoverPage className="h-100 c-br-1 of-hidden bg-op-10"></ProfileCoverPage>
            <div className="w-20 h-auto flex-auto h-pr-fl-ma c-p-1 b-s-b-b ">
              <div className="w-auto m-w-100 ns-bg-2 c-br-3 ns-c-1 h-auto h-pr-fl-ma r-h-c">

              <ProfilePicture className="w-and-h-125px m-w-80px m-h-80px"></ProfilePicture>

              </div>
            </div>
            <div className="w-50 h-auto flex-auto d-i-f h-pr-fl-ma c-p-2 c-br-2">
              <div className="w-100 h-auto h-pr-fl-ma">
                <div className="w-100 h-auto c-f-1 d-i-f c-p-1 f-w-bo">
                  <div className="c-br-1 ns-c-1 ns-bg-1 ns-c-1 medium c-p-1 c-f-1 br-none">
                    NOMBRE Y APELLIDO
                  </div>
                </div>
                <div className="w-100 h-auto c-f-1 c-p-1 normal">
                  <input
                    className="c-br-1 ns-bg-1 ns-c-1 ns-c-1 normal c-p-1 c-f-1 br-none"
                    placeholder="FRASE PERSONAL"
                  />
                </div>
              </div>
            </div>
            <div className="w-30 h-auto flex-auto h-pr-fl-ma c-p-1 b-s-b-b">
            <Notify theme="ns-bg-1  ns-b-s" iconSize="medium" icon="fas fa-check" iconColor="success" title="Tus cambios se han guardado correctamente" titleSize="nano"></Notify>

          </div>
          </div>

          <div className="w-100 h-auto small h-pr-fl-ma c-p-1 ns-bg-2 c-p-1 c-br-1">
          <DataBox text="DATOS BASICOS" textStyle="m-b-5px t-a-c ns-c-e-c ns-bg-e"></DataBox>


            {/*
TODO ██ ██████  ███████ ███    ██ ████████ ██ ██████   █████  ██████
TODO ██ ██   ██ ██      ████   ██    ██    ██ ██   ██ ██   ██ ██   ██
TODO ██ ██   ██ █████   ██ ██  ██    ██    ██ ██   ██ ███████ ██   ██
TODO ██ ██   ██ ██      ██  ██ ██    ██    ██ ██   ██ ██   ██ ██   ██
TODO ██ ██████  ███████ ██   ████    ██    ██ ██████  ██   ██ ██████
     */}
            <div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w ns-bg-1 c-br-1 m-b-5px c-p-1 b-s-b-b ns-b-s ">
              <div className="w-10 m-w-100 m-d-b h-100 flex-auto h-pr-fl-ma c-p-1">
                <div className="w-40px h-40px h-pr-fl-ma r-h-c circle c-p-1 b-s-b-b alert">
                  {/* ? COLOR DEL INDICADOR  */}
                  <i className="fas fa-times c-white centered"></i>
                  {/* ? ICONO DEL INDICADOR */}
                </div>
              </div>
              <div className="w-90 m-w-100 m-d-b h-auto flex-auto h-pr-fl-ma ns-bg-2 b-s-b-b c-p-1 c-br-1">
              <DataBox text="IDENTIDAD" textStyle="m-b-5px t-a-c ns-c-e"></DataBox>
                {/* TODO |                                  | NOMBRE + APELLIDO | */}
                <div className=" w-100 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-50 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title=" NOMBRE:" titleStyle="m-b-5px">
                  <Inp
                        className="w-100"
                        iconLeft="fas fa-signature"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="NOMBRE"
                      />
                  </DataBox>
                  </div>

                  <div className="w-50 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="APELLIDO:" titleStyle="m-b-5px">
                  <Inp
                        className="w-100"
                        iconLeft="fas fa-signature"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="APELLIDO"
                      />
                      </DataBox>
                      </div>

                </div>

                {/* TODO |                                  | CÉDULA DE IDENTIDAD + FECHA DE NACIMIENTO | */}
                <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-50 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="CÉDULA IDENTIDAD:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100 ns-c-1"
                        iconLeft="fas fa-file-contract"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="DOCUMENTO DE IDENTIDAD"
                      />
                    </DataBox>
                  </div>
                  <div className="w-50 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="FECHA DE NACIMIENTO:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100"
                        iconLeft="fas fa-calendar-day"
                        theme="ns-bg-1"
                        orientation=""
                        type="date"
                        size="small"
                        placeholder=""
                      />
                </DataBox>
                  </div>
                </div>
              </div>
            </div>

            {/*
TODO ███████ ███████ ████████ ██    ██ ██████  ██  ██████  ███████
TODO ██      ██         ██    ██    ██ ██   ██ ██ ██    ██ ██
TODO █████   ███████    ██    ██    ██ ██   ██ ██ ██    ██ ███████
TODO ██           ██    ██    ██    ██ ██   ██ ██ ██    ██      ██
TODO ███████ ███████    ██     ██████  ██████  ██  ██████  ███████

 */}

            <div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w ns-bg-1 c-br-1 m-b-5px c-p-1 b-s-b-b ns-b-s">
              <div className="w-10 m-w-100 m-d-b h-100 flex-auto h-pr-fl-ma c-p-1">
                <div className="w-40px h-40px h-pr-fl-ma r-h-c circle c-p-1 b-s-b-b alert">
                  {/* ? COLOR DEL INDICADOR  */}
                  <i className="fas fa-times c-white centered"></i>
                  {/* ? ICONO DEL INDICADOR */}
                </div>
              </div>
              <div className="w-90 m-w-100 m-d-b h-auto flex-auto h-pr-fl-ma ns-bg-2 b-s-b-b c-p-1 c-br-1">
              <DataBox text="ESTUDIOS" textStyle="m-b-5px t-a-c ns-c-e"></DataBox>
                {/* TODO |                                  | ESTUDIOS + IDIOMAS | */}
                <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="NIVEL ACADÉMICO:" titleStyle="m-b-5px">
                      <Sct
                        className="w-100"
                        iconLeft="fas fa-graduation-cap"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                      >
                        <Opt
                          text="SELECCIONAR"
                          selected
                          hidden
                        />
                        <Opt text="SIN ESPECIFICAR" value="0" />
                        <Opt
                          text="PRIMARIA INCOMPLETA"
                          value="1"
                        />
                        <Opt
                          text="PRIMARIA CURSANDO"
                          value="2"
                        />
                        <Opt
                          text="PRIMARIA COMPLETA"
                          value="3"
                        />
                        <Opt
                          text="SECUNDARIA INCOMPLETA"
                          value="4"
                        />
                        <Opt
                          text="SECUNDARIA CURSANDO"
                          value="5"
                        />
                        <Opt
                          text="SECUNDARIA COMPLETA"
                          value="6"
                        />
                        <Opt
                          text="UNIVERSIDAD INCOMPLETA"
                          value="7"
                        />
                        <Opt
                          text="UNIVERSIDAD CURSANDO"
                          value="8"
                        />
                        <Opt
                          text="UNIVERSIDAD COMPLETA"
                          value="9"
                        />
                        <Opt
                          text="POST GRADO INCOMPLETO"
                          value="10"
                        />
                        <Opt
                          text="POST GRADO CURSANDO"
                          value="11"
                        />
                        <Opt
                          text="POST GRADO COMPLETO"
                          value="12"
                        />
                      </Sct>
                      </DataBox>
                  </div>
                  <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="Institución:" titleStyle="m-b-5px">
                  <Inp
                        className="w-100"
                        iconLeft="fas fa-map"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="INSTITUCIÓN"
                      />

                    </DataBox>
                    </div>
                    <div className="w-100 d-i-f h-auto h-pr-fl-ma ">
                    <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="DESDE:" titleStyle="m-b-5px">
                  <Inp
                        className="w-100"
                        iconLeft="fas fa-map"
                        theme="ns-bg-1"
                        orientation=""
                        type="date"
                        size="small"

                      />
                    </DataBox>
                    </div>
                    <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="HASTA:" titleStyle="m-b-5px">
                  <Inp
                        className="w-100"
                        iconLeft="fas fa-map"
                        theme="ns-bg-1"
                        orientation=""
                        type="date"
                        size="small"
                      />
                    </DataBox>
                    </div>
                    </div>
                    </div>

                    <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                  <Spt
                    text="¿Tienes más estudios?"
                    center
                    lineSize="h-2px"
                    font="c-f-1 f-w-bo"
                    lineColor="bg-skyblue-node"
                  ></Spt>
                  <div className="w-auto r-h-c h-pr-fl-ma">
                  <Btn
                    className="w-auto "
                    theme="ns-bg-1"
                    iconLeft="fas fa-plus"
                    size="small"
                    text="AÑADIR ESTUDIOS"
                  ></Btn>
                   </div>
                </div>

                <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-100 m-w-100 h-auto h-pr-fl-ma ">
                  <div className="w-50 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="IDIOMAS:" titleStyle="m-b-5px">
                      <Sct
                        className="w-100"
                        iconLeft="fas fa-language"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                      >
                        <Opt
                          text="SELECCIONAR"
                          selected
                          hidden
                        />
                        <Opt text="SIN ESPECIFICAR" value="0" />
                        <Opt text="ESPAÑOL" value="1" />
                        <Opt text="INGLES" value="2" />
                        <Opt text="PORTUGUES" value="3" />
                        <Opt text="ALEMAN" value="4" />
                        <Opt text="JAPONES" value="5" />
                        <Opt text="CHINO" value="6" />
                        <Opt text="FRANCES" value="7" />
                        <Opt text="ITALIANO" value="8" />
                        <Opt text="RUSO" value="9" />
                        <Opt text="LITUANO" value="10" />
                        <Opt text="ARABE" value="11" />
                        <Opt text="INDU" value="12" />
                        <Opt text="TAILANDES" value="13" />
                      </Sct>
                      </DataBox>
                  </div>
                  <div className="w-50 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="NIVEL:" titleStyle="m-b-5px">
                      <Sct
                        className="w-100"
                        iconLeft="fas fa-arrow-circle-up"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                      >
                        <Opt
                          text="SELECCIONAR"
                          selected
                          hidden
                        />
                        <Opt text="SIN ESPECIFICAR" value="0" />
                        <Opt text="BAJO" value="1" />
                        <Opt text="MEDIO" value="2" />
                        <Opt text="ALTO" value="3" />
                        <Opt text="NATIVO" value="4" />
                      </Sct>
                      </DataBox>
                  </div>
                  <div className="w-100 h-auto h-pr-fl-ma ">
                  <Spt
                    text="¿Sabes más idiomas?"
                    center
                    lineSize="h-2px"
                    font="c-f-1 f-w-bo"
                    lineColor="bg-skyblue-node"
                  ></Spt>
                  <div className="w-auto r-h-c h-pr-fl-ma"><Btn
                  className="w-auto "
                  theme="ns-bg-1"
                  iconLeft="fas fa-plus"
                  size="small"
                  text="AÑADIR IDIOMAS"
                ></Btn></div>

                </div>
                  </div>
                </div>

              </div>
            </div>

            {/*
TODO ██   ██  █████  ██████  ██ ██      ██ ████████  █████   ██████ ██  ██████  ███    ██ ███████ ███████
TODO ██   ██ ██   ██ ██   ██ ██ ██      ██    ██    ██   ██ ██      ██ ██    ██ ████   ██ ██      ██
TODO ███████ ███████ ██████  ██ ██      ██    ██    ███████ ██      ██ ██    ██ ██ ██  ██ █████   ███████
TODO ██   ██ ██   ██ ██   ██ ██ ██      ██    ██    ██   ██ ██      ██ ██    ██ ██  ██ ██ ██           ██
TODO ██   ██ ██   ██ ██████  ██ ███████ ██    ██    ██   ██  ██████ ██  ██████  ██   ████ ███████ ███████
 */}

            <div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w ns-bg-1 c-br-1 m-b-5px c-p-1 b-s-b-b ns-b-s">
              <div className="w-10 m-w-100 m-d-b h-100 flex-auto h-pr-fl-ma c-p-1">
                <div className="w-40px h-40px h-pr-fl-ma r-h-c circle c-p-1 b-s-b-b alert">
                  {/* ? COLOR DEL INDICADOR  */}
                  <i className="fas fa-times c-white centered"></i>
                  {/* ? ICONO DEL INDICADOR */}
                </div>
              </div>
              <div className="w-90 m-w-100 m-d-b h-auto flex-auto h-pr-fl-ma ns-bg-2 b-s-b-b c-p-1 c-br-1">
              <DataBox text="HABILITACIONES" textStyle="m-b-5px t-a-c ns-c-e"></DataBox>

                {/* TODO |                                  | HABILITACIONES | */}
                <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-50 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="LIBRETA DE CONDUCIR:" titleStyle="m-b-5px">
                      <Sct
                        className="w-100"
                        iconLeft="fas fa-address-card"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                      >
                        <Opt
                          theme="ns-bg-1"
                          text="SELECCIONAR"
                          selected
                          hidden
                        />
                        <Opt text="SIN ESPECIFICAR" value="0" />
                        <Opt text="NO TENGO" value="1" />
                        <Opt text="VIGENTE" value="2" />
                      </Sct>
                      </DataBox>
                  </div>
                  <div className="w-50 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="VEHICULO PROPIO:" titleStyle="m-b-5px">
                      <Sct
                        className="w-100"
                        iconLeft="fas fa-car"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                      >
                        <Opt
                          text="SELECCIONAR"
                          selected
                          hidden
                        />
                        <Opt text="SIN ESPECIFICAR" value="0" />
                        <Opt text="NO TENGO" value="1" />
                        <Opt text="CICLOMOTOR" value="2" />
                        <Opt text="MOTOCICLETA" value="3" />
                        <Opt text="AUTO" value="4" />
                        <Opt text="CAMIÓN PEQUEÑO" value="5" />
                        <Opt text="CAMIÓN MEDIANO" value="6" />
                        <Opt text="CAMIÓN GRANDE" value="7" />
                        <Opt
                          text="CAMIÓN UTILITARIO"
                          value="8"
                        />
                        <Opt
                          text="CAMIÓN INDUSTRIAL"
                          value="9"
                        />
                        <Opt  text="MICRO BUS" value="10" />
                        <Opt  text="OMNIBUS" value="11" />
                        <Opt  text="MAQUINARIA VIAL" value="12" />
                        <Opt
                          text="MAQUINARIA AGRICOLA"
                          value="13"
                        />
                        <Opt
                          text="AERONAVE UNITARIA"
                          value="14"
                        />
                        <Opt
                          text="AERONAVE COMERCIAL"
                          value="15"
                        />
                        <Opt
                          text="EMBARCACIÓN PEQUEÑA"
                          value="16"
                        />
                        <Opt
                          text="EMBARCACIÓN MEDIANA"
                          value="17"
                        />
                        <Opt
                          text="EMBARCACIÓN GRANDE"
                          value="18"
                        />
                        <Opt
                          text="EMBARCACIÓN UTILITARIA"
                          value="19"
                        />
                        <Opt
                          text="EMBARCACIÓN INDUSTRIAL"
                          value="20"
                        />
                      </Sct>
                      </DataBox>
                  </div>
                </div>
                <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-50 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="CARNÉ DE SALUD:" titleStyle="m-b-5px">
                      <Sct
                        className="w-100"
                        iconLeft="fas fa-notes-medical"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                      >
                        <Opt
                          text="SELECCIONAR"
                          selected
                          hidden
                        />
                        <Opt text="NO TENGO" value="0" />
                        <Opt text="VIGENTE" value="1" />
                      </Sct>
                      </DataBox>
                  </div>
                  <div className="w-50 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="CARNÉ DE MANIPULACIÓN ALIMENTOS:" titleStyle="m-b-5px">
                      <Sct
                        className="w-100"
                        iconLeft="fas fa-apple-alt"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                      >
                        <Opt
                          text="SELECCIONAR"
                          selected
                          hidden
                        />
                        <Opt text="NO TENGO" value="0" />
                        <Opt text="VIGENTE" value="1" />
                      </Sct>
                      </DataBox>

                  </div>
                </div>
              </div>
            </div>
            {/*

TODO  ██    ██ ██████  ██  ██████  █████   ██████ ██  ██████  ███    ██
TODO  ██    ██ ██   ██ ██ ██      ██   ██ ██      ██ ██    ██ ████   ██
TODO  ██    ██ ██████  ██ ██      ███████ ██      ██ ██    ██ ██ ██  ██
TODO  ██    ██ ██   ██ ██ ██      ██   ██ ██      ██ ██    ██ ██  ██ ██
TODO   ██████  ██████  ██  ██████ ██   ██  ██████ ██  ██████  ██   ████

         */}
            {/* TODO |                                  | PAÍS DE NACIMIENTO + CIUDAD DE NACIMIENTO | */}
            <div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w ns-bg-1 c-br-1 m-b-5px c-p-1 b-s-b-b ns-b-s">
              <div className="w-10 m-w-100 m-d-b h-100 flex-auto h-pr-fl-ma c-p-1">
                <div className="w-40px h-40px h-pr-fl-ma r-h-c circle c-p-1 b-s-b-b alert">
                  <i className="fas fa-times c-white centered"></i>
                </div>
              </div>
              <div className="w-90 m-w-100 m-d-b h-auto flex-auto h-pr-fl-ma ns-bg-2 b-s-b-b c-p-1 c-br-1">
              <DataBox text="UBICACIÓN" textStyle="m-b-5px t-a-c ns-c-e"></DataBox>
                <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="PAÍS ACTUAL:" titleStyle="m-b-5px">
                      <Sct
                        className="w-100"
                        iconLeft="fas fa-flag"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                      >
                        <Opt

                          text="SELECCIONAR"
                          selected
                          hidden
                        />
                        <Opt text="ARGENTINA" value="0" />
                        <Opt text="URUGUAY" value="1" />
                        <Opt text="PARAGUAY" value="2" />
                        <Opt text="CHILE" value="3" />
                        <Opt text="MEXICO" value="4" />
                      </Sct>

                    </DataBox>
                  </div>
                  <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="CIUDAD ACTUAL:" titleStyle="m-b-5px">
                      <Sct
                        className="w-100"
                        iconLeft="fas fa-map-marked-alt"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                      >
                        <Opt

                          text="SELECCIONAR"
                          selected
                          hidden
                        />
                        <Opt

                          text="Seleccione su pais"
                          value="0"
                        />
                      </Sct>
                      </DataBox>
                  </div>
                </div>
                {/* TODO |                                  | DIRECCIÓN DE PERSONA | */}
                <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="BARRIO:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100"
                        iconLeft="fas fa-map"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="BARRIO"
                      />
                     </DataBox>
                  </div>
                  <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="CALLE:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100"
                        iconLeft="fas fa-road"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="CALLE"
                      />
                     </DataBox>
                  </div>
                  <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="INTERSECCIÓN:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100"
                        iconLeft="fas fa-directions"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="INTERSECCIÓN"
                      />
                    </DataBox>
                  </div>
                  <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="APARTAMENTO:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100"
                        iconLeft="fas fa-home"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="APARTAMENTO"
                      />
                    </DataBox>
                  </div>

                </div>
                {/* TODO |                                  | DIRECCIÓN DE PERSONA | */}
                <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="PISO:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100"
                        iconLeft="fas fa-building"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="PISO"
                      />
                   </DataBox>
                  </div>
                  <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="NUMERO DE PUERTA:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100"
                        iconLeft="fas fa-door-closed"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="N° PUERTA"
                      />
                    </DataBox>
                  </div>
                  <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="CÓDIGO POSTAL:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100"
                        iconLeft="fas fa-mail-bulk"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="CÓDIGO POSTAL"
                      />
                    </DataBox>
                  </div>
                  <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="UBICACIÓN ACTUAL:" titleStyle="m-b-5px">
                      <Btn
                        className="w-100"
                        iconLeft="fas fa-map-marker-alt"
                        theme="ns-bg-e ns-c-e-c f-w-bo"
                        orientation=""
                        size="small"
                        text="ACTUALIZAR"
                      />
                    </DataBox>
                  </div>
                </div>
              </div>
            </div>
            {/*

TODO  ██████  ██████  ███    ██ ████████  █████   ██████ ████████  ██████
TODO ██      ██    ██ ████   ██    ██    ██   ██ ██         ██    ██    ██
TODO ██      ██    ██ ██ ██  ██    ██    ███████ ██         ██    ██    ██
TODO ██      ██    ██ ██  ██ ██    ██    ██   ██ ██         ██    ██    ██
TODO  ██████  ██████  ██   ████    ██    ██   ██  ██████    ██     ██████


         */}
            <div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w ns-bg-1 c-br-1 m-b-5px c-p-1 b-s-b-b ns-b-s  ">
              <div className="w-10 m-w-100 m-d-b h-100 flex-auto h-pr-fl-ma c-p-1">
                <div className="w-40px h-40px h-pr-fl-ma r-h-c circle c-p-1 b-s-b-b alert">
                  <i className="fas fa-times c-white centered"></i>
                </div>
              </div>
              <div className="w-90 m-w-100 m-d-b h-auto flex-auto h-pr-fl-ma ns-bg-2 b-s-b-b c-p-1 c-br-1">
              <DataBox text="CONTACTO" textStyle="m-b-5px t-a-c ns-c-e"></DataBox>
                {/* TODO |                                  | REDES SOCIALES | */}
                <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="LINKEDIN:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100"
                        iconLeft="fab fa-linkedin"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="ENLACE"
                      />
                    </DataBox>
                  </div>
                  <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="TWITTER:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100"
                        iconLeft="fab fa-twitter"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="ENLACE"
                      />
                   </DataBox>
                  </div>
                  <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="INSTAGRAM:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100"
                        iconLeft="fab fa-instagram"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="ENLACE"
                      />
                    </DataBox>
                  </div>
                  <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="INSTAGRAM:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100"
                        iconLeft="fab fa-facebook"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="ENLACE"
                      />
                    </DataBox>
                  </div>
                </div>
                <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="SoundCloud:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100"
                        iconLeft="fab fa-soundcloud"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="ENLACE"
                      />
                    </DataBox>
                  </div>
                  <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="YOUTUBE:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100"
                        iconLeft="fab fa-youtube"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="ENLACE"
                      />
                   </DataBox>
                  </div>
                  <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="PATREON:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100"
                        iconLeft="fab fa-patreon"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="ENLACE"
                      />
                    </DataBox>
                  </div>
                  <div className="w-25 m-w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="SPOTIFY:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100"
                        iconLeft="fab fa-spotify"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="ENLACE"
                      />
                    </DataBox>
                  </div>
                </div>
                <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                    <div className="w-100 h-auto h-pr-fl-ma">
                    <DataBox title="TELÉFONO:" titleStyle="m-b-5px">
                        <Inp
                          className="w-100"
                          iconLeft="fas fa-phone"
                          theme="ns-bg-1"
                          orientation=""
                          type="text"
                          size="small"
                          placeholder="+000-000-000-000"
                        />
                      </DataBox>
                    </div>
                  </div>

                  <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                    <div className="w-100 h-auto h-pr-fl-ma">
                    <DataBox title="TELÉFONO 2:" titleStyle="m-b-5px">
                        <Inp
                          className="w-100"
                          iconLeft="fas fa-phone"
                          theme="ns-bg-1"
                          orientation=""
                          type="text"
                          size="small"
                          placeholder="+000-000-000-000"
                        />
                      </DataBox>
                    </div>
                  </div>
                </div>
                <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-100 m-w-100 h-auto h-pr-fl-ma c-p-1">
                 <DataBox title="INGRESE SU SITIO WEB:" titleStyle="m-b-5px">
                      <Inp
                        className="w-100"
                        iconLeft="fas fa-globe"
                        theme="ns-bg-1"
                        orientation=""
                        type="text"
                        size="small"
                        placeholder="https://nodesystems.uy"
                      />
                    </DataBox>
                  </div>
                </div>
              </div>
            </div>

            {/*
TODO ██████  ███████ ███████  ██████ ██████  ██ ██████   ██████ ██  ██████  ███    ██
TODO ██   ██ ██      ██      ██      ██   ██ ██ ██   ██ ██      ██ ██    ██ ████   ██
TODO ██   ██ █████   ███████ ██      ██████  ██ ██████  ██      ██ ██    ██ ██ ██  ██
TODO ██   ██ ██           ██ ██      ██   ██ ██ ██      ██      ██ ██    ██ ██  ██ ██
TODO ██████  ███████ ███████  ██████ ██   ██ ██ ██       ██████ ██  ██████  ██   ████
*/}

            <div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w ns-bg-1 c-br-1 m-b-5px c-p-1 b-s-b-b ns-b-s  ">
              <div className="w-10 m-w-100 m-d-b h-100 flex-auto h-pr-fl-ma c-p-1">
                <div className="w-40px h-40px h-pr-fl-ma r-h-c circle c-p-1 b-s-b-b alert">
                  <i className="fas fa-times c-white centered"></i>
                </div>
              </div>
              <div className="w-90 m-w-100 m-d-b h-auto flex-auto h-pr-fl-ma ns-bg-2 b-s-b-b c-p-1 c-br-1">
              <DataBox text="DESCRIPCIÓN" textStyle="m-b-5px t-a-c ns-c-e"></DataBox>

                {/* TODO |                                  | SECTOR OBJETIVO + ACCESIBILIDADES REQUERIDAS| */}
                <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="SECTOR OBJETIVO:" titleStyle="m-b-5px">
                    <DatList
                    type="datalist"
                    iconLeft="fas fa-bullseye"
                    theme="ns-bg-1"
                    className="w-100"
                    size="small"
                    placeholder="BUSCAR PUESTO"
                    >
                      <Opt value="SELECCIONAR" selected hidden />
                      <Opt value="PROGRAMADOR"></Opt>
                      <Opt value="MAQUETADOR FRONTEND"></Opt>
                      <Opt value="DISEÑADOR/A GRÁFICO"></Opt>
                      <Opt value="ANALISTA EN SISTEMAS"></Opt>
                      <Opt value="VENDEDOR/A"></Opt>
                      <Opt value="CARPINTERO/A"></Opt>
                      <Opt value="MÉDICO NUTRICIONISTA"></Opt>
                      <Opt value="MÉDICO CIRUJANO"></Opt>
                      <Opt value="MÉDICO NEFROLOGÍA"></Opt>
                      <Opt value="MALABARISTA"></Opt>
                      <Opt value="BARMAN"></Opt>
                      <Opt value="MESERO/A"></Opt>
                      <Opt value="CONTADOR/A"></Opt>
                      <Opt value="ABOGADO/A"></Opt>
                      <Opt value="FUTBOLISTA"></Opt>
                      <Opt value="JARDINERO/A"></Opt>
                      <Opt value="MÉDICO ANESTESISTA"></Opt>
                    </DatList>
                    </DataBox>
                  </div>
                  <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="REQUIERE ALGUNA ACCESIBILIDAD:" titleStyle="m-b-5px">
                    <Sct
                      className="w-100"
                      iconLeft="fas fa-wheelchair"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                    >
                      <Opt text="SELECCIONAR" selected hidden />
                      <Opt value="0" text="NO REQUIERE" />
                      <Opt value="1" text="MOTORA" />
                      <Opt value="2" text="SENSORIAL" />
                      <Opt value="3" text="INTELECTUAL" />
                      <Opt value="4" text="PSÍQUICA" />
                      <Opt value="5" text="VISCERAL" />
                      <Opt value="6" text="MULTIPLE" />
                    </Sct>
                    </DataBox>
                  </div>
                </div>
                {/* TODO |                                  | DISPONIBILIDAD HORARIA + DISPONIBILIDAD PARA VIAJAR | */}
                <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="DISPONIBILIDAD HORARIA:" titleStyle="m-b-5px">
                    <Sct
                      className="w-100"
                      iconLeft="fas fa-clock"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                    >
                      <Opt text="SELECCIONAR" selected hidden />
                      <Opt value="0" text="DISPONIBILIDAD MINIMA " />
                      <Opt value="7" text="A PARTIR DE LA MADRUGADA (+ 01:00hs) " />
                      <Opt value="7" text="A PARTIR DE LA MAÑANA (+ 00:06hs)" />
                      <Opt value="7" text="A PARTIR DEL MEDIO DÍA (+ 12:00hs)" />
                      <Opt value="7" text="A PARTIR DE LA TARDE (+ 14:30hs)" />
                      <Opt value="7" text="A PARTIR DE LA NOCHE (+ 19:00hs)" />
                      <Opt value="1" text="- DE 5 HORAS SEMANALES" />
                      <Opt value="2" text="- DE 10 HORAS SEMANALES" />
                      <Opt value="3" text="- DE 20 HORAS SEMANALES" />
                      <Opt value="4" text="AL MENOS 20 HORAS SEMANALES" />
                      <Opt value="5" text="+ DE 20 HORAS SEMANALES " />
                      <Opt value="6" text="+ DE 30 HORAS SEMANALES " />
                      <Opt value="7" text="DISPONIBILIDAD COMPLETA " />

                    </Sct>
                    </DataBox>
                  </div>
                  <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="DISPONIBILIDAD PARA VIAJAR:" titleStyle="m-b-5px">
                    <Sct
                      className="w-100"
                      iconLeft="fas fa-plane-departure"
                      theme="ns-bg-1"
                      orientation=""
                      type="text"
                      size="small"
                    >
                      <Opt text="SELECCIONAR" selected hidden />
                      <Opt value="0" text="SOLO DESDE CASA (HOMEOFFICE)" />
                      <Opt value="1" text="LOCALIDAD (DENTRO DE LA MISMA CIUDAD)" />
                      <Opt value="2" text="CAMBIO DE CIUDAD (TODO EL PAÍS)" />
                      <Opt value="3" text="VIAJES INTERNACIONALES (MIGRACIÓN)" />
                    </Sct>
                    </DataBox>
                  </div>
                </div>
                {/* TODO |                                  | AYUDA DESCRIPCIÓN | */}
                <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-100  h-auto h-pr-fl-ma c-p-1">
                    <div className="w-100 h-auto h-pr-fl-ma c-f-1 d-i-f ns-bg-1 p-l-5px b-s-b-b small c-p-1 c-br-1">
                      <i className="fas fa-exclamation-circle c-p-1 c-green-node p-l-5px p-r-5px"></i>
                      <div className="w-auto h-auto c-green-node f-w-bo h-pr-fl-ma">
                        Ayudale a las empresas a elegirte.
                      </div>
                    </div>
                    <div className="w-100 h-auto h-pr-fl-ma d-i-f c-f-1 ns-bg-2 p-l-5px b-s-b-b small c-p-1 c-br-1">
                      <i className="fas fa-arrow-alt-circle-down c-p-1 ns-c-1 p-l-5px p-r-5px"></i>
                      <div className="w-auto h-auto f-w-bo h-pr-fl-ma">
                        Responde a alguna de las siguientes preguntas en tu
                        descripción.
                      </div>
                    </div>
                  </div>

                  <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                    <div className="w-100 h-auto ns-bg-1 ns-c-1 f-w-bo m-b-5px h-pr-fl-ma c-f-1 p-l-5px b-s-b-b small c-br-1 c-p-1 c-br-1">
                      <i className="fas fa-brain ns-c-e p-l-5px p-r-5px"></i>
                      ¿Cuáles son tus mejores cualidades?
                    </div>
                  </div>

                  <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                    <div className="w-100 h-auto ns-bg-1 ns-c-1 f-w-bo m-b-5px h-pr-fl-ma c-f-1 p-l-5px b-s-b-b small c-br-1 c-p-1 c-br-1">
                      <i className="far fa-clock ns-c-e p-l-5px p-r-5px"></i>
                      ¿Tienes disponibilidad horaria?
                    </div>
                  </div>

                  <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                    <div className="w-100 h-auto ns-bg-1 ns-c-1 f-w-bo m-b-5px h-pr-fl-ma c-f-1 p-l-5px b-s-b-b small c-br-1 c-p-1 c-br-1">
                      <i className="fas fa-chart-bar ns-c-e p-l-5px p-r-5px"></i>
                      ¿Por qué deberían contratarte?
                    </div>
                  </div>
                  <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
                    <div className="w-100 h-auto ns-bg-1 ns-c-1 f-w-bo m-b-5px h-pr-fl-ma c-f-1 p-l-5px b-s-b-b small c-br-1 c-p-1 c-br-1">
                      <i className="fas fa-diagnoses ns-c-e p-l-5px p-r-5px"></i>
                      ¿Qué te hace diferente al resto?
                    </div>
                  </div>
                </div>
                {/* TODO |                                  | DESCRIPCIÓN PERSONA | */}
                <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                  <div className="w-100 m-w-100 h-auto h-pr-fl-ma c-p-1">
                  <DataBox title="DESCRIPCIÓN:" titleStyle="m-b-5px">
                      <textarea
                        maxlength="350"
                        className="w-100 h-10vh c-br-1 txt-a-r-n h-pr-fl-ma br-none ns-bg-1 c-p-1 ns-c-1"
                        placeholder="Ingrese una descripción breve acerca de ti. (Max 350 caracteres.)"
                      ></textarea>
                    </DataBox>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="w-100 h-auto flex-auto h-pr-fl-ma c-p-2 ">
            <div className="w-40 m-w-100 h-auto flex-auto h-pr-fl-ma r-h-c">
              <Btn
                className="w-100 c-p-1 normal"
                theme="success"
                text="GUARDAR CAMBIOS"
                iconLeft="fas fa-save"
              ></Btn>
            </div>
          </div>
        </div>
      </Fragment>
    );
}

export default PersonEditProfile
