import React, { Fragment } from "react";

import { SidebarBtn, Text } from "../../atoms/index.js";

import {
  PublicationControlers,
  PublicationBusiness,
  PublicationFilter,
  Pagination,
} from "../../molecules/index.js";
import "./styles.css";

const BusinessPublications = () => {
  return (
    <Fragment>
      <div className="w-100 h-100 h-pr-fl-ma ns-bg-1 c-p-1">

        <div className="w-100 h-10 h-pr-fl-ma b-s-b-b c-p-1 ns-bg-1 c-br-1  ">

        <div className="w-100 h-auto h-pr-fl-ma d-i-f r-v-c ns-bg-2 c-br-1">
          <div className="w-25 h-auto h-pr-fl-ma c-p-1"><SidebarBtn text="URGENTE" textSize="small f-w-bo p-l-5px" theme="alert" iconRight="fas fa-exclamation-circle"></SidebarBtn></div>
          <div className="w-25 h-auto h-pr-fl-ma c-p-1"><SidebarBtn text="TODOS" textSize="small f-w-bo p-l-5px" theme="ns-bg-1 ns-c-1" iconRight="fas fa-globe-americas"></SidebarBtn></div>
          <div className="w-25 h-auto h-pr-fl-ma c-p-1"><SidebarBtn text="ANTIGUAS" textSize="small f-w-bo p-l-5px" theme="ns-bg-1 ns-c-1" iconRight="fas fa-history"></SidebarBtn></div>
          <div className="w-25 h-auto h-pr-fl-ma c-p-1"><SidebarBtn text="PRIORIDAD" textSize="small f-w-bo p-l-5px" theme="bg-blue-node c-white" iconRight="fas fa-wheelchair"></SidebarBtn></div>
        </div>
        </div>
        <div className="w-100 h-70 h-pr-fl-ma small of-y-auto of-x-none ns-bg-2 c-br-1 c-p-1">
          <PublicationBusiness></PublicationBusiness>
          <PublicationBusiness></PublicationBusiness>
          <PublicationBusiness></PublicationBusiness>
          <PublicationBusiness></PublicationBusiness>
        </div>
        <div className="w-100 h-10 h-pr-fl-ma ns-bg-1 ns-b-s b-s-b-b">
          <div className="w-100 h-auto h-pr-fl-ma r-v-c">
            <PublicationControlers ></PublicationControlers></div>
        </div>
        <div className="w-100 h-10 h-pr-fl-ma c-p-1 b-s-b-b">

            <Pagination className="w-100 h-pr-fl-ma r-v-c"></Pagination>

          </div>
      </div>
    </Fragment>
  );
}

export default BusinessPublications
