import React, { Fragment } from "react";
import "./styles.css";
import { Btn } from "../../atoms/index.js";
import { ProfileCoverPage, ProfilePicture } from "../../molecules";

const PersonPreviewPost = () => {
    return (
      <Fragment>
        <div className="w-100 h-100 h-pr-fl-ma small c-bg-9 c-f-1 c-p-1 c-br-2 ">
          <div className="w-100 h-90 h-pr-fl-ma">
          <ProfileCoverPage className="h-100px"></ProfileCoverPage>
            <ProfilePicture className="w-and-h-100px m-t-30px r-h-c m-b-5px"></ProfilePicture>

          <div className="max-w-90 m-b-10px r-h-c t-a-c flexbox d-i-f h-auto  h-e c-p c-p-1 c-br-1 c-bg-8 h-pr-fl-ma b-s-b-b">
              <div className="w-100 p-l-5px t-of-e c-f-1 t-a-l of-hidden h-auto c-white">
                <div className="p-r-10px t-a-c h-auto h-pr-fl-ma">
                  <i className="fas fa-briefcase c-fuchsia-node"></i>
                </div>
                <span className="p-r-10px">NOMBRE EMPRESA</span>
              </div>
            </div>
          <div className="w-100 h-65 h-pr-fl-ma of-y-auto">
            {/* TODO                            | CARGO | */}
            <div className="w-100 h-auto h-pr-fl-ma m-b-5px">
              <div className="w-100 h-auto h-pr-fl-ma nano f-w-bo c-bg-10 c-p-1 c-br-1 ">
              <i className="fas fa-bullseye c-fuchsia-node h-pr-fl-ma m-r-4px m-t-2px"></i><span className="h-pr-fl-ma">PUESTO OFRECIDO:</span>
              </div>
              <span className="w-100 h-auto h-pr-fl-ma small c-p-1 c-bg-8 c-br-1">
                PROGRAMADOR
              </span>
            </div>
            {/* TODO                            | SEXO REQUERIDO Y EDAD | */}
            <div className="w-100 h-auto h-pr-fl-ma m-b-5px">
              <div className="w-100 h-auto h-pr-fl-ma nano f-w-bo c-bg-10 c-p-1 c-br-1 ">
               <i className="fas fa-restroom c-fuchsia-node h-pr-fl-ma m-r-4px m-t-2px"></i><span className="h-pr-fl-ma">SEXO Y EDAD REQUERIDA:</span>
              </div>
              <div className="w-100 h-auto h-pr-fl-ma small d-i c-p-1 c-bg-8 c-br-1">
                <span className="w-auto h-auto h-pr-fl-ma p-l-5px">
                  INDEFINIDO
                </span>
                <span className="w-auto h-auto h-pr-fl-ma p-l-5px">+</span>
                <span className="w-auto h-auto h-pr-fl-ma p-l-5px">
                  21 AÑOS
                </span>
              </div>
            </div>
            {/* TODO                            | ESTUDIOS | */}
            <div className="w-100 h-auto h-pr-fl-ma m-b-5px">
              <div className="w-100 h-auto h-pr-fl-ma nano f-w-bo c-bg-10 c-p-1 c-br-1 ">
              <i className="fas fa-graduation-cap c-fuchsia-node h-pr-fl-ma m-r-4px m-t-2px"></i><span className="h-pr-fl-ma">ESTUDIOS REQUERIDOS:</span>
              </div>
              <span className="w-100 h-auto h-pr-fl-ma small c-p-1 c-bg-8 c-br-1">
                SECUNDARIA INCOMPLETA
              </span>
            </div>
            {/* TODO                            | DISPONIBILIDAD HORARIA | */}
            <div className="w-100 h-auto h-pr-fl-ma m-b-5px">
              <div className="w-100 h-auto h-pr-fl-ma nano f-w-bo c-bg-10 c-p-1 c-br-1 ">
              <i className="fas fa-clock c-fuchsia-node h-pr-fl-ma m-r-4px m-t-2px"></i><span className="h-pr-fl-ma">DISPONIBILIDAD HORARIA:</span>
              </div>
              <span className="w-100 h-auto h-pr-fl-ma small c-p-1 c-bg-8 c-br-1">
                TIEMPO PARCIAL
              </span>
            </div>
            {/* TODO                            | DISPONIBILIDAD PARA VIAJAR | */}
            <div className="w-100 h-auto h-pr-fl-ma m-b-5px">
              <div className="w-100 h-auto h-pr-fl-ma nano f-w-bo c-bg-10 c-p-1 c-br-1 ">
                <i className="fas fa-plane-departure c-fuchsia-node h-pr-fl-ma m-r-4px m-t-2px"></i><span className="h-pr-fl-ma">DISPONIBILIDAD PARA VIAJAR:</span>
              </div>
              <span className="w-100 h-auto h-pr-fl-ma small c-p-1 c-bg-8 c-br-1">
                COMPLETA
              </span>
            </div>
            {/* TODO                            | IDIOMAS | */}
            <div className="w-100 h-auto h-pr-fl-ma m-b-5px">
              <div className="w-100 h-auto h-pr-fl-ma nano f-w-bo c-bg-10 c-p-1 c-br-1 ">
              <i className="fas fa-language c-fuchsia-node h-pr-fl-ma m-r-4px m-t-2px"></i><span className="h-pr-fl-ma">IDIOMAS REQUERIDOS:</span>
              </div>
              <div className="w-100 h-auto h-pr-fl-ma small c-p-1 c-bg-8 c-br-1">
                <span className="w-auto h-auto h-pr-fl-ma p-l-5px">
                  ESPAÑOL
                </span>
                <span className="w-auto h-auto h-pr-fl-ma p-l-5px">+</span>
                <span className="w-auto h-auto h-pr-fl-ma p-l-5px">
                  PORTUGUES
                </span>
                <span className="w-auto h-auto h-pr-fl-ma p-l-5px">+</span>
                <span className="w-auto h-auto h-pr-fl-ma p-l-5px">INGLES</span>
              </div>
            </div>
            {/* TODO                            | DESCRIPCIÓN | */}
            <div className="w-100 h-auto h-pr-fl-ma m-b-5px">
              <div className="w-100 h-auto h-pr-fl-ma nano f-w-bo c-bg-10 c-p-1 c-br-1 ">
              <i className="fas fa-signature c-fuchsia-node h-pr-fl-ma m-r-4px m-t-2px"></i><span className="h-pr-fl-ma">DESCRIPCIÓN:</span>
              </div>
              <div className="w-100 h-auto h-pr-fl-ma small c-p-1 c-bg-8 c-br-1 l-h-3em of-hidden ">
                <div className="w-auto h-auto h-pr-fl-ma p-l-5px small max-lines-3 of-hidden">
                  Enostrud aute velit quis anim eu culpa. Ut labore ullamco
                  eiusmod mollit Lorem deserunt fugiat consequat aliquip cillum.
                  Cillum commodo officia magna nisi. Tempor sunt occaecat minim
                  excepteur. Ad deserunt aliqua sint anim anim
                </div>
              </div>
            </div>
          </div>
          </div>
          <div className="w-100 h-10 h-pr-fl-ma">
            <div className="w-100 h-100 h-pr-fl-ma">
              <div className="w-100 c-p-1 r-v-b h-auto c-white h-pr-fl-ma">
                <Btn
                  className="w-100 c-p-1 small"
                  theme="success"
                  text="POSTULARME"
                  iconLeft="fas fa-plus"
                ></Btn>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
}

export default PersonPreviewPost
