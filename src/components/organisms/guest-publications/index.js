import React, { Fragment } from "react";
import "./styles.css";

import { Publication } from "../../molecules/index.js";
import { Btn, Text, SidebarBtn } from "../../atoms/index.js";

const GuestPublications = () => {
    return (
      <Fragment>


<div className="w-100 h-100 c-p-1 r-v-c h-pr-fl-ma">
<div className="w-100 h-10 h-pr-fl-ma b-s-b-b ">
          <div className="w-25 h-auto h-pr-fl-ma c-p-1"><SidebarBtn className="small f-w-bo p-l-3px" text="AVISOS" theme="info" iconRight="fas fa-newspaper "></SidebarBtn></div>
          <div className="w-25 h-auto h-pr-fl-ma c-p-1"><SidebarBtn className="small f-w-bo p-l-3px" text="ULTIMAS PUBLICACIONES" theme="default" iconRight="fas fa-list-alt c-fuchsia-node "></SidebarBtn></div>
          <div className="w-25 h-auto h-pr-fl-ma c-p-1"><SidebarBtn className="small f-w-bo p-l-3px" text="LLAMADOS SOCIALES" theme="default" iconRight="fas fa-heart c-red-node "></SidebarBtn></div>
          <div className="w-25 h-auto h-pr-fl-ma c-p-1"><SidebarBtn className="small f-w-bo p-l-3px" text="NOTAS DE VERSIÓN" theme="default" iconRight="fas fa-code c-green-node "></SidebarBtn></div>

        </div>

  <div className="w-100 h-80 m-d-n r-h-c of-auto of-x-hidden  h-pr-fl-ma">
    <Publication Visitor></Publication>
    <Publication Visitor></Publication>
    <Publication Visitor></Publication>
    <Publication Ads></Publication>
    <Publication Visitor></Publication>
    <Publication Visitor></Publication>
    <Publication Visitor></Publication>
    <Publication Visitor></Publication>
    <Publication Ads></Publication>
    <Publication Visitor></Publication>

   </div>
   <div className="w-100 h-10 h-pr-fl-ma">
   <div className="w-100 h-auto r-v-c h-pr-fl-ma">
      <Btn
          text="Ingresar como invitado"
          theme="default"
          IconLeft="fas fa-eye"
          size="small"
          className="h-e m-5px"></Btn>
  </div> </div>
</div>
</Fragment>
  );
}

export default GuestPublications
