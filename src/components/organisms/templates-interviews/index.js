import React, { Fragment } from "react";
import "./styles.css";

import {CardHorizontal} from '../../atoms/index.js';
import {DataBox} from "../../molecules/index.js";


export default function TemplatesInterviews(props) {
  const {  } = props;

  return (
    <Fragment>
<div className="w-100 h-100 h-pr-fl-ma ns-bg-1 c-br-2 c-p-2 of-x-hidden of-y-auto">

<DataBox text="MIS ENTREVISTAS" textStyle="info m-b-5px"></DataBox>

   <div className="w-100 h-auto h-pr-fl-ma c-br-1 t-a-c c-p-1 c-f-1 f-w-bo m-b-5px ns-bg-2 small">
          <div className="w-100 h-auto h-pr-fl-ma ">
            <div className="w-auto h-auto flexbox h-pr-fl-ma r-h-c ns-bg-1 d-i c-p-1 c-br-1 m-t-10px m-b-10px">
              <i className="w-auto h-auto fas fa-question-circle ns-c-e h-pr-fl-ma p-l-5px p-r-5px"></i>
              <div className="w-auto h-auto h-pr-fl-ma">¿Cómo funciona?</div>
            </div>
          </div>

          {/*   !      INICIO ------------------------------------------------------------ | (!) OCULTAR ESTO PARA OCULTAR INSTRUCCIONES | */}

          <div className="w-100 h-auto h-pr-fl-ma t-a-l ns-bg-1 ns-b-s c-br-1 c-p-1">
            <div className="w-33 m-w-100 h-auto h-pr-fl-ma c-p-1">
              <div className="flexbox w-auto h-pr-fl-ma m-w-100 d-i">
                <div className="w-auto d-i h-pr-fl-ma h-auto flex-auto c-p-2 ">
                  <div className="w-and-h-25px ns-bg-2 circle b-s-3px b-s-solid b-c-skyblue">
                    <div className="w-auto h-auto h-pr-fl-ma centered ">
                      <span className="ns-c-1 ">1</span>
                    </div>
                  </div>
                </div>
                <div className="w-auto h-auto h-pr-fl-ma flex-auto">
                  <span className="ns-c-1 h-pr-fl-ma w-100">
                  Cree sus entrevistas
                  </span>
                  <span className="ns-c-1 h-pr-fl-ma l-h-1-2em nano">
                  Dependiendo del sector o el cargo en el que necesite personal.
                  </span>
                </div>
              </div>
            </div>
            <div className="w-33 m-w-100 h-auto h-pr-fl-ma c-p-1">
              <div className="flexbox w-auto h-pr-fl-ma m-w-100 d-i">
                <div className="w-auto d-i h-pr-fl-ma h-auto flex-auto c-p-2 ">
                  <div className="w-and-h-25px ns-bg-2 circle b-s-3px b-s-solid b-c-green">
                    <div className="w-auto h-auto h-pr-fl-ma centered ">
                      <span className="ns-c-1">2</span>
                    </div>
                  </div>
                </div>
                <div className="w-auto h-auto h-pr-fl-ma flex-auto">
                  <span className="ns-c-1 h-pr-fl-ma w-100">
                   Encuentre su candidato
                  </span>
                  <span className="ns-c-1 h-pr-fl-ma l-h-1-2em nano">
                  Elija a una persona y solicite una entrevista.
                  </span>
                </div>
              </div>
            </div>
            <div className="w-33 m-w-100 h-auto h-pr-fl-ma c-p-1">
              <div className="flexbox w-auto h-pr-fl-ma m-w-100 d-i">
                <div className="w-auto d-i h-pr-fl-ma h-auto flex-auto c-p-2 ">
                  <div className="w-and-h-25px ns-bg-2 circle b-s-3px b-s-solid b-c-red">
                    <div className="w-auto h-auto h-pr-fl-ma centered ">
                      <span className="ns-c-1">3</span>
                    </div>
                  </div>
                </div>
                <div className="w-auto h-auto h-pr-fl-ma flex-auto">
                  <span className="ns-c-1 h-pr-fl-ma w-100">
                    Solicite una entrevista
                  </span>
                  <span className="ns-c-1 h-pr-fl-ma l-h-1-2em nano">
                  Al solicitar una entrevista elija una plantilla para enviarle a su destinatario.
                  </span>
                </div>
              </div>
            </div>
          </div>






    </div>
    <div className="w-100 h-auto h-pr-fl-ma ns-bg-2 c-p-1 c-br-1">
    <CardHorizontal
        title="TITULO"
        date="Creado hace 5 días"
        description="Eu quis quis voluptate consectetur proident cillum nostrud deserunt. est sit. Reprehenderit quis qui pariatur reprehenderit. In aliquip tempor ipsum ex ullamco dolor. Et labore ullamco aliqua aliquip ut sunt sunt tempor cillum esse cillum duis fugiat pariatur. Anim cupidatat pariatur cillum officia ullamco fugiat sit nulla ex aliqua veniam pariatur"></CardHorizontal>
        <CardHorizontal
        title="TITULO"
        date="Creado hace 5 días"
        description="Eu quis quis voluptate consectetur proident cillum nostrud deserunt. est sit. Reprehenderit quis qui pariatur reprehenderit. In aliquip tempor ipsum ex ullamco dolor. Et labore ullamco aliqua aliquip ut sunt sunt tempor cillum esse cillum duis fugiat pariatur. Anim cupidatat pariatur cillum officia ullamco fugiat sit nulla ex aliqua veniam pariatur"></CardHorizontal>
        <CardHorizontal
        title="TITULO"
        date="Creado hace 5 días"
        description="Eu quis quis voluptate consectetur proident cillum nostrud deserunt. est sit. Reprehenderit quis qui pariatur reprehenderit. In aliquip tempor ipsum ex ullamco dolor. Et labore ullamco aliqua aliquip ut sunt sunt tempor cillum esse cillum duis fugiat pariatur. Anim cupidatat pariatur cillum officia ullamco fugiat sit nulla ex aliqua veniam pariatur"></CardHorizontal>
        <CardHorizontal
        title="TITULO"
        date="Creado hace 5 días"
        description="Eu quis quis voluptate consectetur proident cillum nostrud deserunt. est sit. Reprehenderit quis qui pariatur reprehenderit. In aliquip tempor ipsum ex ullamco dolor. Et labore ullamco aliqua aliquip ut sunt sunt tempor cillum esse cillum duis fugiat pariatur. Anim cupidatat pariatur cillum officia ullamco fugiat sit nulla ex aliqua veniam pariatur"></CardHorizontal>
    <CardHorizontal
        title="TITULO"
        date="Creado hace 5 días"
        description="Eu quis quis voluptate consectetur proident cillum nostrud deserunt. est sit. Reprehenderit quis qui pariatur reprehenderit. In aliquip tempor ipsum ex ullamco dolor. Et labore ullamco aliqua aliquip ut sunt sunt tempor cillum esse cillum duis fugiat pariatur. Anim cupidatat pariatur cillum officia ullamco fugiat sit nulla ex aliqua veniam pariatur"></CardHorizontal>
</div>
</div>
</Fragment>
  );
}
