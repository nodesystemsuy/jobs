import React, { Fragment } from "react";

import { Btn, Text, Inp } from "../../atoms/index.js";
import { DataBox } from "../../molecules/index.js";
import "./styles.css";

const PasswordChange = () => {
    return (
      <Fragment>
        {/* ?

! ██████   █████  ███████ ███████      ██████ ██   ██  █████  ███    ██  ██████  ███████ 
! ██   ██ ██   ██ ██      ██          ██      ██   ██ ██   ██ ████   ██ ██       ██      
! ██████  ███████ ███████ ███████     ██      ███████ ███████ ██ ██  ██ ██   ███ █████ 
! ██      ██   ██      ██      ██     ██      ██   ██ ██   ██ ██  ██ ██ ██    ██ ██    
! ██      ██   ██ ███████ ███████      ██████ ██   ██ ██   ██ ██   ████  ██████  ███████                                                                                                     

? */}
        <form className="w-100 h-100 c-p-1 of-hidden b-s-b-b h-pr-fl-ma">

          <div className="w-80 m-w-70 h-auto h-pr-fl-ma centered">
          <div className="w-100 h-auto of-hidden c-p-1 ns-bg-1 ns-b-s c-br-1 h-pr-fl-ma">

            <div className="w-100 h-auto h-pr-fl-ma ns-bg-2 c-br-1 m-b-5px c-p-1">
            <DataBox text="NUEVA CONTRASEÑA:" textStyle="ns-bg-1 small "></DataBox></div>

            <div className="w-100 h-auto h-pr-fl-ma c-br-1 ns-bg-2 c-p-1">
               {/* PASSWORD NUEVO (TEXTO) */}
              <DataBox childrenStyle="m-b-5px ns-bg-1" className="small" >
                <Inp
                    className="w-100"
                    iconLeft="fas fa-asterisk"
                    theme="ns-bg-1 ns-c-1"
                    type="text"
                    size="normal"
                    placeholder="Ingrese su nueva contraseña"
                  />
                </DataBox>
                <DataBox childrenStyle="ns-bg-1" className="small" >
                <Inp
                    className="w-100"
                    iconLeft="fas fa-asterisk"
                    theme="ns-bg-1 ns-c-1"
                    type="text"
                    size="normal"
                    placeholder="Repita su nueva contraseña"
                  />
                </DataBox>
                </div>




              <div className="w-100 h-pr-fl-ma c-p-1">
                <Btn
                  theme="alert"
                  iconLeft="fas fa-check"
                  className="w-100 c-p-1"
                  size="normal"
                  text="Recuperar"
                ></Btn>
              </div>
            </div>
          </div>
        </form>
      </Fragment>
    );
}

export default PasswordChange
