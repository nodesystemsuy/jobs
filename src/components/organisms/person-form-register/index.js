import React, { Fragment } from "react";
import "./styles.css";
import { Btn, Text, Inp, Checkbox } from "../../atoms/index.js";
import {DataBox} from "../../molecules/index.js"
import { Link } from "react-router-dom";
import { HOME_REGISTER_BUSINESS, HOME_REGISTER_PERSON } from "../../../constants/routes.constant";

import { useFormik } from "formik";
import * as Yup from 'yup'; 

import { apiRequest } from "../../../store/services/UserService";

const PersonFormRegister = () => {
 
    const formik=useFormik({
      initialValues:{
        Name:"",
        LastName:"",
        NickName:"",
        Date:"",
        Email:"",
        Password:"",
        RepeatPassword:"",
        AcceptTerms:false
      },
      validationSchema: Yup.object({
        Name:Yup.string().required("Nombre Requerido"),
        LastName:Yup.string().required("Apellido Requerido"),
        NickName:Yup.string().required("NickName Requerido"),
        Date:Yup.string().required("Fecha de nacimiento Requerido"),
        Email:Yup.string().
          email("No es un correo válido").
          required("Email Requerido"),
        Password:Yup.string().
          required("Contaseña Requerida").
          oneOf([Yup.ref("RepeatPassword")],"Las contraseñas no son iguales"),
        RepeatPassword:Yup.string().
          required("Repetir Contaseña Requerida").
          oneOf([Yup.ref("Password")],"Las contraseñas no son iguales"),
        AcceptTerms:Yup.boolean().oneOf([true],'Necesitas Aceptar los terminos')
      }),
      onSubmit:(FormData)=>{
        console.log(formik.touched)
        /* apiRequest(
          "sign-up-person",
          {
            name:FormData.Name,
            surname:FormData.LastName,
            born_date:FormData.Date,
            nickname:FormData.NickName,
            email:FormData.Email,
            password:FormData.Password,
            accept_terms:true
          },
          "POST"
        ) */
      }
    
    })

    return (
      <Fragment>
        {/* ?

? ██████  ███████  ██████  ██ ███████ ████████ ███████ ██████      ██    ██ ███████ ███████ ██████ 
? ██   ██ ██      ██       ██ ██         ██    ██      ██   ██     ██    ██ ██      ██      ██   ██ 
? ██████  █████   ██   ███ ██ ███████    ██    █████   ██████      ██    ██ ███████ █████   ██████  
? ██   ██ ██      ██    ██ ██      ██    ██    ██      ██   ██     ██    ██      ██ ██      ██   ██ 
? ██   ██ ███████  ██████  ██ ███████    ██    ███████ ██   ██      ██████  ███████ ███████ ██   ██

? */}
        <form className="w-100 h-100 c-p-1 of-hidden b-s-b-b h-pr-fl-ma">
          <div className="w-90 m-w-70 h-auto h-pr-fl-ma centered">
            <div className="w-100 h-auto of-hidden c-p-1 ns-bg-1 ns-b-s c-br-1 h-pr-fl-ma">
            <div className="w-100 h-auto h-pr-fl-ma ns-bg-2 c-br-1 m-b-5px c-p-1">
            <DataBox text="REGISTRO PERSONAS" textStyle="ns-bg-1 small "></DataBox></div>

              <div className="w-100 b-s-b-b of-hidden h-auto h-pr-fl-ma ns-bg-2 m-b-5px c-br-1 c-p-1 d-i-f">
                <div className="w-50 b-s-b-b of-hidden  h-auto h-pr-fl-ma">
                  <Btn
                    theme="skyblue"
                    iconLeft="fas fa-user-circle"
                    text="Personas"
                    className="w-100 h-auto h-pr-fl-ma c-br-1 b-s-b-b c-br-1 t-a-c c-p"
                    type="button"
                    size="small"
                    path={HOME_REGISTER_PERSON}
                  ></Btn>
                </div>
                <div className="w-50 h-auto of-hidden h-e b-s-b-b h-pr-fl-ma">
                  <Btn
                    text="Empresas"
                    className="w-100 h-auto h-pr-fl-ma c-br-1 bg-none b-s-b-b c-br-1 t-a-c c-p"
                    type="button"
                    iconLeft="fas fa-briefcase"
                    size="small"
                    path={HOME_REGISTER_BUSINESS}
                  ></Btn>
                </div>
              </div>
              <div className="w-100 h-auto h-pr-fl-ma c-br-1 ns-bg-2 c-p-1">
              {/* NOMBRE Y APELLIDO  */}
              <div className="w-100 b-s-b-b h-auto h-pr-fl-ma">
                <div className="w-50 b-s-b-b h-auto h-pr-fl-ma c-p-1">
                <DataBox childrenStyle="m-b-0px ns-bg-1" status="alert ghost" className="small" textHelper={formik.touched.Name&&formik.errors.Name}>
                <Inp
                    className="w-100"
                    iconLeft="fas fa-signature"
                    theme="ns-bg-1 ns-c-1"
                    type="text"
                    size="normal"
                    placeholder="Nombre"
                    Name="Name"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </DataBox>
                </div>
                <div className="w-50 b-s-b-b h-auto h-pr-fl-ma c-p-1">
                <DataBox childrenStyle="m-b-0px ns-bg-1" status="alert ghost" className="small" textHelper={formik.touched.LastName&&formik.errors.LastName}>
                <Inp
                    iconLeft="fas fa-signature"
                    className="w-100"
                    theme="ns-bg-1 ns-c-1"
                    type="text"
                    size="normal"
                    placeholder="Apellido"
                    Name="LastName"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </DataBox>
                </div>
              </div>
               {/* NICKNAME Y DATE*/}
              <div className="w-100 h-auto h-pr-fl-ma">
                <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox classNameStyle="m-b-0px ns-bg-1" status="alert ghost" className="small" textHelper={formik.touched.NickName&&formik.errors.NickName}>
                <Inp
                    className="w-100"
                    iconLeft="fas fa-at"
                    theme="ns-bg-1 ns-c-1"
                    type="text"
                    size="normal"
                    placeholder="Nickname"
                    Name="NickName"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </DataBox>
                </div>
                <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox childrenStyle="m-b-0px ns-bg-1" status="ghost alert" className="small" textHelper={formik.touched.Date&&formik.errors.Date}>
                <Inp
                      className="w-100"
                      theme="ns-bg-1 ns-c-1"
                      size="normal"
                      type="date"
                      placeholder="date"
                      Name="Date"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                </DataBox>
                </div>



              {/* EMAIL  */}
              <div className="w-100 h-auto h-pr-fl-ma c-p-1">
              <DataBox childrenStyle="m-b-0px ns-bg-1" status="alert ghost" className="small" textHelper={formik.touched.Email&&formik.errors.Email}>
                <Inp
                    className="w-100"
                    iconLeft="fas fa-envelope"
                    theme="ns-bg-1 ns-c-1"
                    type="email"
                    size="normal"
                    placeholder="Email"
                    Name="Email"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  /></DataBox>
              </div>

              </div>
              {/* PASSWORD */}
              <div className="w-100 b-s-b-b h-auto h-pr-fl-ma">
                <div className="w-50 b-s-b-b h-auto h-pr-fl-ma c-p-1">
                <DataBox childrenStyle="m-b-0px ns-bg-1" status="alert ghost" className="small" textHelper={formik.touched.Password&&formik.errors.Password}>
                  <Inp
                    className="w-100"
                    iconLeft="fas fa-asterisk"
                    theme="ns-bg-1 ns-c-1"
                    type="password"
                    size="normal"
                    placeholder="Contraseña"
                    Name="Password"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  /></DataBox>

                </div>
                <div className="w-50 b-s-b-b h-auto h-pr-fl-ma c-p-1">
                <DataBox childrenStyle="m-b-0px ns-bg-1" status="alert ghost" className="small" textHelper={formik.touched.RepeatPassword&&formik.errors.RepeatPassword}>
                  <Inp
                    className="w-100"
                    iconLeft="fas fa-asterisk"
                    theme="ns-bg-1 ns-c-1"
                    type="password"
                    size="normal"
                    placeholder="Contraseña"
                    Name="RepeatPassword"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  /></DataBox>
                </div>
              </div>
              </div>

              {/* ACUERDO DE RESPONSABILIDAD (TEXTO) */}
              <div className="w-100 h-pr-fl-ma c-p-1">
                <div className="w-100 h-auto flexbox p-b-5px p-t-5px h-pr-fl-ma">
                  <div className="w-30 flex-auto h-auto h-pr-fl-ma">
                    <Checkbox
                      text="ACEPTO"
                      size="normal"
                      theme="info"
                      className="r-h-c"
                      Name="AcceptTerms"
                      onChange={(value)=>{console.log(value.checked)}}
                      onBlur={formik.handleBlur}
                    ></Checkbox>
                  </div>
                  <div className="w-70 flex-auto h-pr-fl-ma">
                    <Text
                      className="f-w-bo"
                      size="nano"
                      text="Entiendo y acepto todas las políticas, términos y condiciones de uso."
                    ></Text>
                  </div>
                </div>
                <div className="w-100 h-auto h-pr-fl-ma">
                  <Btn
                    theme="success"
                    iconLeft="fas fa-user-circle"
                    className="w-100 c-p-1"
                    size="normal"
                    text="REGISTRARSE"
                    type="submit"
                    onClick={formik.handleSubmit}
                  ></Btn>
                  
                </div>
              </div>
            </div>
          </div>
        </form>
      </Fragment>
    );
}

export default PersonFormRegister
