import React, { Fragment } from "react";
import "./styles.css";
import { Link } from 'react-router-dom'

import { SidebarBtn, Btn, ProgBar, Dropdown } from "../../atoms/index.js";
import { ProfileCoverPage, ProfilePicture, DataBox } from "../../molecules/index.js";
import {
  DASHBOARD_PERSON_EDITPROFILE,
  DASHBOARD_PERSON_MYPOSTULATIONS,
  DASHBOARD_PERSON_PROFILE,
  DASHBOARD_PERSON_PUBLICATIONS,
  DASHBOARD_PERSON_ALERTS,
} from "../../../constants/routes.constant";

const PersonSidebar = () => {
  return (
    <Fragment>
      <div className="w-100 h-100 h-pr-fl-ma  c-p-1 ns-bg-1 ns-b-s c-br-2 b-s-b-b of-hidden obj-fit-con">
        <div className="w-100 h-80 h-pr-fl-ma obj-fit-con">
          <ProfileCoverPage className="h-200px c-br-2"></ProfileCoverPage>
          <ProfilePicture className="w-and-h-100px m-t-20px ns-bg-1 m-b-20px r-h-c"></ProfilePicture>
          <div className="w-100 h-auto c-p-2 of-hidden b-s-b-b obj-fit-con">
            <div className="w-100 h-auto c-p-2 ns-bg-1 ns-b-s c-br-2 h-pr-fl-ma of-hidden">
              <Link to={DASHBOARD_PERSON_PROFILE}>
                <div className="w-100 h-auto h-pr-fl-ma ns-bg-2 c-p-1 c-br-1 m-b-5px">
                  <DataBox text="NOMBRE Y APELLIDO" textStyle="ns-bg-e ns-c-e-c f-w-bo flexbox l-h-1-1em f-w-w small b-s-b-b "></DataBox> </div>

              </Link>

              <div className="w-100 max-h-40vh h-pr-fl-ma c-p-1 ns-bg-2  c-br-1 of-y-auto obj-fit-con">
                <SidebarBtn
                  iconLeft="fas fa-bell"
                  counter="4321"
                  iconColor="c-red-node"
                  counterColor="bg-red-node"
                  text="Mis Alertas"
                  path={DASHBOARD_PERSON_ALERTS}
                  size="small "
                  theme="ns-bg-1 ns-c-1"
                ></SidebarBtn>
                <SidebarBtn
                  iconLeft="fas fa-list-alt"
                  counter="123"
                  iconColor="c-green-node"
                  counterColor="bg-green-node"
                  text="Mis Postulaciones"
                  size="small"
                  path={DASHBOARD_PERSON_MYPOSTULATIONS}
                  theme="ns-bg-1 ns-c-1"
                ></SidebarBtn>
                <SidebarBtn
                  iconLeft="fas fa-newspaper"
                  counter="245"
                  iconColor="c-fuchsia-node"
                  counterColor="bg-fuchsia-node"
                  text="Ver publicaciones"
                  size="small"
                  path={DASHBOARD_PERSON_PUBLICATIONS}
                  theme="ns-bg-1 ns-c-1"
                ></SidebarBtn>
                <Dropdown
                  iconLeft="fas fa-file-alt"
                  iconColor="grey"
                  text="Mis Curriculums"
                  size="small"
                  iconDropRight="true"
                  theme="ns-bg-1 ns-c-1"
                >{/*<SidebarBtn
                  className="w-100"
                  iconLeft="fas fa-file-alt"
                  iconColor="grey"
                  text="Nombre Archivo.pdf"
                  size="small"

                ></SidebarBtn>
                  <SidebarBtn
                    className="w-100"
                    iconLeft="fas fa-file-alt"
                    iconColor="grey"
                    text="Nombre Archivo.pdf"
                    size="small"

                  ></SidebarBtn>
                  <SidebarBtn
                    className="w-100"
                    iconLeft="fas fa-file-alt"
                    iconColor="grey"
                    text="Nombre Archivo.pdf"
                    size="small"

                  ></SidebarBtn>
                  <SidebarBtn
                    className="w-100"
                    iconLeft="fas fa-file-alt"
                    iconColor="grey"
                    text="Nombre Archivo.pdf"
                    size="small"

                  ></SidebarBtn>*/}

                </Dropdown>
                <SidebarBtn
                  iconLeft="fas fa-user-edit"
                  iconColor="c-skyblue-node"
                  text="Editar Perfil"
                  size="small"
                  path={DASHBOARD_PERSON_EDITPROFILE}
                  theme="ns-bg-1 ns-c-1"
                ></SidebarBtn>
                <SidebarBtn
                  iconLeft="fas fa-list-alt"
                  iconColor="c-skyblue-node"
                  text="Mis Entrevistas"
                  size="small"
                  theme="ns-bg-1 ns-c-1"
                ></SidebarBtn>

                <SidebarBtn
                  iconLeft="fas fa-chart-bar"
                  counter="654"
                  iconColor="c-blue-node"
                  counterColor="bg-blue-node"
                  text="Mis Estadisticas"
                  size="small"
                  theme="ns-bg-1 ns-c-1"
                ></SidebarBtn>
               <a
                  href="https://nodesystems.uy/"
                  className="ns-c-1"
                  target="_blank"
                >
                  <SidebarBtn
                    iconLeft="fas fa-user-circle c-yellow-node"
                    counterColor="bg-yellow-node"
                    text="Consejos Gratis"
                    size="small"
                    theme="ns-bg-1 ns-c-1"
                  ></SidebarBtn>
                </a>
              </div>
            </div>
          </div>
        </div>





        <div className="w-100 h-20 h-pr-fl-ma">
          <div className="w-100 d-n h-100 h-pr-fl-ma c-p-1">
            <div className="w-100 ns-bg-2 c-br-1 r-v-b h-auto small h-pr-fl-ma">

              <label
                for="SeleccionarCV" className="w-100 flexbox d-i-f h-auto h-pr-fl-ma c-p">
                <div className="w-70 h-auto flex-auto h-pr-fl-ma b-s-b-b c-p-1">
                  <div className="w-100 h-auto small h-pr-fl-ma c-br-1 p-l-5px p-r-5px ns-bg-1 b-s-b-b">
                    <div className="w-100 h-auto h-pr-fl-ma t-a-c c-f-1 small f-w-bo c-br-1 t-o-e max-l-1 of-h ">
                      <span className="small p-l-5px p-r-5px">Nombre archivo.txt</span></div>

                    <div className="w-100 h-auto m-b-5px c-br-1 h-pr-fl-ma c-p">
                      <ProgBar className="w-100 h-15px"></ProgBar>

                    </div> </div>
                </div>
                <div className="w-30 flex-auto h-pr-fl-ma c-p c-p-1">
                  <Btn className="w-100 h-35px h-pr-fl-ma" theme="info" size="normal" icon="fas fa-upload"></Btn></div>
              </label>

              <input
                className="w-100 nano ns-c-1 m-b-10px"
                type="file"
                id="SeleccionarCV"
                text="Cargar CV"
                hidden
              />

            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default PersonSidebar
