import React, { Fragment } from "react";
import "./styles.css";

import { Btn, Text, Inp } from "../../atoms/index.js";
import { DataBox } from "../../molecules/index.js";


const PasswordRecovery = () => {
    return (
      <Fragment>
        {/*

! ██████   █████  ███████ ███████     ██████  ███████  ██████  ██████  ██    ██ ███████ ██████  ██    ██ 
! ██   ██ ██   ██ ██      ██          ██   ██ ██      ██      ██    ██ ██    ██ ██      ██   ██  ██  ██  
! ██████  ███████ ███████ ███████     ██████  █████   ██      ██    ██ ██    ██ █████   ██████    ████  
! ██      ██   ██      ██      ██     ██   ██ ██      ██      ██    ██  ██  ██  ██      ██   ██    ██  
! ██      ██   ██ ███████ ███████     ██   ██ ███████  ██████  ██████    ████   ███████ ██   ██    ██ 
                                                                                                    

*/}

        <form className="w-100 h-100 c-p-1 of-hidden b-s-b-b h-pr-fl-ma">
          <div className="w-80 m-w-70 h-auto h-pr-fl-ma centered">
          <div className="w-100 h-auto of-hidden c-p-1 ns-bg-1 ns-b-s c-br-1 h-pr-fl-ma">
            <div className="w-100 h-auto h-pr-fl-ma ns-bg-2 c-br-1 m-b-5px c-p-1">
            <DataBox text="INGRESE SUS DATOS:" textStyle="ns-bg-1 small "></DataBox></div>

               <div className="w-100 h-auto h-pr-fl-ma c-br-1 ns-bg-2 c-p-1">
              <DataBox childrenStyle=" ns-bg-1" className="small" >
                <Inp
                    className="w-100"
                    iconLeft="fas fa-envelope"
                    theme="ns-bg-1 ns-c-1"
                    type="text"
                    size="normal"
                    placeholder="Ingrese su email o nickname:"
                  />
                </DataBox>
                </div>

              <div className="w-100 h-pr-fl-ma c-p-1">
                <Btn
                  theme="alert"
                  iconLeft="fas fa-sync"
                  className="w-100 c-p-1"
                  size="normal"
                  text="Recuperar"
                ></Btn>
              </div>
            </div>
          </div>
        </form>
      </Fragment>
    );
}

export default PasswordRecovery
