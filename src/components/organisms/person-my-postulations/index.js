import React, { Fragment } from "react";
import "./styles.css";
import { useLocation } from 'react-router-dom'
import { Text } from "../../atoms";
import {
  Publication,
  Pagination,
} from "../../molecules/index.js";

const PersonMyPostulations = () => {
    const location = useLocation()
    const path = location.pathname

    return (
      <Fragment>
        <div className="w-100 h-100 h-pr-fl-ma ns-bg-1 c-p-1">
        <div className="w-100 h-10 h-pr-fl-ma">
          <Text className="w-100 h-auto  f-w-bo" text="MIS POSTULACIONES" theme="success" size="normal" align="t-a-c"></Text>
          </div>
          <div className="w-100 h-80 h-pr-fl-ma small of-y-auto of-x-none ns-bg-2 c-br-1 c-p-1">
          <Publication Accessible className={path === "/dashboard/person/mypostulations" ? "" : ""}></Publication>
            <Publication Postulated className={path === "/dashboard/person/mypostulations" ? "" : ""}></Publication>
            <Publication Caution className={path === "/dashboard/person/mypostulations" ? "" : ""}></Publication>
            <Publication Urgent className={path === "/dashboard/person/mypostulations" ? "" : ""}></Publication>
            <Publication Info className={path === "/dashboard/person/mypostulations" ? "" : ""}></Publication>
            <Publication Ads className={path === "/dashboard/person/mypostulations" ? "" : ""}></Publication>
            <Publication Visitor className={path === "/dashboard/person/mypostulations" ? "" : ""}></Publication>
          </div>
          <div className="w-100 h-10 h-pr-fl-ma">
            <Pagination className="h-auto h-pr-fl-ma"></Pagination>
          </div>
        </div>
      </Fragment>
    );
}

export default PersonMyPostulations
