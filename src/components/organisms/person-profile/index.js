import React from "react";
import { Img, Btn, ProgBar } from '../../atoms';

import { ProfileCoverPage, ProfilePicture, } from "../../molecules";

import "./styles.css";

const PersonProfile = () => {
  return (
    <>
      <div className="w-100 h-100 h-pr-fl-ma c-br-2 c-p-2 of-x-hidden of-y-auto">

        <div className="w-100 h-auto h-pr-fl-ma c-br-1 d-i flexbox">
          <ProfileCoverPage className="w-100 h-100 h-pr-fl-ma"></ProfileCoverPage>
          <div className="w-100 h-auto h-pr-fl-ma c-p-1 d-f a-c-f-e">

            <div className="w-70 h-100 h-pr-fl-ma ns-bg-1 ns-b-s c-br-3 d-i-f">
              <div className="w-auto h-100 h-pr-fl-ma c-p-1">
                <ProfilePicture className="w-and-h-125px h-pr-fl-ma" theme="ns-bg-2"></ProfilePicture>
              </div>
              <div className="w-80 h-100 h-pr-fl-ma of-h b-s-b-b">
                <div className="w-100 h-auto h-pr-fl-ma c-f-1 medium m-small ns-c-1 f-w-bo c-p-1 c-br-1">NOMBRE Y APELLIDO</div>
                <div className="w-100 h-auto h-pr-fl-ma c-f-1 normal f-w-bo m-nano ns-c-1 c-p-1 c-br-1">FRASE PERSONAL (ESTADO)</div>
                <div className="w-auto h-auto h-pr-fl-ma d-f f-w-w small b-s-b-b">
                  <div className="c-p-1"><Btn icon="fab fa-facebook" className="normal c-br-1" theme="bg-facebook c-white"></Btn></div>
                  <div className="c-p-1"><Btn icon="fab fa-twitter" className="normal c-br-1" theme="bg-twitter c-white"></Btn></div>
                  <div className="c-p-1"><Btn icon="fab fa-youtube" className="normal c-br-1" theme="bg-youtube c-white"></Btn></div>
                  <div className="c-p-1"><Btn icon="fab fa-instagram" className="normal c-br-1" theme="bg-instagram c-white"></Btn></div>
                  <div className="c-p-1"><Btn icon="fab fa-linkedin" className="normal c-br-1" theme="bg-linkedin c-white"></Btn></div>
                  <div className="c-p-1"><Btn icon="fab fa-patreon" className="normal c-br-1" theme="bg-patreon c-white"></Btn></div>
                </div>
              </div>

            </div>
            <div className="w-30 h-auto h-pr-fl-ma">
              <div className="w-auto h-auto h-pr-fr-ma ns-bg-1 ns-b-s f-b-c flexbox f-d-c c-br-2 c-p-1">
                <div className="w-80px h-80px h-pr-fl-ma ns-bg-2 c-br-2 m-b-5px medium">
                   <i className="fas fa-qrcode big centered"></i>
                </div>
                <div className="w-80px h-auto h-pr-fl-ma  c-p-1 ns-bg-2 c-br-1 ">
                  <Btn  icon="fas fa-share small" className="w-100 h-pr-fr-ma r-h-c" size="small" theme="ns-bg-1"></Btn></div>
              </div>

            </div>

          </div>
        </div>



        {/* ?  | CONTENEDOR SECTOR COMPONENTES */}
        <div className="w-100 h-auto small h-pr-fl-ma c-p-1">
          {/* TODO |                                  |LEFT BAR| */}
          <div className="w-70 m-w-100 h-auto h-pr-fl-ma   c-p-1">

            {/* TODO |                                  |HABILITACIONES| */}
            <div className="w-100 h-auto   d-i-f h-pr-fl-ma">
              {/* TODO |                                  | CARNÉ DE SALUD | */}
              <div className="w-33 h-auto d-i-f c-p-1 h-pr-fl-ma c-p-1 ">

                  <div className="w-100 h-auto h-pr-fl-ma ns-bg-2 c-br-1">
                    <div className="w-100 h-auto h-pr-fl-ma d-i-f c-p-1">
                      <div className="w-100 h-auto small d-i-f  ns-bg-1 c-f-1 f-w-bo c-br-1 c-p-1">
                        <div className="w-50 h-auto h-pr-fl-ma l-h-1em c-p-1 f-w-bo c-f-1 ns-c-1 c-br-1 small">CARNÉ DE SALUD</div>
                        <div className="w-50 h-auto h-pr-fr-ma t-a-c c-p-1 f-w-bo c-f-1 success c-br-1 small">VIGENTE</div></div>

                    </div>
                  </div>
                </div>
                {/* TODO |                                  | CARNÉ DE CONDUCIR | */}
                  <div className="w-33 h-auto d-i-f c-p-1 h-pr-fl-ma c-p-1">

                  <div className="w-100 h-auto h-pr-fl-ma ns-bg-2 c-br-1">
                    <div className="w-100 h-auto h-pr-fl-ma d-i-f c-p-1">
                      <div className="w-100 h-auto small d-i-f  ns-bg-1 c-f-1 f-w-bo c-br-1 c-p-1">
                        <div className="w-50 h-auto h-pr-fl-ma l-h-1em c-p-1 f-w-bo ns-c-1 c-f-1 c-br-1 small">CARNÉ DE CONDUCIR</div>
                        <div className="w-50 h-auto h-pr-fr-ma t-a-c c-p-1 f-w-bo c-f-1 success c-br-1 small">VIGENTE</div></div>

                    </div>
                  </div>
                </div>
                {/* TODO |                                  | CARNÉ DE MANIPULACION DE ALIMENTOS | */}
                <div className="w-33 h-auto d-i-f c-p-1 h-pr-fl-ma c-p-1">

                  <div className="w-100 h-auto h-pr-fl-ma ns-bg-2 c-br-1">
                    <div className="w-100 h-auto h-pr-fl-ma d-i-f c-p-1">
                      <div className="w-100 h-auto small d-i-f ns-bg-1 c-f-1 f-w-bo c-br-1 c-p-1">
                        <div className="w-50 h-auto h-pr-fl-ma l-h-1em c-p-1 f-w-bo ns-c-1 c-f-1 c-br-1 small">MAN. DE ALIMENTOS</div>
                        <div className="w-50 h-auto h-pr-fr-ma t-a-c c-p-1 f-w-bo c-f-1 caution c-br-1 small">EN TRAMITE</div></div>

                    </div>
                  </div>
                </div>


            </div>

            {/* TODO |                                  |DESCRIPCION| */}
            <div className="w-100 h-auto h-pr-fl-ma ">
              <div className="w-70 h-auto h-pr-fl-ma c-p-1">
                <div className="w-100 h-auto h-pr-fl-ma ns-bg-1 ns-b-s c-br-1 c-p-1">
                  <div className="w-20 h-auto h-pr-fl-ma b-s-b-b c-p-1">
                    <div className="w-auto r-h-c ar a-r-1-1 h-pr-fl-ma b-s-b-b m-b-5px">
                      <div className="w-50 a-r-content h-pr-fl-ma b-s-b-b of-h circle ns-bg-2 c-p-1">
                        <div className="w-100 h-100 normal h-pr-fl-ma circle ns-bg-1 "><i className="fas fa-user-circle big centered ns-c-1"></i></div>
                      </div>
                    </div>
                    <div className="w-100 h-auto h-pr-fl-ma c-p-1 ns-bg-2 c-br-1">
                      <Btn className="w-100" size="small" theme="ns-bg-1 " text="Seguir"></Btn>
                    </div>
                  </div>
                  <div className="w-80 h-auto h-pr-fl-ma c-p-1">
                    <div className="w-100 h-auto h-pr-fl-ma c-f-1 small max-lines-8 of-h ">DESCRIPCION Enim deserunt deserunt ipsum qui ullamco elit amet mollit pariatur sint irure qui. Sunt commodo consectetur enim est nulla dolor tempor nisi amet ipsum. Adipisicing voluptate ex duis exercitation. Proident sint id occaecat amet.
                      Fugiat excepteur consequat exercitation sit fugiat adipisiciaute consectetur. Aliquip ea eiusmod incididunt eu. Quis eiusmod voluptate nisi veniam ut anim nostrud officia voluptate sit adipisicing ut ex occaecat. Occaecat elit minim ullamco dolore aliqua. Ea Lorem ipsum laboris ex deserunt.</div>
                  </div>
                  <div className="w-100 h-auto h-pr-fl-ma c-p-1 c-f-1 normal f-w-bo line-t b-s-1px default">
                    <div className="w-auto h-pr-fr-ma h-auto c-p-1 small c-f-1 f-w-bo">NOMBRE APELLIDO - 2021</div>
                  </div>


                </div>

              </div>
              {/* TODO |                                  |CURRICULUM VITAE| */}
              <div className="w-30 h-auto h-pr-fl-ma c-p-1">
                <div className="w-100 h-auto h-pr-fl-ma c-br-1 ns-bg-1 ns-b-s c-p-1">
                  <div className="w-100 r-h-c h-auto h-pr-fl-ma m-b-3px">
                    <div className="w-auto a-r a-r-3-4 ns-bg-2 c-br-1">
                      <div className="w-100 a-r-content h-100 h-pr-fl-ma  of-h c-p-1">
                        <div className="w-100 h-100 h-pr-fl-ma ns-bg-1 c-br-1"><i className="fas fa-file big centered ns-c-1"></i></div>
                      </div>
                    </div>
                  </div>
                  <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                    <div className="w-100 h-auto r-h-c h-pr-fl-ma">
                      <Btn text="Solicitar acceso" theme="info" className="w-100" size="small"></Btn></div>
                  </div>
                </div>
              </div>
            </div>


            {/* TODO |                                  |EXPERIENCIAS LABORALES| */}
            <div className="w-100 h-auto h-pr-fl-ma c-p-1">

              {/* TODO |                                  |CONTENIDO EXPERIENCIA LABORAL| */}
              <div className="w-100 h-auto  h-pr-fl-ma ns-bg-1 ns-b-s c-br-1 c-p-1">
                <div className="w-100 h-auto h-pr-fl-ma ns-bg-2 c-f-1 small f-w-bo m-b-5px c-p-1 c-br-1">
                  <div className="w-100 h-auto small ns-bg-1 ns-c-1 c-br-1 c-p-1">EXPERIENCIA LABORAL:</div></div>
                {/* TODO |                                  |FECHA Y LUGAR MAS RECIENTE| */}
                <div className="w-100 h-auto d-i-f ns-bg-2 c-br-1 h-pr-fl-ma c-p-1 m-b-5px">
                  <div className="w-20 h-auto t-a-c c-f-1 small h-pr-fl-ma flexbox a-i-c f-d-c c-p-1">
                    <div className="w-auto h-auto h-pr-fl-ma c-p-1 info c-br-1 f-w-bo">
                      <div className="w-100 h-auto h-pr-fl-ma small ">2021</div>
                      <div className="w-100 h-auto h-pr-fl-ma nano">(RECIENTE)</div>
                    </div>
                  </div>
                  <div className="w-80 h-auto d-i-f h-pr-fl-ma ns-bg-1 c-br-1 c-p-1">
                    <div className="w-auto h-auto h-pr-fl-ma c-br-1 ns-c-1 c-p-1 c-f-1 small f-w-bo l-h-1-1em">
                      EMPRESA
                    </div>
                    <div className="w-auto h-auto h-pr-fl-ma ns-bg-2 ns-c-1 c-br-1 c-p-1 c-f-1 small f-w-bo l-h-1-1em ">
                      AÑO DE INGRESO Y EGRESO | DESDE | HASTA
                    </div>
                  </div>
                </div>

                {/* TODO |                                  |2020| */}
                <div className="w-100 h-auto d-i-f ns-bg-2 c-br-1 h-pr-fl-ma c-p-1">
                  <div className="w-20 h-auto t-a-c c-f-1 small h-pr-fl-ma c-p-1">
                    <div className="w-auto h-auto h-pr-fl-ma r-h-c c-p-1 ns-bg-1 c-br-1 ns-c-1 f-w-bo">2020</div></div>
                  <div className="w-80 h-auto d-i-f h-pr-fl-ma ns-bg-1 c-br-1 c-p-1">
                    <div className="w-auto h-auto h-pr-fl-ma c-br-1 ns-c-1 c-p-1 c-f-1 small f-w-bo l-h-1-1em">
                      EMPRESA
                    </div>
                    <div className="w-auto h-auto h-pr-fl-ma ns-bg-2 ns-c-1 c-br-1 c-p-1 c-f-1 small f-w-bo l-h-1-1em ">
                      AÑO DE INGRESO Y EGRESO | DESDE | HASTA
                    </div>
                  </div>
                </div>
              </div>
            </div>


            {/* TODO |                                  |ESTUDIOS CURSADOS| */}
            <div className="w-100 h-auto h-pr-fl-ma c-p-1">
              {/* TODO |                                  |CONTENIDO ESTUDIOS| */}
              <div className="w-100 h-auto  h-pr-fl-ma ns-bg-1 ns-b-s c-br-1 c-p-1">
                <div className="w-100 h-auto h-pr-fl-ma ns-bg-2 c-f-1 small f-w-bo m-b-5px c-p-1 c-br-1">
                  <div className="w-100 h-auto small ns-bg-1 ns-c-1 c-br-1 c-p-1">ESTUDIOS:</div></div>
                {/* TODO |                                  |FECHA Y LUGAR MAS RECIENTE| */}
                <div className="w-100 h-auto d-i-f ns-bg-2 c-br-1 h-pr-fl-ma c-p-1 m-b-5px">
                  <div className="w-20 h-auto t-a-c c-f-1 small h-pr-fl-ma flexbox a-i-c f-d-c c-p-1">
                    <div className="w-auto h-auto h-pr-fl-ma c-p-1 info c-br-1 f-w-bo">
                      <div className="w-100 h-auto h-pr-fl-ma small ">2021</div>
                      <div className="w-100 h-auto h-pr-fl-ma nano">(RECIENTE)</div>
                    </div>
                  </div>
                  <div className="w-80 h-auto d-i-f h-pr-fl-ma ns-bg-1 c-br-1 c-p-1">
                    <div className="w-auto h-auto h-pr-fl-ma c-br-1 ns-c-1 c-p-1 c-f-1 small f-w-bo l-h-1-1em">
                      LUGAR DE ESTUDIO | CURSANDO
                    </div>
                    <div className="w-auto h-auto h-pr-fl-ma ns-bg-2 ns-c-1 c-br-1 c-p-1 c-f-1 small f-w-bo l-h-1-1em ">
                      ULTIMO AÑO CURSADO | DESDE | HASTA
                    </div>
                  </div>
                </div>

                {/* TODO |                                  |2020| */}
                <div className="w-100 h-auto d-i-f ns-bg-2 c-br-1 h-pr-fl-ma c-p-1">
                  <div className="w-20 h-auto t-a-c c-f-1 small h-pr-fl-ma c-p-1">
                    <div className="w-auto h-auto h-pr-fl-ma r-h-c c-p-1 ns-bg-1 ns-c-1 c-br-1 f-w-bo">2020</div></div>
                  <div className="w-80 h-auto d-i-f h-pr-fl-ma ns-bg-1 c-br-1 c-p-1">
                    <div className="w-auto h-auto h-pr-fl-ma c-br-1 c-p-1 ns-c-1 c-f-1 small f-w-bo l-h-1-1em">
                      LUGAR DE ESTUDIO CURSADO
                    </div>
                    <div className="w-auto h-auto h-pr-fl-ma ns-bg-2 ns-c-1 c-br-1 c-p-1 c-f-1 small f-w-bo l-h-1-1em ">
                      ULTIMO AÑO CURSADO | DESDE | HASTA
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* TODO |                                  |RIGHT BAR| */}
          <div className="w-30 m-w-100 h-auto h-pr-fl-ma c-p-1">
            <div className="w-100 h-auto h-pr-fl-ma ns-bg-1 ns-b-s c-br-1 m-b-5px">
            <ProfileCoverPage className="w-100 h-20vh c-p-1"></ProfileCoverPage>
              <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                <div className="w-40 r-h-c h-auto h-pr-fl-ma">
                  <div className="w-auto c-br-1 h-auto a-r a-r-1-1 m-t-10px">
                    <div className="a-r-content ns-bg-2 c-br-3 c-p-1">
                      <div className="w-100 h-100 h-pr-fl-ma ns-bg-1 h-e c-p c-br-2">
                        <i className="fas fa-user-circle big centered ns-c-1"></i></div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="w-100 h-auto h-pr-fl-ma c-p-1 small">
                {/* TODO |                                  | NOMBRE APELLIDO | */}
                <div className="w-100 h-auto small ns-bg-2 c-br-1 c-p-1 c-f-1 m-b-5px small h-pr-fl-ma b-s-b-b f-w-bo">
                  <div className="w-100 h-auto h-pr-fl-ma ns-c-1 l-h-1-1em ns-bg-1  c-br-1 c-p-1">NOMBRE & APELLIDO</div>
                  <div className="w-100 h-auto h-pr-fl-ma ns-c-1 l-h-1-1em c-p-1">NICKOLAS MACHADO</div></div>
                {/* TODO |                                  | FECHA DE NACIMIENTO| */}
                <div className="w-100 h-auto small ns-bg-2 c-br-1 c-p-1 c-f-1 m-b-5px small h-pr-fl-ma b-s-b-b f-w-bo">
                  <div className="w-100 h-auto h-pr-fl-ma ns-c-1 l-h-1-1em c-br-1 ns-bg-1 c-p-1">EDAD</div>
                  <div className="w-100 h-auto h-pr-fl-ma ns-c-1 l-h-1-1em c-p-1">282 AÑOS</div></div>
                {/* TODO |                                  | EMAIL | */}
                <div className="w-100 h-auto small ns-bg-2 c-br-1 c-p-1 c-f-1 m-b-5px small h-pr-fl-ma b-s-b-b f-w-bo">
                  <div className="w-100 h-auto h-pr-fl-ma ns-c-1 l-h-1-1em c-br-1 ns-bg-1 c-p-1">EMAIL</div>
                  <div className="w-100 h-auto h-pr-fl-ma ns-c-1 l-h-1-1em c-p-1">nodesystemsuy@gmail.com</div>
                  </div>
              </div>
            </div>




            {/* TODO |                                  |APTITUDES| */}
            <div className="w-100 h-auto h-pr-fl-ma ns-bg-1 ns-b-s c-br-1 c-p-1 m-b-5px">
              <div className="w-100 h-auto d-i-f ns-bg-2 c-br-1 c-p-1 c-f-1 m-b-5px f-w-bo h-pr-fl-ma">
                <div className="w-100 h-auto h-pr-fl-ma ns-bg-1 c-br-1">
                  <div className="w-30 h-auto h-pr-fl-ma b-s-b-b c-p-1">
                    <div className="w-auto r-h-c ar a-r-1-1 h-pr-fl-ma b-s-b-b m-b-5px">
                      <div className="w-50 a-r-content h-pr-fl-ma b-s-b-b of-h circle ns-bg-2 c-p-1">
                        <div className="w-100 h-100 normal h-pr-fl-ma circle ns-bg-1"><i className="fas fa-user-circle big centered ns-c-1"></i></div>
                      </div>
                    </div>

                  </div>
                  <div className="w-70 h-auto h-pr-fl-ma b-s-b-b small c-p-1">

                    <div className="w-100 h-auto h-pr-fl-ma c-p-1 small l-h-1em">PROFESION / RUBRO ACTUAL</div>
                    <div className="w-100 h-auto c-br-1 h-pr-fl-ma c-p c-p-1">
                      <ProgBar className="w-100 h-15px" ></ProgBar>

                    </div>
                  </div>
                </div>
              </div>

              <div className="w-100 h-auto h-pr-fl-ma c-f-1 small f-w-bo c-p-1 ns-bg-2 c-br-1">
                <div className="w-100 h-auto small ns-bg-1 ns-c-1 c-br-1 c-p-1">CONOCIMIENTO:</div></div>
              <div className="w-100 h-auto c-br-1 h-pr-fl-ma c-p c-p-1">
                <ProgBar className="w-100 h-15px" ></ProgBar>
              </div>
              <div className="w-100 h-auto h-pr-fl-ma c-f-1 small f-w-bo c-p-1 ns-bg-2 c-br-1">
                <div className="w-100 h-auto small ns-bg-1 ns-c-1 c-br-1 c-p-1">EXPERIENCIA:</div></div>
              <div className="w-100 h-auto c-br-1 h-pr-fl-ma c-p c-p-1">
                <ProgBar className="w-100 h-15px" ></ProgBar>
              </div>
              <div className="w-100 h-auto h-pr-fl-ma c-f-1 small f-w-bo c-p-1 ns-bg-2 c-br-1">
                <div className="w-100 h-auto small ns-bg-1 ns-c-1 c-br-1 c-p-1">HABILIDAD:</div></div>
              <div className="w-100 h-auto c-br-1 h-pr-fl-ma c-p c-p-1">
                <ProgBar className="w-100 h-15px" ></ProgBar>
              </div>

            </div>
            {/* TODO |                                  | DISPONIBILIDADES | */}
            <div className="w-100 h-auto h-pr-fl-ma ns-bg-1 ns-b-s c-br-1 c-p-1 m-b-5px c-p-1 c-br-1">
            <div className="w-100 h-auto h-pr-fl-ma c-f-1 small f-w-bo c-p-1 ns-bg-2 c-br-1 m-b-5px">
                <div className="w-100 h-auto small ns-bg-1 t-a-c ns-c-1 c-br-1 c-p-1">DISPONIBILIDADES</div></div>

              <div className="w-100 h-auto h-pr-fl-ma ns-bg-2 c-br-1 c-p-1">
                {/* TODO |                                  |DISPONIBILIDAD HORARIA| */}
                <div className="w-100 h-auto d-i-f ns-bg-1 c-br-1 c-p-1 h-pr-fl-ma c-p-1 m-b-5px">
                  <div className="w-20 h-auto h-pr-fl-ma">
                    <div className="w-auto r-h-c ar a-r-1-1 h-pr-fl-ma b-s-b-b m-b-5px">
                      <div className="w-50 a-r-content h-pr-fl-ma b-s-b-b of-h circle ns-bg-2 c-p-1">
                        <div className="w-100 h-100 normal h-pr-fl-ma circle ns-bg-1"><i className="fas fa-clock normal centered default"></i></div>
                      </div>
                    </div>
                  </div>
                  <div className="w-80 h-auto h-pr-fl-ma">
                    <div className="w-100 h-auto h-pr-fl-ma d-i-f c-p-1">
                      <div className="w-100 h-auto small d-i-f ns-bg-2 c-f-1 f-w-bo c-br-1 c-p-1">
                        <div className="w-50 h-auto h-pr-fl-ma l-h-1em c-p-1 f-w-bo c-f-1 c-br-1 small">HORARIA</div>
                        <div className="w-50 h-auto h-pr-fr-ma c-p-1 f-w-bo c-f-1 success c-br-1 small">COMPLETA</div></div>

                    </div>
                  </div>
                </div>
                {/* TODO |                                  |DISPONIBILIDAD VIAJAR| */}
                <div className="w-100 h-auto d-i-f  ns-bg-1 c-br-1 c-p-1 h-pr-fl-ma c-p-1 m-b-5px">
                  <div className="w-20 h-auto h-pr-fl-ma">
                    <div className="w-auto r-h-c ar a-r-1-1 h-pr-fl-ma b-s-b-b m-b-5px">
                      <div className="w-50 a-r-content h-pr-fl-ma b-s-b-b of-h circle  ns-bg-2 c-p-1">
                        <div className="w-100 t-a-c h-100 normal h-pr-fl-ma circle  ns-bg-1"><i className="fas fa-plane-departure normal centered default"></i></div>
                      </div>
                    </div>
                  </div>
                  <div className="w-80 h-auto h-pr-fl-ma">
                    <div className="w-100 h-auto h-pr-fl-ma d-i-f c-p-1">
                      <div className="w-100 h-auto  small d-i-f   ns-bg-2 c-f-1 f-w-bo c-br-1 c-p-1">
                        <div className="w-50 h-auto h-pr-fl-ma l-h-1em c-p-1 f-w-bo c-f-1 c-br-1 small">VIAJAR</div>
                        <div className="w-50 t-a-c h-auto h-pr-fr-ma c-p-1 f-w-bo c-f-1 caution c-br-1 small">REGIONAL</div></div>

                    </div>
                  </div>
                </div>
                {/* TODO |                                  |DISPONIBILIDAD TRABAJO REMOTO| */}
                <div className="w-100 h-auto d-i-f  ns-bg-1 c-br-1 c-p-1 h-pr-fl-ma c-p-1 ">
                  <div className="w-20 h-auto h-pr-fl-ma">
                    <div className="w-auto r-h-c ar a-r-1-1 h-pr-fl-ma b-s-b-b m-b-5px">
                      <div className="w-50 a-r-content h-pr-fl-ma b-s-b-b of-h circle  ns-bg-2 c-p-1">
                        <div className="w-100 h-100 normal h-pr-fl-ma circle  ns-bg-1"><i className="fas fa-home normal centered default"></i></div>
                      </div>
                    </div>
                  </div>
                  <div className="w-80 h-auto h-pr-fl-ma">
                    <div className="w-100 h-auto h-pr-fl-ma d-i-f c-p-1">
                      <div className="w-100 h-auto small d-i-f  ns-bg-2 c-f-1 f-w-bo c-br-1 c-p-1">
                        <div className="w-50 h-auto h-pr-fl-ma l-h-1em c-p-1 f-w-bo c-f-1 c-br-1 small">TRABAJO REMOTO</div>
                        <div className="w-50 t-a-c h-auto h-pr-fr-ma c-p-1 f-w-bo c-f-1 success c-br-1 small">COMPLETA</div></div>

                    </div>
                  </div>
                </div>
              </div>
            </div>


          </div>


          {/* ! | CURRICULUM VITAE | DESCRIPCION | APTITUDES */}
          <div className="w-100 h-auto small h-pr-fl-ma">

            <div className="w-30 h-auto h-pr-fl-ma c-p-1">

            </div>



          </div>

        </div>
      </div>
    </>
  );
}

export default PersonProfile
