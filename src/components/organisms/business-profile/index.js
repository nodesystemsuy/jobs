import React, { Fragment } from "react";
import "./styles.css";

import { Btn } from "../../atoms/index.js";
import { ProfileCoverPage, ProfilePicture, DataBox, } from "../../molecules/index.js";

const  BusinessProfile = () => {
    return (
      <Fragment>
        <div className="w-100 h-100 h-pr-fl-ma ns-bg-1 c-br-2 small c-p-2 of-x-hidden of-y-auto">
          <div className="w-100 h-auto flexbox h-pr-fl-ma d-i-f c-br-1 of-hidden m-b-10px">
            <ProfileCoverPage className="h-25vh "></ProfileCoverPage>
            <div className="w-20 h-auto flex-auto h-pr-fl-ma c-p-1 b-s-b-b ">
              <div className="w-80 m-w-100 h-auto h-pr-fl-ma r-h-c">
                <ProfilePicture className="w-and-h-125px ns-bg-2 m-w-80px m-h-80px "></ProfilePicture>
              </div>
            </div>
            <div className="w-60 h-auto flex-auto d-i-f h-pr-fl-ma c-p-2 c-br-2">
              <div className="w-80 h-auto  h-pr-fl-ma">
                <div className="w-100 h-auto c-f-1 d-i-f c-p-1 medium f-w-bo">
                  <span className="c-bg-8 c-br-1 p-l-5px p-r-5px">
                    NOMBRE EMPRESA
                  </span>
                </div>
                <div className="w-100 h-auto c-f-1 c-p-1 normal c-br-1">
                  <span className="c-bg-8 c-br-1 p-l-5px p-r-5px">
                    SLOGAN DE EMPRESA
                  </span>
                </div>
              </div>
              <div className="w-20  h-auto flex-auto h-pr-fl-ma">
                <div className="w-auto m-d-n r-h-c h-auto h-auto h-pr-fl-ma c-bg-8 c-p-1 c-br-1">
                  <div className="w-100 h-auto c-f-1 nano h-auto">DESDE:</div>
                  <div className="w-100 h-auto c-f-1 f-w-bo h-auto medium h-pr-fl-ma">
                    1830
                  </div>
                </div>
              </div>
            </div>
            <div className="w-20 h-auto flex-auto h-pr-fl-ma c-p-2 c-br-2">
              <div className="w-100 h-auto h-pr-fl-ma m-b-2px h-e c-p small c-br-1 c-bg-8 d-i-f t-of-e p-2px m-b-2px">
                <i className="t-a-c w-20 m-w-100 fas fa-globe"></i>
                <span className="w-80 c-f-1 f-w-bo small m-d-n">Sito Web</span>
              </div>
              <div className="w-100 h-auto h-pr-fl-ma m-b-2px h-e c-p small c-br-1 c-bg-8 d-i-f t-of-e p-2px m-b-2px">
                <i className="t-a-c w-20 m-w-100 fab fa-linkedin"></i>
                <span className="w-80 c-f-1 f-w-bo small m-d-n">Linkedin</span>
              </div>
              <div className="w-100 h-auto h-pr-fl-ma m-b-2px h-e c-p small c-br-1 c-bg-8 d-i-f t-of-e p-2px m-b-2px">
                <i className="t-a-c w-20 m-w-100 fab fa-twitter"></i>
                <span className="w-80 c-f-1 f-w-bo small m-d-n">Twitter</span>
              </div>
              <div className="w-100 h-auto h-pr-fl-ma m-b-2px h-e c-p small c-br-1 c-bg-8 d-i-f t-of-e p-2px m-b-2px">
                <i className="t-a-c w-20 m-w-100 fab fa-instagram"></i>
                <span className="w-80 c-f-1 f-w-bo small m-d-n">Instagram</span>
              </div>
              <div className="w-100 h-auto h-pr-fl-ma m-b-2px h-e c-p small c-br-1 c-bg-8 d-i-f t-of-e p-2px m-b-2px">
                <i className="t-a-c w-20 m-w-100 fab fa-facebook"></i>
                <span className="w-80 c-f-1 f-w-bo small m-d-n">Facebook</span>
              </div>
            </div>
          </div>

          <div className="w-100 h-auto h-pr-fl-ma d-i-f c-p-1 ns-bg-2 c-br-1 m-b-5px c-br-1 ">
            <div className="w-100 of-hidden h-pr-fl-ma ">
              <marquee className="w-100 h-auto  h-pr-fl-ma">
                <div className="w-100 h-auto d-i-f h-pr-fl-ma">
              <DataBox className="d-i-f ns-bg-1 m-l-3px small m-r-3px" text="PROPUESTAS OFRECIDAS" content="3123" contentStyle="c-green-node"></DataBox>
              <DataBox className="d-i-f ns-bg-1 m-l-3px small m-r-3px" text="PROPUESTAS OFRECIDAS" content="3123" contentStyle="c-green-node"></DataBox>
              <DataBox className="d-i-f ns-bg-1 m-l-3px small m-r-3px" text="PROPUESTAS OFRECIDAS" content="3123" contentStyle="c-green-node"></DataBox>
              <DataBox className="d-i-f ns-bg-1 m-l-3px small m-r-3px" text="PROPUESTAS OFRECIDAS" content="3123" contentStyle="c-green-node"></DataBox>
              </div>
              </marquee>
            </div>
            <div className="w-20 of-hidden h-pr-fl-ma">
              <Btn
                className="w-100 h-pr-fr-ma"
                theme="info"
                icon="fas fa-bell"
                size="small"
              ><span className="m-d-n small m-l-5px">NOTIFICACIONES</span></Btn>
            </div>
          </div>

          <div className="w-100 h-auto h-pr-fl-ma small ns-bg-2 c-br-1 m-b-5px ">
            {/* TODO |                                  |UBICACION + HORA DE APERTURA + SUCURSAL | */}

          <div className="w-100 h-auto h-pr-fl-ma small ">
              <div className="w-50 m-w-100 h-auto h-pr-fl-ma c-p-1">
              <DataBox title="UBICACIÓN:" titleStyle="m-b-5px" textStyle="m-b-5px" >
              <div className="w-100 h-30vh ns-bg-1 ns-b-s c-br-1 h-pr-fl-ma m-b-5px c-p-1">
                  <div className="w-100 h-100 h-pr-fl-ma ns-bg-2 c-br-1 c-p h-e">
                    <span className="centered medium">
                      <i className="fas big fa-map-marker-alt"></i>
                    </span>
                  </div>
                </div>
              </DataBox>



              </div>
              <div className="w-50 m-w-100 h-auto h-pr-fl-ma ">

                <div className="w-70 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="DIRECCIÓN:" titleStyle="m-b-5px" text="CALLE| PISO | APTO | N° PUERTA" textStyle="m-b-5px"></DataBox>
                <DataBox title="TELÉFONO:" titleStyle="m-b-5px" text="000-000-000-000 | 000 0000 0000" textStyle="m-b-5px"></DataBox>
                <DataBox title="UBICACIÓN:" titleStyle="m-b-5px" text="LUNES A VIERNES | DE 00:00 A 00:00" textStyle="m-b-5px">
                <DataBox text="SABADOS | DE 00:00 A 00:00 " textStyle="m-b-5px" ></DataBox>
                <DataBox text="DOMINGOS | CERRADO" content="HOY NO FIO MAÑANA SI" contentStyle="" textStyle="m-b-5px" ></DataBox>
                </DataBox>


                </div>
                <div className="w-30 h-auto h-pr-fl-ma c-p-1">
                <DataBox title="AHORA:" titleStyle="m-b-5px" className="m-b-5px">
                  <Btn
                    iconLeft="fas fa-door-open"
                    className="w-100"
                    theme="success"
                    text="ABIERTO"
                    size="normal"
                  ></Btn></DataBox>


                  <div className="w-100 h-auto ns-bg-1 ns-c-1 f-w-bo m-b-5px h-pr-fl-ma c-f-1 p-l-5px b-s-b-b small c-br-1">
                    SUCURSAL:
                  </div>
                  <div className="w-100 h-100px h-pr-fl-ma ns-bg-1 ns-b-s ns-c-1 c-br-1 of-hidden">
                    <div className="w-100 h-70px h-pr-fl-ma">
                      <span className="centered big c-f-1 c-f-bo">
                        <i className="fas fa-store-alt"></i>
                      </span>
                    </div>
                    <div className="w-100 h-30px ns-bg-1 c-f-1 f-w-bo h-pr-fl-ma line-t b-s-1px default t-a-c">
                      <span className="centered">CENTRAL</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* TODO |                                  | DESCRIPCIÓN EMPRESA CONTENEDOR | */}

            <div className="w-100 h-auto h-pr-fl-ma small ">
                  {/* TODO |                                  |DESCRIPCION EMPRESA| */}
            <div className="w-50 h-auto h-pr-fl-ma ">
              <div className="w-100 h-auto h-pr-fl-ma c-p-1">
                <div className="w-100 h-auto h-pr-fl-ma ns-bg-1 ns-b-s c-br-1 c-p-1">
                  <div className="w-20 h-auto h-pr-fl-ma b-s-b-b c-p-1">
                    <div className="w-auto r-h-c ar a-r-1-1 h-pr-fl-ma b-s-b-b m-b-5px">
                      <div className="w-50 a-r-content h-pr-fl-ma b-s-b-b of-h circle ns-bg-2 c-p-1">
                        <div className="w-100 h-100 normal h-pr-fl-ma circle ns-bg-1"><i className="fas fa-user-circle big centered ns-c-1"></i></div>
                      </div>
                    </div>
                    <div className="w-100 h-auto h-pr-fl-ma c-p-1 ns-bg-2 c-br-1">
                      <Btn className="w-100" size="small" theme="ns-bg-1" text="Seguir"></Btn>
                    </div>
                  </div>
                  <div className="w-80 h-auto h-pr-fl-ma c-p-1">
                    <div className="w-100 h-auto h-pr-fl-ma c-f-1 small max-lines-8 of-h ">DESCRIPCION Enim deserunt deserunt ipsum qui ullamco elit amet mollit pariatur sint irure qui. Sunt commodo consectetur enim est nulla dolor tempor nisi amet ipsum. Adipisicing voluptate ex duis exercitation. Proident sint id occaecat amet.
                      Fugiat excepteur consequat exercitation sit fugiat adipisiciaute consectetur. Aliquip ea eiusmod incididunt eu. Quis eiusmod voluptate nisi veniam ut anim nostrud officia voluptate sit adipisicing ut ex occaecat. Occaecat elit minim ullamco dolore aliqua. Ea Lorem ipsum laboris ex deserunt.</div>
                  </div>
                  <div className="w-100 h-auto h-pr-fl-ma c-p-1 c-f-1 normal f-w-bo line-t b-s-1px default">
                    <div className="w-auto h-pr-fr-ma h-auto c-p-1 small c-f-1 f-w-bo">EMPRESA- 2021</div>
                  </div>
                  </div>
                  </div>
                </div>


              <div className="w-50 m-w-100 c-f-1 t-a-c h-auto h-pr-fl-ma">
                <div className="w-100 h-auto h-pr-fl-ma">
                  <div className="w-50 h-auto h-pr-fl-ma c-p-1 ">
                  <DataBox titleStyle="m-b-5px" title="SECTOR/RUBRO EMPRESARIAL:" text="TECNOLOGÍAS DE LA INFORMACIÓN"></DataBox>
                  </div>
                  <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox titleStyle="m-b-5px" title="CANTIDAD DE EMPLEADOS:" text="DE 500 A 1000"></DataBox>
                  </div>
                </div>
                {/* TODO |                                  | PAÍS DE ORIGEN + CIUDAD DE ORIGEN | */}

                <div className="w-100 h-auto h-pr-fl-ma">
                  <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox titleStyle="m-b-5px" title="PAÍS DE ORIGEN" text="URUGUAY"></DataBox>

                  </div>
                  <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox titleStyle="m-b-5px" title="CIUDAD DE ORIGEN" text="MONTEVIDEO"></DataBox>
                  </div>
                </div>
                {/* TODO |                                  | UBICACIÓN ACTUAL + SUCURSAL DE ESTA CUENTA | */}

                <div className="w-100 h-auto h-pr-fl-ma ">
                  <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                  <DataBox titleStyle="m-b-5px" title="UBICACIÓN ACTUAL" text="0.00.00.00"></DataBox>
                  </div>
                  <div className="w-50  h-auto h-pr-fl-ma c-p-1">
                  <DataBox titleStyle="m-b-5px" title="SUCURSAL ACTUAL" text="CENTRAL"></DataBox>
                  </div>
                </div>
              </div>
            </div>
            </div>
            {/* TODO |                                  | PUBLICACIONES ACTIVAS | */}
            <div className="w-100 h-auto h-auto h-pr-fl-ma ns-bg-2 c-br-1 c-p-1">
              <div className="w-100 c-f-1 h-auto">
                <div className="w-100 h-auto f-w-bo c-f-1 ns-bg-1 ns-b-s c-br-1 h-pr-fl-ma">
                <div className="w-auto h-auto r-h-c h-pr-fl-ma c-p-1">
                <div className="w-100 h-auto h-pr-fl-ma t-a-c normal c-skyblue-node">NO HAY OFERTAS LABORALES ACTUALMENTE</div>
                <div className="w-100 h-auto h-pr-fl-ma t-a-c small">¿QUIERES ENTERARTE CUANDO -- NOMBRE EMPRESA -- PUBLIQUE UNA OFERTA LABORAL?</div>

                <div className="w-50 r-h-c h-auto h-pr-fl-ma c-p-1">
                  <div className="w-100 r-h-c m-w-20 h-pr-fl-ma">
                <Btn
                className="w-100 r-h-c h-pr-fr-ma"
                theme="info"
                icon="fas fa-bell"
                size="small"
              ><span className="m-d-n small m-l-5px">ACTIVAR NOTIFICACIONES</span></Btn></div>

         </div></div>
                </div> </div>
            </div>

        </div>
      </Fragment>
    );
}

export default BusinessProfile
