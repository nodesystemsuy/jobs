import React, { Fragment } from "react";
import "./styles.css";

import { Btn, Text, Inp, Checkbox } from "../../atoms/index.js";
import {DataBox} from "../../molecules/index.js"
import { Link } from "react-router-dom";
import { HOME_REGISTER_BUSINESS, HOME_REGISTER_PERSON } from "../../../constants/routes.constant";

const BusinessFormRegister = () => {
    return (
      <Fragment>
        {/* ?

* ██████  ███████  ██████  ██ ███████ ████████ ███████ ██████      ██████  ██    ██ ███████ ███████ ██ ███    ██ ███████ ███████ 
* ██   ██ ██      ██       ██ ██         ██    ██      ██   ██     ██   ██ ██    ██ ██      ██      ██ ████   ██ ██      ██      
* ██████  █████   ██   ███ ██ ███████    ██    █████   ██████      ██████  ██    ██ ███████ ███████ ██ ██ ██  ██ █████   ███████ 
* ██   ██ ██      ██    ██ ██      ██    ██    ██      ██   ██     ██   ██ ██    ██      ██      ██ ██ ██  ██ ██ ██           ██ 
* ██   ██ ███████  ██████  ██ ███████    ██    ███████ ██   ██     ██████   ██████  ███████ ███████ ██ ██   ████ ███████ ███████ 
                                                                                                                               

? */}
        <form className="w-100 h-100 c-p-1 of-hidden b-s-b-b h-pr-fl-ma">
        <div className="w-90 m-w-70 h-auto h-pr-fl-ma centered">
            <div className="w-100 h-auto of-hidden c-p-1 ns-bg-1 ns-b-s c-br-1 h-pr-fl-ma">
            <div className="w-100 h-auto h-pr-fl-ma ns-bg-2 c-br-1 m-b-5px c-p-1">
            <DataBox text="REGISTRO EMPRESAS" textStyle="ns-bg-1 small "></DataBox></div>

            <div className="w-100 b-s-b-b of-hidden h-auto h-pr-fl-ma ns-bg-2 m-b-5px c-br-1 c-p-1 d-i-f">
                <div className="w-50 b-s-b-b of-hidden d-i-f h-auto h-pr-fl-ma ">
                  <Btn
                    text="Personas"
                    className="w-100 h-auto h-pr-fl-ma c-br-1 bg-none b-s-b-b c-br-1 t-a-c c-p"
                    type="button"
                    size="small"
                    iconLeft="fas fa-user-circle"
                    path={HOME_REGISTER_PERSON}
                  ></Btn>
                </div>
                <div className="w-50 h-auto of-hidden h-e b-s-b-b h-pr-fl-ma">
                  <Btn
                    theme="fuchsia"
                    text="Empresas"
                    className="w-100 h-auto h-pr-fl-ma c-br-1 b-s-b-b c-br-1 t-a-c c-p"
                    type="button"
                    iconLeft="fas fa-briefcase"
                    size="small"
                    path={HOME_REGISTER_BUSINESS}
                  ></Btn>
                </div>
              </div>

              <div className="w-100 h-auto h-pr-fl-ma c-br-1 ns-bg-2 c-p-1">
              {/* NOMBRE Y APELLIDO  */}
              <div className="w-100 b-s-b-b h-auto h-pr-fl-ma">
                <div className="w-50 b-s-b-b h-auto h-pr-fl-ma c-p-1">
                <DataBox childrenStyle="m-b-5px ns-bg-1" className="small">
                <Inp
                    className="w-100"
                    iconLeft="fas fa-store"
                    theme="ns-bg-1 ns-c-1"
                    type="text"
                    size="normal"
                    placeholder="Nombre Fantasía"
                  />
                </DataBox>
                </div>
                <div className="w-50 b-s-b-b h-auto h-pr-fl-ma c-p-1">
                <DataBox childrenStyle="m-b-5px ns-bg-1" className="small">
                <Inp
                    iconLeft="fas fa-signature"
                    className="w-100"
                    theme="ns-bg-1 ns-c-1"
                    type="text"
                    size="normal"
                    placeholder="Razón Social"
                  />
                </DataBox>
                </div>
              </div>
               {/* NICKNAME Y RUT/RUC*/}
              <div className="w-100 h-auto h-pr-fl-ma">
                <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox classNameStyle="m-b-5px ns-bg-1" className="small">
                <Inp
                    className="w-100"
                    iconLeft="fas fa-at"
                    theme="ns-bg-1 ns-c-1"
                    type="text"
                    size="normal"
                    placeholder="Nickname"
                  />
                </DataBox>
                </div>
                <div className="w-50 h-auto h-pr-fl-ma c-p-1">
                <DataBox childrenStyle="m-b-5px ns-bg-1" className="small">
                <Inp
                      className="w-100"
                      theme="ns-bg-1 ns-c-1"
                      size="normal"
                      type="text"
                      placeholder="N° ID: RUT/RUC"
                    />
                </DataBox>
                </div>

              {/* EMAIL  */}
              <div className="w-100 h-auto h-pr-fl-ma c-p-1">
              <DataBox childrenStyle="m-b-5px ns-bg-1" className="small">
                <Inp
                    className="w-100"
                    iconLeft="fas fa-envelope"
                    theme="ns-bg-1 ns-c-1"
                    type="email"
                    size="normal"
                    placeholder="Email"
                  /></DataBox>
              </div>

              </div>
              {/* PASSWORD */}
              <div className="w-100 b-s-b-b h-auto h-pr-fl-ma">
                <div className="w-50 b-s-b-b h-auto h-pr-fl-ma c-p-1">
                <DataBox childrenStyle="m-b-5px ns-bg-1" className="small">
                  <Inp
                    className="w-100"
                    iconLeft="fas fa-asterisk"
                    theme="ns-bg-1 ns-c-1"
                    type="password"
                    size="normal"
                    placeholder="Contraseña"
                  /></DataBox>

                </div>
                <div className="w-50 b-s-b-b h-auto h-pr-fl-ma c-p-1">
                <DataBox childrenStyle="m-b-5px ns-bg-1" className="small">
                  <Inp
                    className="w-100"
                    iconLeft="fas fa-asterisk"
                    theme="ns-bg-1 ns-c-1"
                    type="password"
                    size="normal"
                    placeholder="Contraseña"
                  /></DataBox>
                </div>
              </div>
              </div>





              {/* NOMBRE Y APELLIDO (TEXTO)
              <div className="w-100 b-s-b-b h-auto h-pr-fl-ma">
                <div className="w-50 b-s-b-b h-auto h-pr-fl-ma">
                  <Text
                    text="Nombre Fantasía"
                    className="f-w-bo"
                    size="nano"
                  ></Text>
                </div>
                <div className="w-50 b-s-b-b h-auto h-pr-fl-ma">
                  <Text
                    text="Razón Social"
                    className="f-w-bo"
                    size="nano"
                  ></Text>
                </div>
              </div>

              <div className="w-100 h-auto h-pr-fl-ma">
                <div className="w-50 h-auto h-pr-fl-ma">
                  <Inp
                    iconLeft="fas fa-user-circle"
                    className="w-100"
                    theme="c-bg-9"
                    orientation="left"
                    type="text"
                    size="small"
                    placeholder="Nombre"
                  />
                </div>
                <div className="w-50 h-auto h-pr-fl-ma">
                  <Inp
                    theme="c-bg-9"
                    className="w-100"
                    orientation="right"
                    type="text"
                    size="small"
                    placeholder="Razon Social"
                  />
                </div>
              </div>

              <div className="w-100 h-auto h-pr-fl-ma">
                <div className="w-50 h-auto h-pr-fl-ma">
                  <Text
                    text="Nombre de usuario"
                    className="f-w-bo"
                    size="nano"
                  ></Text>
                </div>
                <div className="w-50 h-auto h-pr-fl-ma">
                  <Text
                    text="N° Ident. (RUT/RUC)"
                    className="f-w-bo"
                    size="nano"
                  ></Text>
                </div>
              </div>

              <div className="w-100 h-auto h-pr-fl-ma">
                <div className="w-50 h-auto h-pr-fl-ma">
                  <Inp
                    className="w-100"
                    iconLeft="fas fa-at"
                    theme="c-bg-9"
                    orientation="left"
                    type="text"
                    size="small"
                    placeholder="Nickname"
                  />
                </div>
                <div className="w-50 h-auto h-pr-fl-ma">
                  <div className="w-100 h-auto flex-auto h-pr-fl-ma">
                    <Inp
                      className="w-100"
                      theme="c-bg-9"
                      orientation="right"
                      type="text"
                      size="small"
                      placeholder="xxxxxxxxxxxxx"
                    />
                  </div>
                </div>
              </div>

              <div className="w-100 h-auto h-pr-fl-ma">
                <Text
                  text="Ingrese su email:"
                  className="f-w-bo"
                  size="nano"
                ></Text>
              </div>

              <div className="w-100 h-auto h-pr-fl-ma">
                <div className="w-100   h-auto h-pr-fl-ma">
                  <Inp
                    iconLeft="fas fa-envelope"
                    theme="c-bg-9"
                    className="w-100"
                    type="email"
                    size="small"
                    placeholder="Email"
                  />
                </div>
              </div>

              <div className="w-100 h-auto h-pr-fl-ma">
                <div className="w-50 b-s-b-b h-auto h-pr-fl-ma">
                  <Text
                    text="Ingrese su contraseña:"
                    className="f-w-bo"
                    size="nano"
                  ></Text>
                </div>
                <div className="w-50 b-s-b-b h-auto h-pr-fl-ma">
                  <Text
                    text="Repita su contraseña:"
                    className="f-w-bo"
                    size="nano"
                  ></Text>
                </div>
              </div>

              <div className="w-100 h-auto h-pr-fl-ma">
                <div className="w-50 h-auto h-pr-fl-ma">
                  <Inp
                    className="w-100"
                    iconLeft="fas fa-asterisk"
                    theme="c-bg-9"
                    orientation="left"
                    type="password"
                    size="small"
                    placeholder="Contraseña"
                  />
                </div>
                <div className="w-50   h-auto h-pr-fl-ma">
                  <Inp
                    className="w-100"
                    iconRight="fas fa-eye c-p h-e"
                    theme="c-bg-9"
                    orientation="right"
                    type="password"
                    size="small"
                    placeholder="Contraseña"
                  />
                </div>
              </div> */}




              {/* ACUERDO DE RESPONSABILIDAD (TEXTO) */}
              <div className="w-100 h-pr-fl-ma c-p-1">
                <div className="w-100 h-auto flexbox c-p-1 h-pr-fl-ma">
                  <div className="w-30 flex-auto h-auto h-pr-fl-ma">
                    <Checkbox
                      text="ACEPTO"
                      size="normal"
                      theme="fuchsia"
                      className="r-h-c"
                    ></Checkbox>
                  </div>
                  <div className="w-70 flex-auto h-pr-fl-ma">
                    <Text
                      className="f-w-bo"
                      size="nano"
                      text="Entiendo y acepto todas las políticas, términos y condiciones de uso."
                    ></Text>
                  </div>
                </div>
                <div className="w-100 h-auto h-pr-fl-ma">
                <Btn
                  theme="success"
                  iconLeft="fas fa-briefcase"
                  className="w-100 c-p-1"
                  size="normal"
                  text="REGISTRARSE"
                ></Btn></div>
              </div>
            </div>
          </div>
        </form>
      </Fragment>
    );
}

export default BusinessFormRegister
