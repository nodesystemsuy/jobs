import React, { Fragment } from "react";
import { Link } from 'react-router-dom'
import "./styles.css";

import { SidebarBtn, Btn, Img } from "../../atoms/index.js";
import { ProfileCoverPage, ProfilePicture, DataBox } from "../../molecules/index.js";

import { DASHBOARD_BUSINESS_CREATEINTERVIEW, DASHBOARD_BUSINESS_CREATEPOST, DASHBOARD_BUSINESS_EDITPROFILE, DASHBOARD_BUSINESS_PROFILE, DASHBOARD_BUSINESS_CANDIDATES, DASHBOARD_BUSINESS_INTERVIEWS, DASHBOARD_BUSINESS_PUBLICATIONS } from '../../../constants/routes.constant'

const BusinessSidebar = () => {
  return (
    <Fragment>
      <div className="w-100 h-100 h-pr-fl-ma  ns-bg-1 c-p-1 c-br-2">
        <div className="w-100 h-80 h-pr-fl-ma">
          <ProfileCoverPage className="h-200px c-br-2"></ProfileCoverPage>
          <ProfilePicture className="w-and-h-100px m-t-20px ns-bg-1 m-b-20px r-h-c"></ProfilePicture>
          <div className="w-100 h-auto c-p-2">
            <div className="w-100 h-auto c-p-2 ns-bg-1 ns-b-s c-br-2 h-pr-fl-ma">
              <Link to={DASHBOARD_BUSINESS_PROFILE}>
              <div className="w-100 h-auto h-pr-fl-ma ns-bg-2 c-p-1 c-br-1 m-b-5px">
                  <DataBox text="NOMBRE EMPRESA" textStyle="ns-bg-e ns-c-e-c f-w-bo flexbox l-h-1-1em f-w-w small b-s-b-b "></DataBox> </div>

              </Link>
              <div className="w-100 max-h-40vh h-pr-fl-ma c-br-1  ns-bg-2 c-p-1 of-y-auto obj-fit-con">
                <SidebarBtn
                  iconLeft="fas fa-bell"
                  counter="4321"
                  iconColor="c-red-node"
                  counterColor="bg-red-node"
                  text="Mis Alertas"
                  size="small"
                  theme="ns-bg-1 ns-c-1"
                ></SidebarBtn>
                <SidebarBtn
                  iconLeft="fas fa-newspaper"
                  counter="123"
                  iconColor="c-green-node"
                  counterColor="bg-green-node"
                  text="Mis Publicaciones"
                  size="small"
                  path={DASHBOARD_BUSINESS_PUBLICATIONS}
                  theme="ns-bg-1 ns-c-1"
                ></SidebarBtn>
                <SidebarBtn
                  iconLeft="fas fa-users"
                  counter="245"
                  iconColor="c-skyblue-node"
                  counterColor="bg-skyblue-node"
                  text="Ver Candidatos"
                  size="small"
                  path={DASHBOARD_BUSINESS_CANDIDATES}
                  theme="ns-bg-1 ns-c-1"
                ></SidebarBtn>
                <SidebarBtn
                  iconLeft="fas fa-user-edit"
                  iconColor="c-fuchsia-node"
                  text="Editar Perfil"
                  size="small"
                  path={DASHBOARD_BUSINESS_EDITPROFILE}
                  theme="ns-bg-1 ns-c-1"
                ></SidebarBtn>
                <SidebarBtn
                  iconLeft="fas fa-list-alt"
                  iconColor="c-skyblue-node"
                  text="Mis Entrevistas"
                  size="small"
                  path={DASHBOARD_BUSINESS_INTERVIEWS}
                  theme="ns-bg-1 ns-c-1"
                ></SidebarBtn>
                <SidebarBtn
                  iconLeft="fas fa-chart-bar"
                  counter="654"
                  iconColor="c-blue-node"
                  counterColor="bg-blue-node"
                  text="Mis Estadisticas"
                  size="small"
                  theme="ns-bg-1 ns-c-1"
                ></SidebarBtn>
                <a
                  href="https://nodesystems.uy/"
                  className="ns-c-1"
                  target="_blank"
                >
                  <SidebarBtn
                    iconLeft="fas fa-user-circle c-yellow-node"
                    counterColor="bg-yellow-node"
                    text="Consejos Gratis"
                    size="small"
                    theme="ns-bg-1 ns-c-1"
                  ></SidebarBtn>
                </a></div>
            </div></div>
        </div>

        <div className="w-100 h-20 h-pr-fl-ma">
          <div className="w-100 h-100 h-pr-fl-ma c-p-1">
            <div className="w-100  ns-bg-1 c-br-1 r-v-b h-auto small h-pr-fl-ma ns-bg-1 ns-b-s c-p-1">
              <div className="w-100 h-auto h-pr-fl-ma ns-bg-2 c-p-1 c-br-1">
            <div className="w-100 h-auto h-pr-fl-ma t-a-c c-f-1 f-w-bo l-h-1em of-h flexbox d-i-f a-i-c ns-bg-1 c-br-1 c-p-1">
        <div className="w-auto h-auto nano f-w-bo ns-c-1 ">BUSCA PERSONAL Y PREPARALES PARA SUS ENTREVISTAS:</div><i className=" w-auto h-auto fas fa-times ns-c-1 c-p-1 h-e c-p c-br-1"></i></div></div>

              <label
                for="SeleccionarCV" className="w-100 flexbox d-i-f h-auto h-pr-fl-ma c-p">

                <div className="w-50 h-auto flex-auto h-pr-fl-ma b-s-b-b c-p-1">
                  <div className="w-100 h-auto small  h-pr-fl-ma c-br-1  ns-bg-2 b-s-b-b c-p-1">

                    <div className="w-100 h-auto c-br-1 h-pr-fl-ma c-p">
                      <Btn className="small t-a-c w-100 of-h" iconLeft="fas fa-plus" path={DASHBOARD_BUSINESS_CREATEINTERVIEW} text="ENTREVISTA" theme="info"></Btn>
                    </div> </div>
                </div>
                <div className="w-50 h-auto flex-auto h-pr-fl-ma b-s-b-b c-p-1">
                  <div className="w-100 h-auto small  h-pr-fl-ma c-br-1 ns-bg-2 b-s-b-b c-p-1">

                    <div className="w-100 h-auto c-br-1 h-pr-fl-ma c-p">
                      <Btn className="small t-a-c w-100 of-h" iconLeft="fas fa-plus" path={DASHBOARD_BUSINESS_CREATEPOST} text="POST" theme="success"></Btn>
                    </div> </div>
                </div></label>
            </div>
          </div>
        </div>

      </div>
    </Fragment>
  );
}

export default BusinessSidebar
