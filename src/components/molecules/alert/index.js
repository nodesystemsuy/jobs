import React, { Fragment } from "react";


import "./styles.css";

export default function Alert(props) {
  const { Icon, title, description, className, theme } = props;

  return (
    <Fragment>
<div className={` w-100 h-auto h-pr-fl-ma m-b-5px flexbox ${className} ${theme} ns-bg-1 ns-b-s c-br-1 c-p-1 `}>
    <div className="w-10 m-w-20 h-auto h-pr-fl-ma flex-auto ">
        <div className="w-and-h-40px h-pr-fl-ma bg-red-node r-h-c circle">
            <i className="fas centered fa-bell c-white h-pr-fl-ma"></i>
        </div>
    </div>



    <div className="w-90 m-w-80 h-auto m-b-3px h-pr-fl-ma flex-auto">
        <div className="w-100 h-auto flexbox m-b-3px ns-bg-2 c-br-1 c-p-1 d-i">
            <div className="w-auto p-r-10px flex-auto ">
                <span className="w-and-h-25px h-pr-fl-ma  ns-bg-1 c-p-1 b-s-b-b c-br-1 c-p h-e">
                    <i className={` fas fa-user-circle ${Icon} ns-c-1 centered ns-c-1 `} ></i>
                </span>
            </div>
            <div className="w-100 h-25px flex-auto ns-c-1 f-w-bo p-t-3px b-s-b-b c-f-1 normal">
                {title}
            </div>
            <div className="w-20 flex-auto">
                <span className="w-and-h-25px h-pr-fr-ma  ns-bg-1 c-p-1 b-s-b-b c-br-1 c-p h-e">
                    <i className="fas fa-times small  centered ns-c-1"></i>
                </span>
                <span
                    className="w-and-h-25px h-pr-fr-ma  ns-bg-1 m-r-5px c-p-1 b-s-b-b c-br-1 c-p h-e">
                    <i className="fas fa-eye  small centered ns-c-1"></i>
                </span>
            </div>
        </div>
        {description && <div  className="w-100 ns-bg-1  c-br-1 small c-f-1  c-p-1">
            <span className="max-l-3 ns-c-1 small ns-c-1 f-w-bo">{description}
            </span>
        </div> }
    </div>
</div>
</Fragment>
  );
}
