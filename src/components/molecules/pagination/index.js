import React, { Fragment } from "react";
import "./styles.css";

import { Btn } from "../../atoms/index.js";

const Pagination = ()=>{
  return (
      <Fragment>
        <div className="w-100 h-auto small r-v-c flexbox h-pr-fl-ma  ns-bg-1 c-f-1 small c-br-2 c-p-1">
          <div className="w-15 m-w-20 flex-auto h-auto h-pr-fl-ma c-p-1 ns-bg-2 c-br-1">
            <Btn className="w-100" iconLeft="fas fa-chevron-left" theme="ns-bg-1 ns-c-1" size="small" text="ANTERIOR"></Btn>
          </div>
          <div className="w-70 m-w-60 flex-auto h-auto h-pr-fl-ma b-s-b-b m-r-5px m-l-5px ns-bg-2 c-br-1">
            <div className="r-h-c h-pr-fl-ma">
              <div className="w-auto h-auto h-pr-fl-ma f-w-bo m-2px ns-bg-1 ns-c-1 c-p h-e c-f-1 p-l-10px p-r-10px b-s-b-b c-br-1 c-p-1">
                <span className="c-p-1">1</span>
              </div>
              <div className="w-auto h-auto h-pr-fl-ma f-w-bo m-2px ns-bg-1 ns-c-1 c-p h-e c-f-1 p-l-10px p-r-10px b-s-b-b c-br-1 c-p-1">
                <span className="c-p-1">&hellip;</span>
              </div>
              <div className="w-auto h-auto h-pr-fl-ma f-w-bo m-2px ns-bg-1 ns-c-1 c-p h-e c-f-1 p-l-10px p-r-10px b-s-b-b c-br-1 c-p-1">
                <span className="c-p-1">2</span>
              </div>
              <div className="w-auto h-auto h-pr-fl-ma f-w-bo m-2px ns-bg-1 ns-c-1 c-p h-e c-f-1 p-l-10px p-r-10px b-s-b-b c-br-1 c-p-1">
                <span className="c-p-1">3</span>
              </div>
              <div className="w-auto h-auto h-pr-fl-ma f-w-bo m-2px ns-bg-1 ns-c-1 c-p h-e c-f-1 p-l-10px p-r-10px b-s-b-b c-br-1 c-p-1">
                <span className="c-p-1">4</span>
              </div>
              <div className="w-auto h-auto h-pr-fl-ma f-w-bo m-2px ns-bg-1 ns-c-1 c-p h-e c-f-1 p-l-10px p-r-10px b-s-b-b c-br-1 c-p-1">
                <span className="c-p-1">5</span>
              </div>
              <div className="w-auto h-auto h-pr-fl-ma f-w-bo m-2px ns-bg-1 ns-c-1 c-p h-e c-f-1 p-l-10px p-r-10px b-s-b-b c-br-1 c-p-1">
                <span className="c-p-1">&hellip;</span>
              </div>
              <div className="w-auto h-auto h-pr-fl-ma f-w-bo m-2px ns-bg-1 ns-c-1 c-p h-e c-f-1 p-l-10px p-r-10px b-s-b-b c-br-1 c-p-1">
                <span className="c-p-1">6</span>
              </div>
            </div>
          </div>
          <div className="w-15 m-w-20 flex-auto h-auto h-pr-fr-ma c-p-1 ns-bg-2 c-br-1">
            <Btn className="w-100" iconRight="fas fa-chevron-right" theme="ns-bg-1 ns-c-1" size="small" text="SIGUIENTE"></Btn>
          </div>
        </div>
      </Fragment>
    );
}

export default Pagination
