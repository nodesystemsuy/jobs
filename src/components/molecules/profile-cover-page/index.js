import React, { Fragment } from "react";
import "./styles.css";

import testImg from "../../../assets/images/testimg.png";

export default function ProfileCoverPage(props) {
  const { className } = props;

  return (
    <Fragment>
        <div className={` ${className}  w-100  p-a c-br-2 of-hidden bg-s-cov h-e c-p t-03s`}>
          <img src={testImg} alt="" className="w-100 h-100 c-br-2 obj-fit-cov"/>
        </div>
      </Fragment>
    );
  }

