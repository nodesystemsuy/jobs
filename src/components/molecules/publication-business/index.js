import React, { Fragment } from "react";
import "./styles.css";
import { Checkbox } from "../../atoms";


export default function PublicationBusiness(props) {
  const {} = props;

  return (
    <Fragment>
      <div className="w-100 ns-bg-1 small flexbox c-br-2 m-b-5px h-auto h-pr-fl-ma c-p ">
        <div className="w-5 h-15vh h-pr-fl-ma">
          <div className="w-70 r-v-c h-80 br-t-r-10px br-b-r-10px  bg-red-node v-a-m">
            <i className="fas fa-exclamation centered c-white"></i>
          </div>
        </div>
        <div className="w-95 h-100 h-pr-fl-ma">
          <div className="w-100 d-i-f c-p-1 h-auto h-pr-fl-ma">
            <div className="w-30 h-auto c-f-1 small h-pr-fl-ma f-w-bo t-of-e ns-c-1">
              PUBLICATION BUSINESS
            </div>
            <div className="w-20 h-auto c-f-1 small h-pr-fl-ma f-w-bo t-of-e t-a-c  c-green-node">
              0000000
            </div>
            <div className="w-10 h-auto c-f-1 small h-pr-fl-ma f-w-bo t-of-e t-a-c ns-c-1">
              UY
            </div>
            <div className="w-20 h-auto c-f-1 h-pr-fl-ma f-w-bo t-of-e t-a-c ns-c-1">
              NODESYSTEMS
            </div>
            <div className="w-20 h-auto c-f-1  h-pr-fl-ma t-of-e t-a-c ">
              <div className="w-auto h-auto h-pr-fr-ma">
                <Checkbox text="Seleccionar"></Checkbox>

              </div>
            </div>
          </div>
          <div className="w-100 d-i-f h-10vh c-p-1">
            <div className="w-80 m-w-70 h-100 max-lines-4 of-hidden  c-f-1 ns-c-1 ">
              Lorem ipsum, dolor sit amet consectetur adipisicing elit.
              Obcaecati, veritatis vitae in quo accusantium molensectetur
              adipisicing elit. Obcaecati, veritatis vitae in quo accusantium
              molestiae quasi corrupti aperiam quibusdam beatae? Deleniti
              dolorum placeat tempora quisquam, eius natus stiae quasi corrupti
              aperiam quibusdam beatae? Deleniti dolorum placeat tempora
              quisquam, eius natus provident at. Obcaecati.
            </div>
            <div className="w-20 m-w-30 h-auto h-pr-fl-ma m-small">
            <div className="w-100 h-auto d-i-f a-i-c b-s-b-b h-pr-fl-ma ns-bg-2 c-br-1 c-p-1">
                <div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w a-i-c c-p-1 b-s-b-b ns-bg-1 br-left h-e c-br-1 small">
                  <i className="w-auto h-auto h-pr-fl-ma fas fa-trash t-a-c ns-c-1 p-t-5px"></i>
                  <div className="w-100 h-auto h-pr-fl-ma f-w-bo t-a-c small ns-c-1 c-f-1">
               ELIMINAR
                  </div>

              </div>
              <div className="w-100 h-auto h-pr-fl-ma flexbox f-w-w  a-i-c c-p-1 b-s-b-b ns-bg-1 br-right h-e c-br-1 small">
                  <i className="w-auto h-auto h-pr-fl-ma fas fa-edit t-a-c ns-c-1 p-t-5px"></i>
                  <div className="w-100 h-auto h-pr-fl-ma f-w-bo t-a-c small ns-c-1 c-f-1">
                    MODIFICAR
                  </div>

              </div>


              </div>
            </div>
          </div>
          <div className="w-100 d-i-f h-auto f-w-bo h-pr-fl-ma">
            <div className="w-25 h-auto c-f-1 nano h-pr-fl-ma t-of-e c-p-1 c-p">
              <i className="h-pr-fl-ma fas c-p-1 fa-clock"></i>
              <span className="h-pr-fl-ma c-p-1 ns-c-1">Hace 2 min</span>
            </div>
            <div className="w-25 h-auto c-f-1 nano h-pr-fl-ma t-of-e c-p-1 t-a-c f-w-bo c-p c-blue-node ">
              <i className="h-pr-fl-ma fas fa-wheelchair  p-l-10px c-white  bg-blue-node c-br-1 c-p-1"></i>
              <span className="h-pr-fl-ma c-p-1 ns-c-1">PRIORIDAD</span>
            </div>
            <div className="w-25 h-auto c-f-1 nano h-pr-fl-ma t-of-e c-p-1 t-a-c ">
              <span className="h-pr-fl-ma c-p-1 ns-c-1">Prioridad</span>
            </div>
            <div className="w-25 h-auto c-f-1 nano h-pr-fl-ma t-of-e c-p-1 t-a-c ">
              <span className="h-pr-fl-ma  t-of-e max-l-1 c-p-1 ns-c-1">
                Montevideo | Uruguay
              </span>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}
