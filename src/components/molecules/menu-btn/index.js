import React from "react";
import "./styles.css";

import {Btn} from "../../atoms/index.js";
import {ProfilePicture} from "../index.js";

import { BusinessMenu , PersonMenu } from '../../organisms'

export default function MenuBtn(props) {
  const { className, setMenuView, menuView } = props;

  return (
    <>
      <div
        className={`w-80 h-auto flexbox ${className} f-w-n-w d-i-f l-h-1em h-pr-fl-ma small c-p-1 ns-b-s c-br-2 c-p`}
      >
        <div className="w-auto h-auto flex-auto h-pr-fl-ma ">
          <div className="w-and-h-30px ns-bg-2 circle"><ProfilePicture className="w-and-h-100"></ProfilePicture></div>
        </div>
        <div className="w-auto h-auto flex-auto h-pr-fl-ma c-f-1 normal m-d-n ">
          <div className="w-100 h-auto h-pr-fl-ma nano  c-br-2 b-s-b-b c-p-1 d-i-f">
            <div className="w-60 h-auto h-pr-fl-ma m-l-2px f-w-bo ns-c-1 c-br-1">NOMBRE APELLIDO</div>
            <div className="w-40 t-a-c h-auto h-pr-fl-ma ns-bg-e ns-c-e-c c-p-1 c-br-1 f-w-bo">LVL: 20</div></div>
        </div>
        <div className="c-p-1 h-pr-fl-ma" onClick={()=>setMenuView(!menuView)}>
        <Btn 
        icon="fas fa-bars"
        theme="ns-c-1"
        className="w-auto c-p-1"
        ></Btn></div>
      </div>

    </>
  );
}
