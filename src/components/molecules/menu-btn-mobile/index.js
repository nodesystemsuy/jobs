import React, { Fragment } from "react";
import { Link } from 'react-router-dom'
import "./styles.css";
import { Btn } from "../../atoms";
export default function MenuBtnMobile(props) {
  const {
    type,
    theme,
    size,
    className,
    children,
    iconLeft,
    icon,
    path,
    text,
    iconRight,
  } = props;

  return (
    <>
    <Link to={path}>

    {children && <div className="MenuBtnBackground b-s-b-b z-i-99 of-visible"></div>}
      <div className="MenuBtnOptionList b-s-b-b z-i-99 of-visible flexbox a-i-c">
        <div className="w-100 h-auto h-pr-fl-ma c-p-1">
       {children}</div>
      </div>
      <div
        type={`${type} `}

        className={`MenuBtnMobile h-e  ${theme} ${size} b-s-b-b z-i-99 circle flexbox a-i-c `}
      >
          {icon && <div className="w-100 h-100 h-pr-fl-ma"><Btn className={`w-100 h-100 circle ${className}`}><i className={` ${icon} z-i-99 `}></i></Btn>     </div>}

      </div>
      </Link>
    </>
  );
}
