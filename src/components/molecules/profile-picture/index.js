import React, { Fragment } from "react";
import "./styles.css";

import testImg from "../../../assets/images/testimg.png";
export default function ProfilePicture(props) {
  const { className, theme, } = props;

  return (
    <Fragment>
      <label
        className={`${className} of-hidden c-p-1 ${theme} b-s-b-b c-br-3 h-pr-fl-ma`}
        for="ProfilePicture"
      >
        <img
          className={`w-100 h-100 c-p h-pr-fl-ma ${theme}  of-hidden h-e obj-fit-cov c-br-3`}
          src={testImg}
        />
      </label>
      <input type="file" id="ProfilePicture" hidden />
    </Fragment>
  );
}
