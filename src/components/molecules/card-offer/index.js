import React, { Fragment } from "react";
import "./styles.css";

import {Btn, Text} from "../../atoms/index.js";

export default function CardOffer(props) {
  const { icon, title,price, badge, btnText, cent, subtitle, className, before, theme, description } = props;

  return (
    <Fragment>
<div className={` h-auto  h-pr-fl-ma c-p-1 ${className} `}>
            <div className="w-100 ns-bg-1 c-br-1 h-auto h-pr-fl-ma of-h c-p-1 ">
              <div className="w-100 h-auto h-pr-fl-ma ">
                <Text text={title} align="t-a-c" size="title" theme={theme} className="h-pr-fl-ma f-w-bo"></Text></div>
                <div className="w-100 h-10 h-pr-fl-ma small">
                <div className="w-100 h-auto h-pr-fl-ma c-br-1 nano d-i-f m-b-5px c-p-1">
                 <div  align="t-a-c" className="w-50 h-auto h-pr-fl-ma c-p-1 f-w-bo">ANTES <s>{before}</s></div>
                  <div align="t-a-c" className="w-50 h-auto h-pr-fl-ma c-p-1 f-w-bo">AHORA:</div>
                </div>
              </div>
                <div  className="w-100 h-10 h-pr-fl-ma f-w-bo normal">

                <div  className="w-auto r-h-c d-i-f a-i-b medium h-auto c-p-1">
                <div  className="w-auto h-auto t-a-c f-w-bo big">{price}.</div>
                <div  className="w-auto h-auto t-a-c f-w-bo normal">{cent}</div>
               </div></div>
               {badge &&<div  className="w-100 h-auto h-pr-fl-ma a-i-c t-a-c f-w-bo small">{badge}</div>}
               {subtitle &&  <div className="w-100 h-auto h-pr-fl-ma f-w-bo small t-a-c c-p-1">{subtitle}</div>}
               {description && <div className="w-100 h-auto h-pr-fl-ma f-w-bo nano t-a-c c-p-1 c-br-1 ns-bg-2">{description}</div>}
                <div className="w-100 h-auto  h-pr-fl-ma">
              <div className="w-80 h-auto h-pr-fl-ma r-h-c c-p-1">
                <Btn className="w-100 t-a-c"  iconLeft="fas fa-check" size="small" theme={theme} text={btnText}></Btn>
                </div>
              </div>
              <div className="w-100 h-auto h-pr-fl-ma f-w-bo small t-a-c h-auto c-p-1">BENEFICIOS:</div>
              <div className="w-100 h-auto h-pr-fl-ma small">
                <div className="w-100 h-auto r-h-c h-pr-fl-ma small of-auto">
                  <div className="w-100 h-25px c-br-1 t-a-l ns-bg-2 c-p-1 m-b-2px small">10 MEJORA/mes</div>
                  <div className="w-100 h-25px c-br-1 t-a-l ns-bg-2 c-p-1 m-b-2px small">5 MEJORA/mes</div>
                  <div className="w-100 h-25px c-br-1 t-a-l ns-bg-2 c-p-1 m-b-2px small">3 MEJORA/mes</div>
                  <div className="w-100 h-25px c-br-1 t-a-l ns-bg-2 c-p-1 m-b-2px small">2 MEJORA/mes</div>
                  <div className="w-100 h-25px c-br-1 t-a-l ns-bg-2 c-p-1 m-b-2px small">5 MEJORA/mes</div>

                </div>
              </div>


            </div>

          </div>

    </Fragment>
  );
}
