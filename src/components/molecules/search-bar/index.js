import React, { Fragment } from "react";

import { Btn,  } from "../../atoms/index.js";
import { DataBox } from "../../molecules/index.js";

import "./styles.css";

const SearchBar = () => {
      return (
        <Fragment>

<div className="w-100 h-100 h-pr-fl-ma ">

    <label
        for="SearchBar"
        className="w-100 h-45px flexbox d-i-f ns-b-s b-s-b-b ns-bg-1 ns-c-1 h-pr-fl-ma c-br-1">
        <input

            className="w-80 h-100 d-i-f br-none ns-bg-1 small b-s-b-b p-l-10px ns-c-1 br-left c-f-1 ns-c-1 h-pr-fl-ma"
            placeholder="Buscar publicaciones aquí..."
            id="SearchBar"/>
        <div className="w-20 h-100 d-i-f flexbox h-pr-fl-ma ns-bg-1">
            <div className="w-40 h-100 h-pr-fl-ma  b-s-b-b ns-bg-1">
                <Btn
                    className="w-and-h-100 normal br-0px h-e c-br-1  e-f-rotate-180 ns-bg-1 "
                   icon="fas fa-sort-amount-up-alt"></Btn>
            </div>
            <div className="w-60 h-100 h-pr-fl-ma  b-s-b-b ">
                <Btn
                    className="w-and-h-100 normal br-right h-e c-br-1 "
                    theme="ns-bg-1"
                    icon="fas fa-search"></Btn>
            </div>
        </div>
    </label>

</div>
 </Fragment>
 );
}

export default SearchBar
