import React, { Fragment } from "react";
import "./styles.css";
import { Btn, Checkbox } from "../../atoms/index.js";

export default function PublicationControlers(props) {
  //const {} = props;

  return (
    <Fragment>
      <div className="w-100 h-50px h-pr-fl-ma c-br-1  ">
         <div className="w-100 h-auto h-pr-fl-ma r-v-c c-p-1">
          <div className="w-auto d-i-f h-auto ns-bg-2 c-p-1 c-br-1  h-pr-fr-ma">
          <div className="w-auto d-i-f h-pr-fl-ma h-auto">
          <div className="w-auto d-i-f h-pr-fl-ma h-auto m-r-5px">
          <Btn
            iconLeft="fas fa-trash"
            size="small"
            theme="alert"
            text="ELIMINAR"
            className="w-100"
          ></Btn>
          </div>
          <div className="w-auto d-i-f h-pr-fl-ma h-auto m-r-5px">
          <Btn
            iconLeft="fas fa-check-circle"
            size="small"
            theme="success"
            text="FINALIZAR"
            className="w-100"
          ></Btn></div>
          <div className="w-auto d-i-f h-pr-fl-ma h-auto">
           <Checkbox
            size="small"
            theme="ns-bg-1 default"
            text="SELECCIONAR TODO"
            className="h-pr-fl-ma c-p-1 f-w-bo"
          ></Checkbox>
        </div></div></div>
        </div>

      </div>
    </Fragment>
  );
}
