import React, { Fragment } from "react";


import "./styles.css";

export default function DataBox(props) {
  const { Icon, text, title, status, content, children, description, textField, textHelper, className, theme, titleStyle, textStyle, contentStyle, childrenStyle } = props;

  return (
    <Fragment>

      <div className={` w-100 h-auto t-r-o-l h-pr-fl-ma ${className} ${theme} c-br-1`}>
      {textField &&  <div className={` w-100 h-100 h-pr-fl-ma c-f-1 normal ${textField} textField c-br-1 z-i-99`}><span>{textField}</span></div>}
        <div className={` w-100 h-100 h-pr-fl-ma ${textHelper && status} status c-br-1`}>

          {title && <div className={` ${titleStyle} w-100  h-auto ns-bg-1 ns-b-s ns-c-1 f-w-bo  h-pr-fl-ma c-f-1 p-l-5px b-s-b-b small c-br-1 `}>{title}</div>}
          {text && <div className={` ${textStyle} w-100  h-auto ns-bg-1 ns-b-s f-w-bo  ns-c-1 c-br-1 c-p-1 h-pr-fl-ma d-i-f c-f-1 t-a-c `}><span className="w-100 h-auto h-pr-fl-ma">{text}</span></div>}
          {content && <div className={`${contentStyle} w-100  h-auto ns-bg-1 ns-b-s f-w-bo  ns-c-1 c-br-1 c-p-1  h-pr-fl-ma d-i-f c-f-1`}>{content}</div>}
          {children && <div className={` ${childrenStyle} w-100  h-auto ns-bg-1 f-w-bo  ns-c-1 c-br-1 c-p-1  h-pr-fl-ma d-i-f c-f-1`}>{children}</div> }
          {/* {textHelper &&  <div className={`w-100 h-auto h-pr-fl-ma c-f-1 small  l-h-1em c-p-1 ${textHelper} c-br-1 z-i-99 `}>{textHelper}</div>} */}
          {textHelper &&  <div className={`w-100 h-auto h-pr-fl-ma c-f-1 small  l-h-1em p-1px ${textHelper} c-br-1 z-i-99 `}>{textHelper}</div>}
        </div>

      </div>
    </Fragment>
  );
}
